﻿Imports isr.Core.EnumExtensions
Imports isr.Core.AssemblyInfoExtensions
''' <summary>
''' Test panel.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/30/2009" by="David" revision="1.0.3376.x">
''' Created
''' </history>
Public Class TestPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' set large font for messages.
        Me._messagesBox.Font = New Font(Me._messagesBox.Font.FontFamily, 10, FontStyle.Regular)
        Me._messagesBox.TabCaption = "MESSA&GES"

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shadow parameter.
    ''' </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW

            Return cp
        End Get
    End Property

#End Region

    ''' <summary>Cleans up managed components.</summary>
    ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
    Private Sub onDisposeManagedResources()

        If Me._testStation IsNot Nothing Then
            Me._testStation.Dispose()
            Me._testStation = Nothing
        End If

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()
        ' set module objects that reference other objects to Nothing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
            ' disable the refresh timer.
            Me._refreshTimer.Enabled = False
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

        ' set default cursor.
        Me.Cursor = System.Windows.Forms.Cursors.Default

        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.PrependStatusMessage("UNLOADING.")
            Me._messagesBox.PrependMessage(Me._statusLabel.Text)

            Me.PrependStatusMessage("CLOSE TEST STATION")
            If Me._testStation IsNot Nothing Then
                Me._testStation.CloseTestStation()
            End If

            Me.PrependStatusMessage("SAVING SETTINGS.")
            System.Windows.Forms.Application.DoEvents()

            ' terminate all the singleton objects.
            Me.PrependStatusMessage("CLOSING. PLEASE WAIT.")

            ' delay a bit longer
            Dim stopTimer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
            Do Until stopTimer.ElapsedMilliseconds > Me._refreshTimer.Interval
                Threading.Thread.Sleep(10 + Me._refreshTimer.Interval \ 10I)
                Application.DoEvents()
            Loop

        Catch ex As Exception

            ExceptionDisplay.ProcessException(ex)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

            Me._initializeButton.Enabled = False
            Me._closeStationButton.Enabled = False
            Me._openStationButton.Enabled = True

        End Try

    End Sub

    ''' <summary>
    ''' Occurs when the form is loaded.
    ''' Does the pre-processing rendering.
    ''' </summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects

            ' display location of datalog.
            Me._messagesBox.PrependMessage(My.Application.Log.DefaultFileLogWriter.CustomLocation)
            Me._messagesBox.PrependMessage("Logging data to:")

            ' center the form
            ' Me.CenterToScreen()

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

        Catch ex As Exception

            Dim synopsis As String = "FAILED LOADING"
            Me._statusLabel.Text = synopsis
            Me._messagesBox.PrependException(ex.StackTrace)
            Me._messagesBox.PrependException(ex.Source.ToString)
            Me._messagesBox.PrependException(ex.Message)
            Me._messagesBox.PrependException(synopsis)

            If ExceptionDisplay.ProcessException(ex, "", WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects them.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges")>
    Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        ' allow form rendering time to complete: process all messages currently in the queue.
        Application.DoEvents()

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' initializeUserInterface()
            Me._initializeButton.Enabled = False

            ' set the file name of the reset process.
            isr.Kte.BaseTestStation.ResetProcessFilePathName = My.Settings.ResetProcessFilePathName

            ' set combo
            Me._modelComboBox.Items.Clear()
            Me._modelComboBox.DataSource = GetType(isr.Kte.ImpedanceModel).ValueDescriptionPairs()
            Me._modelComboBox.DisplayMember = "Value"
            Me._modelComboBox.ValueMember = "Key"
            Me._modelComboBox.SelectedItem = ImpedanceModel.SeriesResistance.ValueDescriptionPair()

            Me._speedComboBox.Items.Clear()
            Me._speedComboBox.DataSource = GetType(isr.Kte.MeasurementSpeed).ValueDescriptionPairs()
            Me._speedComboBox.DisplayMember = "Value"
            Me._speedComboBox.ValueMember = "Key"
            Me._speedComboBox.SelectedItem = MeasurementSpeed.Normal.ValueDescriptionPair()

            Me._cableComboBox.Items.Clear()
            Me._cableComboBox.DataSource = GetType(isr.Kte.CorrectedCableLength).ValueDescriptionPairs()
            Me._cableComboBox.DisplayMember = "Value"
            Me._cableComboBox.ValueMember = "Key"
            Me._cableComboBox.SelectedItem = CorrectedCableLength.OnePointFiveMeters.ValueDescriptionPair()

            ' enable the refresh timer.
            Me._refreshTimer.Interval = 500
            Me._refreshTimer.Enabled = True

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

        Catch ex As Exception

            Dim synopsis As String = "FAILED LOADING"
            Me._statusLabel.Text = synopsis
            Me._messagesBox.PrependException(ex.StackTrace)
            Me._messagesBox.PrependException(ex.Source.ToString)
            Me._messagesBox.PrependException(ex.Message)
            Me._messagesBox.PrependException(synopsis)

            If ExceptionDisplay.ProcessException(ex, "", WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " GUI "

    ''' <summary>
    '''  Displays a status message and adds it to the message box.
    ''' </summary>
    Private Sub PrependMessage(ByVal format As String, ByVal ParamArray args() As Object)

        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me._messagesBox.PrependMessage(details)
        My.Application.Log.WriteEntry(details, TraceEventType.Verbose)

    End Sub

    ''' <summary>
    '''  Displays a status message and adds it to the message box.
    ''' </summary>
    Private Sub PrependStatusMessage(ByVal format As String, ByVal ParamArray args() As Object)

        Me._statusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me._messagesBox.PrependMessage(Me._statusLabel.Text)
        My.Application.Log.WriteEntry(Me._statusLabel.Text, TraceEventType.Verbose)

    End Sub

    ''' <summary>
    ''' Gets or sets the display format.
    ''' </summary>
    Private _displayFormat As String = "0.000e+00"

#End Region

#Region " SUPPORT METHODS "

    ''' <summary>
    ''' Shows the error provider.
    ''' </summary>
    Private Sub showErrorProvider(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object)
        Me.PrependStatusMessage(format, args)
        Me._errorProvider.SetError(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary>
    ''' Hides the error provider.
    ''' </summary>
    Private Sub hideErrorProvider(ByVal control As Control)
        Me._errorProvider.SetError(control, "")
    End Sub

#End Region

#Region " LIST VIEW MANAGEMENT "

    ''' <summary>
    ''' Creates the list view for measurements.
    ''' </summary>
    Private Sub createMeasurementsListView()

        Me._measurementstListView.GridLines = True
        Me._measurementstListView.LabelEdit = False
        Me._measurementstListView.ShowGroups = False
        Me._measurementstListView.ShowItemToolTips = True
        Me._measurementstListView.UseAlternatingBackColors = True

        Me._instrumentColumn.AspectGetter = AddressOf instrumentNameGetter
        Me._measurementModelColumn.AspectGetter = AddressOf measurementModelGetter
        Me._measurementStatusColumn.AspectGetter = AddressOf measurementStatusGetter
        Me._primaryMeasurementColumn.AspectGetter = AddressOf primaryValueGetter
        Me._secondaryMeasurementColumn.AspectGetter = AddressOf secondaryValueGetter

        Me._measurementstListView.SetObjects(Me._testStation.Instruments)
        Me._measurementstListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)
        Me._measurementstListView.Invalidate()

    End Sub

    Private Shared Function instrumentGetter(ByVal row As Object) As isr.Kte.IInstrument
        Return CType(row, isr.Kte.IInstrument)
    End Function

    Private Function instrumentNameGetter(ByVal row As Object) As Object
        Return instrumentGetter(row).UniqueKey
    End Function

    Private Function measurementModelGetter(ByVal row As Object) As Object
        Dim instrument As isr.Kte.IInstrument = instrumentGetter(row)
        If instrument.Enabled AndAlso instrument.MeasurementModel.HasActualValue Then
            Dim value As Integer = instrument.MeasurementModel.ActualValue
            Return CType(value, isr.Kte.ImpedanceModel).ToString()
        Else
            Return ""
        End If
    End Function

    Private Function measurementStatusGetter(ByVal row As Object) As Object
        Dim instrument As isr.Kte.IInstrument = instrumentGetter(row)
        If instrument.Enabled Then
            Return "0x" & instrument.MeasurementStatus.ToString("X8", Globalization.CultureInfo.CurrentCulture)
        Else
            Return ""
        End If
    End Function

    Private Function primaryValueGetter(ByVal row As Object) As Object
        Dim instrument As isr.Kte.IInstrument = instrumentGetter(row)
        If instrument.Enabled AndAlso instrument.Impedance.HasValue Then
            Return instrument.Impedance.PrimaryValue.ToString(Me._displayFormat, Globalization.CultureInfo.CurrentCulture)
        Else
            Return ""
        End If
    End Function

    Private Function secondaryValueGetter(ByVal row As Object) As Object
        Dim instrument As isr.Kte.IInstrument = instrumentGetter(row)
        If instrument.Enabled AndAlso instrument.Impedance.HasValue Then
            Return instrument.Impedance.SecondaryValue.ToString(Me._displayFormat, Globalization.CultureInfo.CurrentCulture)
        Else
            Return ""
        End If
    End Function

#End Region

#Region " IMPEDANCE CONTROLS EVENTS "

    ''' <summary>
    ''' Applies settings.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub applySettings()

        hideErrorProvider(Me._applyButton)
        Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew

        ' make sure the channel of the active instrument is activated.
        Me._testStation.ActiveInstrument.SelectChannel()

        Me.PrependStatusMessage("Setting instrument enabled {0}", Me._enabledCheckBox.Checked)
        Me._testStation.ActiveInstrument.Enabled = Me._enabledCheckBox.Checked

        Me.PrependStatusMessage("Setting source bias to {0} volts", Me._sourceBiasNumeric.Value)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.SamplingBiasSetter(Me._sourceBiasNumeric.Value)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Source bias set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._sourceBiasNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.SamplingBias.ActualValue,
                                                                 Me._sourceBiasNumeric.Minimum), Me._sourceBiasNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        ' set the source bias so we do not have to remove it from the set values.
        Me._testStation.ActiveInstrument.SourceBiasSetter(Me._sourceBiasNumeric.Value)

        Me.PrependStatusMessage("Setting sampling count to 1")
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.SamplingCountSetter(1)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Sampling count set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting hold time to 0")
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.HoldTimeSetter(0)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("hold time set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting sampling interval to 0")
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.SamplingIntervalSetter(0)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("sampling interval set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting source frequency to {0} Hertz", Me._frequencyNumeric.Value)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.SourceFrequencySetter(Me._frequencyNumeric.Value)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Source frequency set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._frequencyNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.SourceFrequency.ActualValue,
                                                                Me._frequencyNumeric.Minimum), Me._frequencyNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Dim scaleFactor As Double = 1000
        Me.PrependStatusMessage("Setting source level to {0} volts", Me._sourceLevelNumeric.Value / scaleFactor)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.SourceLevelSetter(CSng(Me._sourceLevelNumeric.Value / scaleFactor))
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Source level set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._sourceLevelNumeric.Value = CDec(Math.Min(Math.Max(scaleFactor * Me._testStation.ActiveInstrument.SourceLevel.ActualValue,
                                                                 Me._sourceLevelNumeric.Minimum), Me._sourceLevelNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        scaleFactor = 1000000.0
        Me.PrependStatusMessage("Setting sense current range to {0} A", Me._currentRangeNumeric.Value / scaleFactor)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.SenseCurrentRangeSetter(CSng(Me._currentRangeNumeric.Value / scaleFactor))
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Sense current range set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._currentRangeNumeric.Value = CDec(Math.Min(Math.Max(scaleFactor * Me._testStation.ActiveInstrument.SenseCurrentRange.ActualValue,
                                                                 Me._currentRangeNumeric.Minimum), Me._currentRangeNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Dim impedanceModel As isr.Kte.ImpedanceModel = CType(CType(Me._modelComboBox.SelectedItem, Collections.Generic.KeyValuePair(Of [Enum], String)).Key, isr.Kte.ImpedanceModel)
        Me._testStation.ActiveInstrument.MeasurementModelSetter(impedanceModel)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Measurement model set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._modelComboBox.SelectedItem = CType(Me._testStation.ActiveInstrument.MeasurementModel.ActualValue, isr.Kte.ImpedanceModel).ValueDescriptionPair()
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me._primaryValueTestBoxLabel.Text = isr.Kte.BaseInstrument.PrimaryValueCaption(impedanceModel)
        Me._secondaryValueTestBoxLabel.Text = isr.Kte.BaseInstrument.SecondaryValueCaption(impedanceModel)

        Dim measurementSpeed As isr.Kte.MeasurementSpeed = CType(CType(Me._speedComboBox.SelectedItem, Collections.Generic.KeyValuePair(Of [Enum], String)).Key, isr.Kte.MeasurementSpeed)
        Me._testStation.ActiveInstrument.MeasurementSpeedSetter(measurementSpeed)
        Me._speedComboBox.SelectedItem = CType(Me._testStation.ActiveInstrument.MeasurementSpeed.ActualValue, isr.Kte.MeasurementSpeed).ValueDescriptionPair()

        ' if custom speed, use the specified settings.
        Me.PrependStatusMessage("Setting aperture to {0} NPLC", Me._apertureNumeric.Value)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.ApertureSetter(Me._apertureNumeric.Value)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Aperture set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._apertureNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.Aperture.ActualValue, Me._apertureNumeric.Minimum), Me._apertureNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting delay factor to {0}", Me._delayNumeric.Value)
        Me._testStation.ActiveInstrument.DelayFactorSetter(Me._delayNumeric.Value)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Delay factor set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._delayNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.DelayFactor.ActualValue, Me._delayNumeric.Minimum), Me._delayNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting Filter factor to {0}", Me._filterNumeric.Value)
        Me._testStation.ActiveInstrument.FilterFactorSetter(Me._filterNumeric.Value)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Filter factor set and confirmed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._filterNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.FilterFactor.ActualValue, Me._filterNumeric.Minimum), Me._filterNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Dim cableCorrection As isr.Kte.CorrectedCableLength = CType(CType(Me._cableComboBox.SelectedItem, Collections.Generic.KeyValuePair(Of [Enum], String)).Key, isr.Kte.CorrectedCableLength)
        Me.PrependStatusMessage("Setting cable correction to {0}", cableCorrection)
        Me._testStation.ActiveInstrument.CableCorrectionSetter(cableCorrection)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Cable correction set in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._cableComboBox.SelectedItem = Me._testStation.ActiveInstrument.CableCorrection.ValueDescriptionPair()
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting load compensation to {0}", Me._loadCompensationCheckBox.Checked)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.LoadCompensationEnabledSetter(Me._loadCompensationCheckBox.Checked)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Load compensation set in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._loadCompensationCheckBox.Checked = Me._testStation.ActiveInstrument.LoadCompensationEnabled.ActualValue
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting Open compensation to {0}", Me._openCompensationCheckBox.Checked)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.OpenCompensationEnabledSetter(Me._openCompensationCheckBox.Checked)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Open compensation set in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._openCompensationCheckBox.Checked = Me._testStation.ActiveInstrument.OpenCompensationEnabled.ActualValue
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        Me.PrependStatusMessage("Setting Short compensation to {0}", Me._shortCompensationCheckBox.Checked)
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.ShortCompensationEnabledSetter(Me._shortCompensationCheckBox.Checked)
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Short compensation set in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._shortCompensationCheckBox.Checked = Me._testStation.ActiveInstrument.ShortCompensationEnabled.ActualValue
        Else
            Me.showErrorProvider(Me._applyButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        ' enable measurement if all instrument have their setting
        Me._measureParallelButton.Enabled = Me._testStation.Instruments.AllActual
        Me._measureButton.Enabled = Me._testStation.ActiveInstrument.AllParametersActual

        If Not Me._testStation.Instruments.AllActual Then
            If Me._testStation.ActiveInstrument.AllParametersActual Then
                Me.PrependStatusMessage("Apply other instrumet(s)")
            Else
                Me.PrependStatusMessage("{0} not set", Me._testStation.ActiveInstrument.NonActual)
            End If
        End If

        ' update the instrument status dispaly
        updateInstrumentStatus()

    End Sub

    ''' <summary>
    ''' Updates the instrument status based on having all values applied.
    ''' </summary>
    Private Sub updateInstrumentStatus()

        If Me._testStation.ActiveInstrument.AllParametersActual Then
            Me._instrumentStatusLabel.Text = "READY"
            Me._instrumentStatusLabel.BackColor = Color.LightGreen
        Else
            Me.PrependStatusMessage("Apply required")
            Me._instrumentStatusLabel.Text = "APPLY REQUIRED"
            Me._instrumentStatusLabel.BackColor = Color.Orange
        End If

    End Sub

    ''' <summary>
    ''' Applies settings.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _applyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _applyButton.Click
        Try
            applySettings()
        Catch ex As Exception
            ExceptionDisplay.ProcessException(ex, "Apply Settings Exception.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub measureSingle()

        hideErrorProvider(Me._measureButton)

        Me._primaryValueTestBox.Text = ""
        Me._secondaryValueTestBox.Text = ""
        Me._measurementStatusTextBox.Text = ""

        Me.PrependStatusMessage("Measuring {0} impedance model with {1} speed",
                                CType(Me._testStation.ActiveInstrument.MeasurementModel.ActualValue, ImpedanceModel),
                                CType(Me._testStation.ActiveInstrument.MeasurementSpeed.CacheValue, MeasurementSpeed))

        ' disable parallel mode for single measurements.
        If Me._testStation.ParallelImpedanceEnabled Then
            Me._testStation.ParallelImpedanceEnabled = False
        End If

        Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
        Me._testStation.ActiveInstrument.MeasureImpedance(10000, 10)
        Dim measurementTime As Double = 1000 * timer.ElapsedTicks / Stopwatch.Frequency

        If Me._testStation.ActiveInstrument.Impedance.HasValue Then
            Me.PrependStatusMessage("Impedance measured in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Me._primaryValueTestBox.Text = Me._testStation.ActiveInstrument.Impedance.PrimaryValue.ToString(Me._displayFormat, Globalization.CultureInfo.CurrentCulture)
            Me._secondaryValueTestBox.Text = Me._testStation.ActiveInstrument.Impedance.SecondaryValue.ToString(Me._displayFormat, Globalization.CultureInfo.CurrentCulture)
        Else
            Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            Return
        End If

        ' update actual Status settings values.
        Me.PrependStatusMessage("Getting Measuerement Status")
        Try
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.ActiveInstrument.MeasurementStatusGetter()
            If Me._testStation.ActiveInstrument.LastOperationSuccess Then
                Me.PrependStatusMessage("Got measurement Status in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
                Me._measurementStatusTextBox.Text = "0x" & Me._testStation.ActiveInstrument.MeasurementStatus.ToString("X8", Globalization.CultureInfo.CurrentCulture)
                If isr.Kte.BaseInstrument.IsMeasurementFailed(Me._testStation.ActiveInstrument.MeasurementStatus) Then
                    Me._measurementStatusTextBox.BackColor = Color.Red
                Else
                    Me._measurementStatusTextBox.BackColor = Color.LightGreen
                End If
            Else
                Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            End If
        Catch ex As Exception
            Me.showErrorProvider(Me._measureButton, "Exception occurred getting measurement Status; details: {0}", ex)
        End Try

        ' update actual speed settings values.
        Me.PrependStatusMessage("Getting Measuerement Speed")
        Try
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.ActiveInstrument.MeasurementSpeedGetter()
            If Me._testStation.ActiveInstrument.LastOperationSuccess Then
                Me.PrependStatusMessage("Got measurement speed in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
                If [Enum].IsDefined(GetType(isr.Kte.MeasurementSpeed), Me._testStation.ActiveInstrument.MeasurementSpeed.ActualValue) Then
                    Me._speedComboBox.SelectedItem = CType(Me._testStation.ActiveInstrument.MeasurementSpeed.ActualValue, isr.Kte.MeasurementSpeed).ValueDescriptionPair()
                Else
                    Me.showErrorProvider(Me._measureButton, "Measurement speed value of {0} is not defined", Me._testStation.ActiveInstrument.MeasurementSpeed.ActualValue)
                End If
            Else
                Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            End If
        Catch ex As Exception
            Me.showErrorProvider(Me._measureButton, "Exception occurred getting measurement speed; details: {0}", ex)
        End Try

        ' update actual Model settings values.
        Me.PrependStatusMessage("Getting Measuerement Model")
        Try
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.ActiveInstrument.MeasurementModelGetter()
            If Me._testStation.ActiveInstrument.LastOperationSuccess Then
                Me.PrependStatusMessage("Got measurement Model in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
                If [Enum].IsDefined(GetType(isr.Kte.ImpedanceModel), Me._testStation.ActiveInstrument.MeasurementModel.ActualValue) Then
                    Me._modelComboBox.SelectedItem = CType(Me._testStation.ActiveInstrument.MeasurementModel.ActualValue, isr.Kte.ImpedanceModel).ValueDescriptionPair()
                Else
                    Me.showErrorProvider(Me._measureButton, "Measurement Model value of {0} is not defined", Me._testStation.ActiveInstrument.MeasurementModel.ActualValue)
                End If
            Else
                Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            End If
        Catch ex As Exception
            Me.showErrorProvider(Me._measureButton, "Exception occurred getting measurement model; details: {0}", ex)
        End Try

        ' update actual speed settings values.
        Me.PrependStatusMessage("Getting aperture")
        Try
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.ActiveInstrument.ApertureGetter()
            If Me._testStation.ActiveInstrument.LastOperationSuccess Then
                Me.PrependStatusMessage("Got aperture in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
                ' limit because the instrument could return values that are slightly out of range.
                Me._apertureNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.Aperture.ActualValue, Me._apertureNumeric.Minimum), Me._apertureNumeric.Maximum))
            Else
                Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            End If
        Catch ex As Exception
            Me.showErrorProvider(Me._measureButton, "Exception occurred getting apperture; details: {0}", ex)
        End Try

        Me.PrependStatusMessage("Getting Delay Factor")
        Try
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.ActiveInstrument.DelayFactorGetter()
            If Me._testStation.ActiveInstrument.LastOperationSuccess Then
                Me.PrependStatusMessage("Got Delay Factor in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
                ' limit because the instrument could return values that are slightly out of range.
                Me._delayNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.DelayFactor.ActualValue, Me._delayNumeric.Minimum), Me._delayNumeric.Maximum))
            Else
                Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            End If
        Catch ex As Exception
            Me.showErrorProvider(Me._measureButton, "Exception occurred getting Delay Factor; details: {0}", ex)
        End Try

        Me.PrependStatusMessage("Getting Filter Factor")
        Try
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.ActiveInstrument.FilterFactorGetter()
            If Me._testStation.ActiveInstrument.LastOperationSuccess Then
                Me.PrependStatusMessage("Got Filter Factor in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
                ' limit because the instrument could return values that are slightly out of range.
                Me._filterNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.FilterFactor.ActualValue, Me._filterNumeric.Minimum), Me._filterNumeric.Maximum))
            Else
                Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
            End If
        Catch ex As Exception
            Me.showErrorProvider(Me._measureButton, "Exception occurred getting Filter Factor; details: {0}", ex)
        End Try
        Me.PrependStatusMessage("Impedance measured in {0:0.00} ms", measurementTime)

        ' add the measurements to the list.
        createMeasurementsListView()

    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _measureButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _measureButton.Click
        Dim parallelEnabled As Boolean = Me._measureParallelButton.Enabled
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._measureButton.Enabled = False
            Me._measureParallelButton.Enabled = False
            measureSingle()
        Catch ex As Exception
            ExceptionDisplay.ProcessException(ex, "Measure Single Exception.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        Finally
            ' delay a bit. 
            Dim stopWatch As New Diagnostics.Stopwatch
            stopWatch.Start()
            Do Until stopWatch.ElapsedMilliseconds >= 250
                Application.DoEvents()
            Loop
            Me._measureParallelButton.Enabled = parallelEnabled
            Me._measureButton.Enabled = True
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Measures impledance on all instruments and returns control once all data are collected into the instruments.
    ''' </summary>
    Private Sub measureParallel()

        hideErrorProvider(Me._measureParallelButton)

        Me._primaryValueTestBox.Text = ""
        Me._secondaryValueTestBox.Text = ""
        Me._measurementStatusTextBox.Text = ""

        Me.PrependStatusMessage("Initiating parallel measurements")

        Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
        Me._testStation.Instruments.MeasureImpedance(3000, 10)
        Dim measurementTime As Double = 1000 * timer.ElapsedTicks / Stopwatch.Frequency
        Me.PrependStatusMessage("Measurement returned after {0:0.0} ms", measurementTime)

        If Me._testStation.LastOperationFailed Then
            Me.showErrorProvider(Me._measureParallelButton, Me._testStation.LastStatusDetails)
            Return
        End If

        If Me._testStation.ActiveInstrument.Impedance.HasValue Then
            Me._primaryValueTestBox.Text = Me._testStation.ActiveInstrument.Impedance.PrimaryValue.ToString(Me._displayFormat, Globalization.CultureInfo.CurrentCulture)
            Me._secondaryValueTestBox.Text = Me._testStation.ActiveInstrument.Impedance.SecondaryValue.ToString(Me._displayFormat, Globalization.CultureInfo.CurrentCulture)
        ElseIf Me._testStation.ActiveInstrument.Enabled Then
            Me.showErrorProvider(Me._measureParallelButton, "Instrument {0} have no impedance value", Me._testStation.ActiveInstrument.UniqueKey)
            Return
        End If

        Me.PrependStatusMessage("Getting instruments status")
        Me._testStation.Instruments.FetchInstrumentsStatus()
        If Me._testStation.LastOperationFailed Then
            Me.showErrorProvider(Me._measureParallelButton, Me._testStation.LastStatusDetails)
            Return
        End If

        If Me._testStation.ActiveInstrument.Enabled Then
            Me._measurementStatusTextBox.Text = "0x" & Me._testStation.ActiveInstrument.MeasurementStatus.ToString("X8", Globalization.CultureInfo.CurrentCulture)
            If isr.Kte.BaseInstrument.IsMeasurementFailed(Me._testStation.ActiveInstrument.MeasurementStatus) Then
                Me._measurementStatusTextBox.BackColor = Color.Red
            Else
                Me._measurementStatusTextBox.BackColor = Color.LightGreen
            End If
        Else
            Me._measurementStatusTextBox.Text = ""
            Me._measurementStatusTextBox.BackColor = Color.LightGray
        End If

        Me.PrependStatusMessage("Getting instruments aperture")
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.Instruments.FetchInstrumentsAperture()
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Got apertures in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            ' limit because the instrument could return values that are slightly out of range.
            Me._apertureNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.Aperture.ActualValue, Me._apertureNumeric.Minimum), Me._apertureNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
        End If

        Me.PrependStatusMessage("Getting Instruments Delay Factors")
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.Instruments.FetchInstrumentsDelayFactor()
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Got Delay Factors in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            ' limit because the instrument could return values that are slightly out of range.
            Me._delayNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.DelayFactor.ActualValue, Me._delayNumeric.Minimum), Me._delayNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
        End If

        Me.PrependStatusMessage("Getting Instruments Filter Factors")
        timer = Diagnostics.Stopwatch.StartNew
        Me._testStation.Instruments.FetchInstrumentsFilterFactor()
        If Me._testStation.ActiveInstrument.LastOperationSuccess Then
            Me.PrependStatusMessage("Got Filter Factors in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            ' limit because the instrument could return values that are slightly out of range.
            Me._filterNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.FilterFactor.ActualValue, Me._filterNumeric.Minimum), Me._filterNumeric.Maximum))
        Else
            Me.showErrorProvider(Me._measureButton, Me._testStation.ActiveInstrument.LastStatusDetails)
        End If

        Me.PrependStatusMessage("Impedance measured in {0:0.00} ms", measurementTime)

        ' add the measurements to the list.
        createMeasurementsListView()

    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _measureParallelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _measureParallelButton.Click
        Dim singleEnabled As Boolean = Me._measureButton.Enabled
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._measureButton.Enabled = False
            Me._measureParallelButton.Enabled = False
            measureParallel()
        Catch ex As Exception
            ExceptionDisplay.ProcessException(ex, "Measure Parallel Exception.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        Finally
            ' delay a bit. 
            Dim stopWatch As New Diagnostics.Stopwatch
            stopWatch.Start()
            Do Until stopWatch.ElapsedMilliseconds >= 250
                Application.DoEvents()
            Loop
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me._measureParallelButton.Enabled = True
            Me._measureButton.Enabled = singleEnabled
        End Try
    End Sub

#End Region

#Region " INSTRUMENT CONTROLS EVENTS "

    ''' <summary>
    ''' Updates the instruments list to display all instruments.
    ''' </summary>
    Private Sub updateInstrumentsList()

        Me._identifierTextBox.Text = ""

        ' enable if parallel mode is enabled. 
        Me._measureParallelButton.Enabled = Me._testStation.ChannelCount > 1 ' ParallelImpedanceEnabled

        ' update the instrument list
        Me._availabelInstrumentsListBox.Items.Clear()
        Me._availabelInstrumentsListBox.Items.AddRange(Me._testStation.Instruments.ToArray())
        Me._availabelInstrumentsListBox.SelectedItem = Me._testStation.ActiveInstrument

        ' select the active instrument so as to display the status.
        Me.selectActiveInstrument(Me._testStation.ActiveInstrument.UniqueId)

        If Me._testStation.Instruments.Count >= Me._testStation.ChannelCount Then
            Me._addInstrumentButton.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' Adds an instrument.
    ''' </summary>
    Private Sub addInstrument()

        hideErrorProvider(Me._addInstrumentButton)

        ' display any left-over messages
        Me.PrependStatusMessage(Me._testStation.MessageQueue.DequeueAll)

        ' add new or select an existing instrument.
        If Me._testStation.AddInstrument(Me._instrumentFamilyNameTextBox.Text.Trim(), CInt(Me._instrumentNumberNumeric.Value), Me._forceMultiCvu.Checked) Then

            ' display results.
            Me.PrependStatusMessage(Me._testStation.MessageQueue.DequeueAll)

            Me.updateInstrumentsList()

        Else

            Me.showErrorProvider(Me._addInstrumentButton, Me._testStation.MessageQueue.DequeueAll)

        End If

    End Sub

    ''' <summary>
    ''' Selects the instrument.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _addInstrumentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _addInstrumentButton.Click
        Try
            addInstrument()
        Catch ex As Exception
            ExceptionDisplay.ProcessException(ex, "Add Instrument Exception.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        End Try
    End Sub

    Private Sub updateActiveInstrumentSettings()

        hideErrorProvider(Me._selectInstrumentButton)

        Me._enabledCheckBox.Checked = Me._testStation.ActiveInstrument.Enabled

        Me._sourceBiasNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.SourceBias.CacheValue,
                                                             Me._sourceBiasNumeric.Minimum), Me._sourceBiasNumeric.Maximum))

        Me._frequencyNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.SourceFrequency.CacheValue,
                                                            Me._frequencyNumeric.Minimum), Me._frequencyNumeric.Maximum))

        Dim scaleFactor As Double = 1000
        Me._sourceLevelNumeric.Value = CDec(Math.Min(Math.Max(scaleFactor * Me._testStation.ActiveInstrument.SourceLevel.CacheValue,
                                                             Me._sourceLevelNumeric.Minimum), Me._sourceLevelNumeric.Maximum))

        scaleFactor = 1000000.0
        Me._currentRangeNumeric.Value = CDec(Math.Min(Math.Max(scaleFactor * Me._testStation.ActiveInstrument.SenseCurrentRange.CacheValue,
                                                             Me._currentRangeNumeric.Minimum), Me._currentRangeNumeric.Maximum))

        Me._modelComboBox.SelectedItem = CType(Me._testStation.ActiveInstrument.MeasurementModel.CacheValue, isr.Kte.ImpedanceModel).ValueDescriptionPair()
        Dim impedanceModel As isr.Kte.ImpedanceModel = CType(Me._testStation.ActiveInstrument.MeasurementModel.CacheValue, Kte.ImpedanceModel)
        Me._primaryValueTestBoxLabel.Text = isr.Kte.BaseInstrument.PrimaryValueCaption(impedanceModel)
        Me._secondaryValueTestBoxLabel.Text = isr.Kte.BaseInstrument.SecondaryValueCaption(impedanceModel)

        Dim measurementSpeed As isr.Kte.MeasurementSpeed = CType(Me._testStation.ActiveInstrument.MeasurementSpeed.CacheValue, isr.Kte.MeasurementSpeed)
        Me._speedComboBox.SelectedItem = measurementSpeed.ValueDescriptionPair()

        Me._apertureNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.Aperture.CacheValue, Me._apertureNumeric.Minimum), Me._apertureNumeric.Maximum))
        Me._delayNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.DelayFactor.CacheValue, Me._delayNumeric.Minimum), Me._delayNumeric.Maximum))
        Me._filterNumeric.Value = CDec(Math.Min(Math.Max(Me._testStation.ActiveInstrument.FilterFactor.CacheValue, Me._filterNumeric.Minimum), Me._filterNumeric.Maximum))

        Me._cableComboBox.SelectedItem = Me._testStation.ActiveInstrument.CableCorrection.ValueDescriptionPair()
        Me._loadCompensationCheckBox.Checked = Me._testStation.ActiveInstrument.LoadCompensationEnabled.CacheValue
        Me._openCompensationCheckBox.Checked = Me._testStation.ActiveInstrument.OpenCompensationEnabled.CacheValue
        Me._shortCompensationCheckBox.Checked = Me._testStation.ActiveInstrument.ShortCompensationEnabled.CacheValue

        ' enable measurement if all instrument have their setting
        Me._measureParallelButton.Enabled = Me._testStation.Instruments.AllActual
        Me._measureButton.Enabled = Me._testStation.ActiveInstrument.AllParametersActual

        ' update the instrument status display
        updateInstrumentStatus()

    End Sub

    Private Sub selectActiveInstrument(ByVal index As Integer)
        If index >= 0 AndAlso Me._testStation.Instruments IsNot Nothing AndAlso Me._testStation.Instruments.Count > index Then
            Me._testStation.Instruments.SelectActiveInstrument(index)
            Me._identifierTextBox.Text = Me._testStation.ActiveInstrument.UniqueId.ToString(Globalization.CultureInfo.CurrentCulture)
            Me._impedanceGroupBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} CONFIGURATION:", Me._testStation.ActiveInstrument.UniqueKey)
            Me._measureButton.Text = "&MEASURE " & Me._testStation.ActiveInstrument.UniqueKey
            Me._impedanceGroupBox.Enabled = True
            updateActiveInstrumentSettings()
        End If
    End Sub

    Private Sub _availabelInstrumentsListBox_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _availabelInstrumentsListBox.DoubleClick
        If Me._availabelInstrumentsListBox.SelectedItem IsNot Nothing AndAlso Me._availabelInstrumentsListBox.SelectedIndex >= 0 Then
            selectActiveInstrument(CType(Me._availabelInstrumentsListBox.SelectedItem, IInstrument).Index)
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _selectInstrumentButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _selectInstrumentButton.Click
        Try
            hideErrorProvider(Me._selectInstrumentButton)
            If Me._availabelInstrumentsListBox.Items IsNot Nothing AndAlso Me._availabelInstrumentsListBox.Items.Count > 0 AndAlso
                Me._availabelInstrumentsListBox.SelectedItem IsNot Nothing Then
                Dim item As isr.Kte.IInstrument = CType(Me._availabelInstrumentsListBox.SelectedItem, IInstrument)
                selectActiveInstrument(item.Index)
            Else
                Me.showErrorProvider(Me._selectInstrumentButton, "Instruments not available.")
            End If
        Catch ex As Exception
            ExceptionDisplay.ProcessException(ex, "Select Instrument Exception.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        End Try
    End Sub

#End Region

#Region " STATION CONTROLS EVENTS "

    ''' <summary>
    ''' Gets or sets reference to the test station.
    ''' </summary>
    Private _testStation As isr.Kte.ITestStation

    ''' <summary>
    ''' Selects and initilizes the test station.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _initializeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _initializeButton.Click

        hideErrorProvider(Me._closeStationButton)
        hideErrorProvider(Me._initializeButton)

        Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.PrependStatusMessage("Initializing test station")
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.InitializeTestStation()
            If Me._testStation.LastOperationFailed Then
                Me.showErrorProvider(Me._initializeButton, "Failed initializing test station; {0}", Me._testStation.LastStatusDetails())
                Return
            End If
            Me.PrependStatusMessage("Station initialized in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)

            ' override user select
            Me._parallelCheckBox.Checked = Me._parallelCheckBox.Checked AndAlso Me._testStation.SupportsParallel

            If Me._parallelCheckBox.Checked OrElse Me._forceMultiCvu.Checked Then
                ' enable parallel mode
                Me.PrependStatusMessage("Enabling Parallel CVU")
                Me._testStation.ParallelImpedanceEnabled = True
                If Me._testStation.LastOperationFailed Then
                    Me.showErrorProvider(Me._initializeButton, "Failed enabling parallel testing. Failure ignored; {0}", Me._testStation.LastStatusDetails())
                ElseIf Not Me._testStation.ParallelImpedanceEnabled Then
                    Me.showErrorProvider(Me._initializeButton, "Failed enabling parallel testing -- Value not set. Failure ignored.")
                End If
            Else
                Me.PrependStatusMessage("Disabling Parallel CVU")
                Me._testStation.ParallelImpedanceEnabled = False
                If Me._testStation.LastOperationFailed Then
                    Me.showErrorProvider(Me._initializeButton, "Failed disabling parallel testing. Failure ignored; {0}", Me._testStation.LastStatusDetails())
                ElseIf Me._testStation.ParallelImpedanceEnabled Then
                    Me.showErrorProvider(Me._initializeButton, "Failed disabling parallel testing -- Value not cleared. Failure ignored.")
                End If
            End If

            If Me._testStation.ParallelImpedanceEnabled Then
                Me._addInstrumentButton.Text = "&ADD"
                Me._selectInstrumentButton.Enabled = True
            Else
                Me._addInstrumentButton.Text = "&SELECT"
                Me._selectInstrumentButton.Enabled = False
            End If

            ' get the channel count and set the maximum count
            Me._instrumentNumberNumeric.Maximum = Me._testStation.ChannelCount
            Me._channelCountNumeric.Value = Me._testStation.ChannelCount
            If Me._testStation.LastOperationStatus = OperationStatus.MessageAvailable Then
                Me.PrependMessage(Me._testStation.MessageQueue.DequeueAll)
            End If

            Me._instrumentGroupBox.Enabled = True
            Me._impedanceGroupBox.Enabled = False
            Me._impedanceGroupBox.Text = "IMPEDANCE INSTRUMENT CONFIGURATION:"
            Me._measureButton.Enabled = False
            Me._measureParallelButton.Enabled = False
            Me._interactiveGroupBox.Enabled = Not Me._lowLevelControlRadioButton.Checked

        Catch ex As Exception

            Me.showErrorProvider(Me._initializeButton, "Exception occurred initializing")
            ExceptionDisplay.ProcessException(ex, "Exception occurred initializing.", isr.WindowsForms.ExceptionDisplayButtons.Continue)

        Finally

            ' update the instrument list
            Me.updateInstrumentsList()

            ' disable the control buttons
            Me._usingDevicesCheckBox.Enabled = False
            Me._externalControlInterfaceRadioButton.Enabled = False
            Me._lowLevelControlRadioButton.Enabled = False
            Me._initializeButton.Enabled = False
            Me._parallelCheckBox.Enabled = False
            Me._resetHardwareButton.Enabled = False
            Me._forceMultiCvu.Enabled = False

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Enables or disables parallel CVU.
    ''' </summary>
    Private Sub enableParallelImpedance(ByVal value As Boolean)
        If value Then
            Me._measureParallelButton.Text = "MEASURE PARALLEL CVU"
        Else
            Me._measureParallelButton.Text = "MEASURE MULTI CVU"
        End If
    End Sub

    Private Sub _forceMultiCvu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _forceMultiCvu.Click
        enableParallelImpedance(Me._parallelCheckBox.Checked OrElse Me._forceMultiCvu.Checked)
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Function _openTestStation() As Boolean

        hideErrorProvider(Me._closeStationButton)
        hideErrorProvider(Me._openStationButton)
        hideErrorProvider(Me._initializeButton)

        Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' select the control mode.
            If Me._lowLevelControlRadioButton.Checked Then
                Me._testStation = New isr.Kte.LowLevelStation
            Else
                Me._testStation = New isr.Kte.MessageBasedStation
            End If

            ' select the test station.
            Me.PrependStatusMessage("Selecting test station")
            timer = Diagnostics.Stopwatch.StartNew
            If Not Me._testStation.OpenTestStation(10000) Then
                Me.showErrorProvider(Me._openStationButton, "Failed opening test station; {0}", Me._testStation.LastStatusDetails())
                Return False
            End If
            Me.PrependStatusMessage("Station opened in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)

            Me._testStation.ParallelImpedanceEnabled = Me._parallelCheckBox.Checked
            Me._initializeButton.Enabled = True
            Me._interactiveGroupBox.Enabled = Not Me._lowLevelControlRadioButton.Checked

        Catch ex As Exception

            Me.showErrorProvider(Me._openStationButton, "Exception occurred opening test station")
            ExceptionDisplay.ProcessException(ex, "Exception occurred opening test station.", isr.WindowsForms.ExceptionDisplayButtons.Continue)

        Finally

            ' disable the control buttons
            Me._usingDevicesCheckBox.Enabled = Not Me._testStation.IsOpen
            Me._externalControlInterfaceRadioButton.Enabled = Not Me._testStation.IsOpen
            Me._lowLevelControlRadioButton.Enabled = Not Me._testStation.IsOpen
            Me._parallelCheckBox.Enabled = Not Me._testStation.IsOpen
            Me._resetHardwareButton.Enabled = Not Me._testStation.IsOpen
            Me._forceMultiCvu.Enabled = Not Me._testStation.IsOpen
            Me._closeStationButton.Enabled = Me._testStation.IsOpen
            Me._openStationButton.Enabled = Not Me._testStation.IsOpen

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _openStationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _openStationButton.Click

        Me._openTestStation()

    End Sub

    Private Sub _parallelCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _parallelCheckBox.Click
        enableParallelImpedance(Me._parallelCheckBox.Checked OrElse Me._forceMultiCvu.Checked)
    End Sub

    ''' <summary>
    ''' Releases the test station.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _closeStationButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _closeStationButton.Click

        hideErrorProvider(Me._closeStationButton)
        hideErrorProvider(Me._openStationButton)
        hideErrorProvider(Me._initializeButton)
        hideErrorProvider(Me._applyButton)
        Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' select the test station.
            Me.PrependStatusMessage("Releasing test station")
            timer = Diagnostics.Stopwatch.StartNew
            Me._testStation.CloseTestStation()
            If Me._testStation.LastOperationSuccess Then
                Me.PrependStatusMessage("Station released in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)
            Else
                Me.showErrorProvider(Me._closeStationButton, "Failed releasing station; {0}", Me._testStation.LastStatusDetails())
            End If

        Catch ex As Exception

            Me.showErrorProvider(Me._closeStationButton, "Failed releasing station")
            ExceptionDisplay.ProcessException(ex, "Exception occurred closing test station.", isr.WindowsForms.ExceptionDisplayButtons.Continue)

        Finally

            ' remove the test station as indication that things are closed
            Me._testStation.Dispose()
            Me._testStation = Nothing

            Me._instrumentGroupBox.Enabled = False
            Me._impedanceGroupBox.Enabled = False
            Me._measureButton.Enabled = False
            Me._measureParallelButton.Enabled = False

            ' re-able the control buttons
            Me._usingDevicesCheckBox.Enabled = True
            Me._externalControlInterfaceRadioButton.Enabled = True
            Me._lowLevelControlRadioButton.Enabled = True
            Me._parallelCheckBox.Enabled = True
            Me._resetHardwareButton.Enabled = True
            Me._forceMultiCvu.Enabled = True
            Me._closeStationButton.Enabled = False
            Me._interactiveGroupBox.Enabled = False

            Me._initializeButton.Enabled = False
            Me._closeStationButton.Enabled = False
            Me._openStationButton.Enabled = True

            ' Restore form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _resetHardwareButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _resetHardwareButton.Click
        Try
            hideErrorProvider(Me._resetHardwareButton)
            Me.PrependStatusMessage("Resetting Test Station. Please wait...")
            If Not isr.Kte.BaseTestStation.ExecuteResetProcess(10000) Then
                showErrorProvider(Me._resetHardwareButton, "Failed resetting test station.")
            End If
        Catch ex As Exception
            showErrorProvider(Me._resetHardwareButton, "Exception occurred resetting hardware.")
            ExceptionDisplay.ProcessException(ex, "Exception occurred resetting hardware.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _sendReceiveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sendReceiveButton.Click
        Try
            hideErrorProvider(Me._sendReceiveButton)
            If Not String.IsNullOrWhiteSpace(Me._commandComboBox.Text) Then
                Me._replyTextBox.Text = ""
                Me._speedTextBox.Text = ""
                isr.Kte.Kxci.SafeNativeMethods.StatusByteSetter(64)
                Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
                timer = Diagnostics.Stopwatch.StartNew
                If Me._usingDevicesCheckBox.Checked Then
                    Dim result As New Kxci.QueryResultInfo(Kxci.SafeNativeMethods.SendReceive(1024, Me._commandComboBox.Text))
                    Me._replyTextBox.Text = result.Reading
                End If
                If isr.Kte.Kxci.SafeNativeMethods.LastOperationFailed Then
                    showErrorProvider(Me._sendReceiveButton, "Failed receiving. {0}", isr.Kte.Kxci.SafeNativeMethods.LastStatusDetails)
                Else
                    Me._speedTextBox.Text = (1000 * timer.ElapsedTicks / Stopwatch.Frequency).ToString("0.00", Globalization.CultureInfo.CurrentCulture) & "ms"
                End If
                ' update the status byte.
                updateSrqDisplay()
            End If
        Catch ex As Exception
            showErrorProvider(Me._sendReceiveButton, "Exception occurred sending/receiving.")
            ExceptionDisplay.ProcessException(ex, "Exception occurred sending/receiving.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        End Try
    End Sub

    Private Sub updateSrqDisplay()
        ' update the status byte.
        Me._toolTip.SetToolTip(Me._srqLabel, "SRQ: ox" & Me._testStation.StatusByteGetter().ToString("X2", Globalization.CultureInfo.CurrentCulture))
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _srqLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _srqLabel.Click
        Try
            updateSrqDisplay()
        Catch ex As Exception
            ExceptionDisplay.ProcessException(ex, "Update SRQ Exception.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        End Try
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _startHostButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _startHostButton.Click
        hideErrorProvider(Me._startHostButton)
        Try
            isr.Kte.MessageBasedStation.HostProcessFilePathName = My.Settings.HostProcessFilePathName

            If MessageBasedStation.OpenHostProcess(5000, 200) Then

                ' allow a bit of time for the host process to get ready
                Threading.Thread.Sleep(1000)

                ' open the test station.
                Me._openTestStation()

            Else
                showErrorProvider(Me._startHostButton, "Failed starting host process")
                Me._messagesBox.PrependMessage("Failed starting host process")
            End If

        Catch ex As Exception
            showErrorProvider(Me._startHostButton, "Exception occurred starting host process")
            ExceptionDisplay.ProcessException(ex, "Exception occurred starting host process.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
        End Try
    End Sub

#End Region

#Region " UI CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Updates the maximum number of instruments. Allows to manually force the instrument count whenever the 
    ''' 'channels?' command fails.
    ''' </summary>
    Private Sub _channelCountNumeric_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _channelCountNumeric.ValueChanged
        Me._instrumentNumberNumeric.Maximum = Me._channelCountNumeric.Value
    End Sub

    Private Sub _externalControlInterfaceRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _externalControlInterfaceRadioButton.CheckedChanged
        enableLaunchingHostProcess()
    End Sub

    Private Sub enableLaunchingHostProcess()

        ' check the KXCI status
        If Me._externalControlInterfaceRadioButton.Checked Then
            If MessageBasedStation.HasHostProcessStarted Then
                Me._startHostButton.Enabled = False
                If Me._testStation Is Nothing Then
                    Me._openStationButton.Enabled = True
                End If
            Else
                Me._startHostButton.Enabled = True
                Me._openStationButton.Enabled = False
            End If
        End If
    End Sub

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _refreshTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _refreshTimer.Tick

        Dim lastStatusByte As Integer
        Dim lastSocketStatus As Integer
        Dim lastStatus As Integer

        Try

            If Me._lowLevelControlRadioButton.Checked Then
                lastStatusByte = isr.Kte.Lpt.SafeNativeMethods.LastStatusByte
                lastStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
                lastSocketStatus = isr.Kte.Lpt.SafeNativeMethods.LastSocketStatus
            Else
                lastStatusByte = isr.Kte.Kxci.SafeNativeMethods.LastStatusByte
                lastStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
                lastSocketStatus = isr.Kte.Kxci.SafeNativeMethods.LastSocketStatus
            End If

            Me._statusByteLabel.Text = "0x" & lastStatusByte.ToString("X2", Globalization.CultureInfo.CurrentCulture)
            Me._functionStatusLabel.Text = lastStatus.ToString(Globalization.CultureInfo.CurrentCulture)
            Me._socketStatusLabel.Text = lastSocketStatus.ToString(Globalization.CultureInfo.CurrentCulture)

            If (lastStatusByte And 64) = 64 Then
                Me._srqLabel.BackColor = Color.LightGreen
            Else
                Me._srqLabel.BackColor = Drawing.SystemColors.Control
            End If

            If (lastStatusByte And 16) = 16 Then
                Me._busyStatusLabel.BackColor = Color.LightGreen
            Else
                Me._busyStatusLabel.BackColor = Drawing.SystemColors.Control
            End If
            If (lastStatusByte And 2) = 2 Then
                Me._errorAvailableLabel.BackColor = Color.LightGreen
            Else
                Me._errorAvailableLabel.BackColor = Drawing.SystemColors.Control
            End If
            If (lastStatusByte And 1) = 1 Then
                Me._messageAvailabelLabel.BackColor = Color.LightGreen
            Else
                Me._messageAvailabelLabel.BackColor = Drawing.SystemColors.Control
            End If

            enableLaunchingHostProcess()

            Application.DoEvents()

        Catch ex As Exception
            Me._refreshTimer.Enabled = False
            ExceptionDisplay.ProcessException(ex, "Timer Exception.", isr.WindowsForms.ExceptionDisplayButtons.Continue)
            Me._refreshTimer.Enabled = True
        End Try

    End Sub

    ''' <summary>
    ''' Updates using hardware.
    ''' </summary>
    Private Sub _usingDevicesCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _usingDevicesCheckBox.CheckedChanged
        My.Settings.UsingDevices = Me._usingDevicesCheckBox.Checked
        enableLaunchingHostProcess()
    End Sub

#End Region

#Region " INSTRUMENT SETTINGS CONTROLS "

    Private Sub _apertureNumeric_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _apertureNumeric.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.Aperture.CacheValue = Me._apertureNumeric.Value

            ' update the instrument status dispaly
            updateInstrumentStatus()
        End If
    End Sub

    ''' <summary>
    ''' Updates the cached value.
    ''' </summary>
    Private Sub _cableComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _cableComboBox.SelectedIndexChanged

        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Dim cableCorrection As isr.Kte.CorrectedCableLength = CType(CType(Me._cableComboBox.SelectedItem, Collections.Generic.KeyValuePair(Of [Enum], String)).Key, isr.Kte.CorrectedCableLength)
            Me._testStation.ActiveInstrument.CableCorrection = cableCorrection

            ' update the instrument status dispaly
            updateInstrumentStatus()
        End If

    End Sub

    Private Sub _currentRangeNumeric_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _currentRangeNumeric.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.SenseCurrentRange.CacheValue = Me._currentRangeNumeric.Value
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _delayNumeric_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _delayNumeric.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.DelayFactor.CacheValue = Me._delayNumeric.Value
            updateInstrumentStatus()
        End If
    End Sub

    ''' <summary>
    ''' Updates the enabled status.
    ''' </summary>
    Private Sub _enabledCheckBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _enabledCheckBox.Validating
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me.PrependStatusMessage("Setting instrument enabled {0}", Me._enabledCheckBox.Checked)
            Me._testStation.ActiveInstrument.Enabled = Me._enabledCheckBox.Checked
        End If
    End Sub

    Private Sub _frequencyNumeric_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _frequencyNumeric.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.SourceFrequency.CacheValue = Me._frequencyNumeric.Value
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _filterNumeric_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _filterNumeric.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.FilterFactor.CacheValue = Me._filterNumeric.Value
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _loadCompensationCheckBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _loadCompensationCheckBox.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.LoadCompensationEnabled.CacheValue = Me._loadCompensationCheckBox.Checked
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _modelComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _modelComboBox.SelectedIndexChanged
        Me._toolTip.SetToolTip(Me._modelComboBox, Me._modelComboBox.Text)
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Dim impedanceModel As isr.Kte.ImpedanceModel = CType(CType(Me._modelComboBox.SelectedItem, Collections.Generic.KeyValuePair(Of [Enum], String)).Key, isr.Kte.ImpedanceModel)
            Me._testStation.ActiveInstrument.MeasurementModel.CacheValue = impedanceModel
            ' update the instrument status dispaly
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _openCompensationCheckBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _openCompensationCheckBox.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.OpenCompensationEnabled.CacheValue = Me._openCompensationCheckBox.Checked
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _shortCompensationCheckBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _shortCompensationCheckBox.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.ShortCompensationEnabled.CacheValue = Me._shortCompensationCheckBox.Checked
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _sourceBiasNumeric_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sourceBiasNumeric.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.SourceBias.CacheValue = Me._sourceBiasNumeric.Value
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _sourceLevelNumeric_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _sourceLevelNumeric.Validated
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.SourceLevel.CacheValue = Me._sourceLevelNumeric.Value
            updateInstrumentStatus()
        End If
    End Sub

    Private Sub _speedComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _speedComboBox.SelectedIndexChanged
        Me._toolTip.SetToolTip(Me._speedComboBox, Me._speedComboBox.Text)
        Dim measurementSpeed As isr.Kte.MeasurementSpeed = CType(CType(Me._speedComboBox.SelectedItem, Collections.Generic.KeyValuePair(Of [Enum], String)).Key, isr.Kte.MeasurementSpeed)
        If measurementSpeed = measurementSpeed.Custom Then
            Me._apertureNumeric.ReadOnly = False
            Me._delayNumeric.ReadOnly = False
            Me._filterNumeric.ReadOnly = False
        Else
            Me._apertureNumeric.ReadOnly = True
            Me._delayNumeric.ReadOnly = True
            Me._filterNumeric.ReadOnly = True
        End If
        If Me._testStation IsNot Nothing AndAlso Me._testStation.ActiveInstrument IsNot Nothing Then
            Me._testStation.ActiveInstrument.MeasurementSpeed.CacheValue = measurementSpeed
            ' update the instrument status dispaly
            updateInstrumentStatus()
        End If
    End Sub

#End Region

End Class

