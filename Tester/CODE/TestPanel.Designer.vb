﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class TestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.onDisposeManagedResources()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TestPanel))
        Me._statusStrip = New System.Windows.Forms.StatusStrip
        Me._statusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me._statusByteLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me._functionStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me._socketStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me._messagesBox = New isr.Controls.MessagesBox
        Me._tabControl = New System.Windows.Forms.TabControl
        Me._stationTabPage = New System.Windows.Forms.TabPage
        Me._stationLayout = New System.Windows.Forms.TableLayoutPanel
        Me._testSystemGroupBox = New System.Windows.Forms.GroupBox
        Me._startHostButton = New System.Windows.Forms.Button
        Me._openStationButton = New System.Windows.Forms.Button
        Me._resetHardwareButton = New System.Windows.Forms.Button
        Me._controlModeGroupBox = New System.Windows.Forms.GroupBox
        Me._forceMultiCvu = New System.Windows.Forms.CheckBox
        Me._parallelCheckBox = New System.Windows.Forms.CheckBox
        Me._externalControlInterfaceRadioButton = New System.Windows.Forms.RadioButton
        Me._lowLevelControlRadioButton = New System.Windows.Forms.RadioButton
        Me._usingDevicesCheckBox = New System.Windows.Forms.CheckBox
        Me._closeStationButton = New System.Windows.Forms.Button
        Me._initializeButton = New System.Windows.Forms.Button
        Me._instrumentsTabPage = New System.Windows.Forms.TabPage
        Me._instrumentsLayout = New System.Windows.Forms.TableLayoutPanel
        Me._instrumentGroupBox = New System.Windows.Forms.GroupBox
        Me._selectInstrumentButton = New System.Windows.Forms.Button
        Me._availabelInstrumentsListBoxLabel = New System.Windows.Forms.Label
        Me._availabelInstrumentsListBox = New System.Windows.Forms.ListBox
        Me._addInstrumentButton = New System.Windows.Forms.Button
        Me._channelCountNumeric = New System.Windows.Forms.NumericUpDown
        Me._instrumentNumberNumeric = New System.Windows.Forms.NumericUpDown
        Me._identifierTextBox = New System.Windows.Forms.TextBox
        Me._channelCountNumericLabel = New System.Windows.Forms.Label
        Me._instrumentFamilyNameTextBox = New System.Windows.Forms.TextBox
        Me._instrumentNumberNumericLabel = New System.Windows.Forms.Label
        Me._instrumentFamilyNameTextBoxLabel = New System.Windows.Forms.Label
        Me._identifiedTextBoxLabel = New System.Windows.Forms.Label
        Me._impedanceGroupBox = New System.Windows.Forms.GroupBox
        Me._enabledCheckBox = New System.Windows.Forms.CheckBox
        Me._instrumentStatusLabel = New System.Windows.Forms.Label
        Me._compensationGroupBox = New System.Windows.Forms.GroupBox
        Me._loadCompensationCheckBox = New System.Windows.Forms.CheckBox
        Me._openCompensationCheckBox = New System.Windows.Forms.CheckBox
        Me._shortCompensationCheckBox = New System.Windows.Forms.CheckBox
        Me._cableComboBox = New System.Windows.Forms.ComboBox
        Me._cableComboBoxLabel = New System.Windows.Forms.Label
        Me._speedComboBox = New System.Windows.Forms.ComboBox
        Me._speedComboBoxLabel = New System.Windows.Forms.Label
        Me._modelComboBox = New System.Windows.Forms.ComboBox
        Me._modelComboBoxLabel = New System.Windows.Forms.Label
        Me._filterNumericLabel = New System.Windows.Forms.Label
        Me._delayNumericLabel = New System.Windows.Forms.Label
        Me._apertureNumericLabel = New System.Windows.Forms.Label
        Me._currentRangeNumericLabel = New System.Windows.Forms.Label
        Me._sourceLevelNumericLabel = New System.Windows.Forms.Label
        Me._frequencyNumericLabel = New System.Windows.Forms.Label
        Me._sourceBiasNumericLabel = New System.Windows.Forms.Label
        Me._filterNumeric = New System.Windows.Forms.NumericUpDown
        Me._delayNumeric = New System.Windows.Forms.NumericUpDown
        Me._apertureNumeric = New System.Windows.Forms.NumericUpDown
        Me._currentRangeNumeric = New System.Windows.Forms.NumericUpDown
        Me._sourceLevelNumeric = New System.Windows.Forms.NumericUpDown
        Me._frequencyNumeric = New System.Windows.Forms.NumericUpDown
        Me._sourceBiasNumeric = New System.Windows.Forms.NumericUpDown
        Me._applyButton = New System.Windows.Forms.Button
        Me._impedanceTabPage = New System.Windows.Forms.TabPage
        Me._impedanceLayout = New System.Windows.Forms.TableLayoutPanel
        Me._measurementsGroupBox = New System.Windows.Forms.GroupBox
        Me._measurementstListView = New BrightIdeasSoftware.FastObjectListView
        Me._instrumentColumn = New BrightIdeasSoftware.OLVColumn
        Me._measurementModelColumn = New BrightIdeasSoftware.OLVColumn
        Me._primaryMeasurementColumn = New BrightIdeasSoftware.OLVColumn
        Me._secondaryMeasurementColumn = New BrightIdeasSoftware.OLVColumn
        Me._measurementStatusColumn = New BrightIdeasSoftware.OLVColumn
        Me._measureGroupBox = New System.Windows.Forms.GroupBox
        Me._measureParallelButton = New System.Windows.Forms.Button
        Me._measureButton = New System.Windows.Forms.Button
        Me._primaryValueTestBoxLabel = New System.Windows.Forms.Label
        Me._primaryValueTestBox = New System.Windows.Forms.TextBox
        Me._measurementStatusTextBox = New System.Windows.Forms.TextBox
        Me._measurementStatusTextBoxLabel = New System.Windows.Forms.Label
        Me._secondaryValueTestBox = New System.Windows.Forms.TextBox
        Me._secondaryValueTestBoxLabel = New System.Windows.Forms.Label
        Me._diagnosticsTabPage = New System.Windows.Forms.TabPage
        Me._diagnosticsLayout = New System.Windows.Forms.TableLayoutPanel
        Me._interactiveGroupBox = New System.Windows.Forms.GroupBox
        Me._speedTextBox = New System.Windows.Forms.TextBox
        Me._speedTextBoxLabel = New System.Windows.Forms.Label
        Me._busyStatusLabel = New System.Windows.Forms.Label
        Me._errorAvailableLabel = New System.Windows.Forms.Label
        Me._messageAvailabelLabel = New System.Windows.Forms.Label
        Me._srqLabel = New System.Windows.Forms.Label
        Me._sendReceiveButton = New System.Windows.Forms.Button
        Me._replyTextBox = New System.Windows.Forms.TextBox
        Me._commandComboBox = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me._messagesTabPage = New System.Windows.Forms.TabPage
        Me._errorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._refreshTimer = New System.Windows.Forms.Timer(Me.components)
        Me._statusStrip.SuspendLayout()
        Me._tabControl.SuspendLayout()
        Me._stationTabPage.SuspendLayout()
        Me._stationLayout.SuspendLayout()
        Me._testSystemGroupBox.SuspendLayout()
        Me._controlModeGroupBox.SuspendLayout()
        Me._instrumentsTabPage.SuspendLayout()
        Me._instrumentsLayout.SuspendLayout()
        Me._instrumentGroupBox.SuspendLayout()
        CType(Me._channelCountNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._instrumentNumberNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._impedanceGroupBox.SuspendLayout()
        Me._compensationGroupBox.SuspendLayout()
        CType(Me._filterNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._delayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._apertureNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._currentRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._sourceLevelNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._frequencyNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._sourceBiasNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._impedanceTabPage.SuspendLayout()
        Me._impedanceLayout.SuspendLayout()
        Me._measurementsGroupBox.SuspendLayout()
        CType(Me._measurementstListView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._measureGroupBox.SuspendLayout()
        Me._diagnosticsTabPage.SuspendLayout()
        Me._diagnosticsLayout.SuspendLayout()
        Me._interactiveGroupBox.SuspendLayout()
        Me._messagesTabPage.SuspendLayout()
        CType(Me._errorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_statusStrip
        '
        Me._statusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._statusLabel, Me._statusByteLabel, Me._functionStatusLabel, Me._socketStatusLabel})
        Me._statusStrip.Location = New System.Drawing.Point(0, 517)
        Me._statusStrip.Name = "_statusStrip"
        Me._statusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me._statusStrip.Size = New System.Drawing.Size(716, 22)
        Me._statusStrip.TabIndex = 0
        Me._statusStrip.Text = "StatusStrip1"
        '
        '_statusLabel
        '
        Me._statusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                              System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                          System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._statusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me._statusLabel.Name = "_statusLabel"
        Me._statusLabel.Size = New System.Drawing.Size(630, 17)
        Me._statusLabel.Spring = True
        Me._statusLabel.Text = "<>"
        Me._statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._statusLabel.ToolTipText = "Status message"
        '
        '_statusByteLabel
        '
        Me._statusByteLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                                  System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                              System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._statusByteLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._statusByteLabel.Name = "_statusByteLabel"
        Me._statusByteLabel.Size = New System.Drawing.Size(35, 17)
        Me._statusByteLabel.Text = "&ox00"
        Me._statusByteLabel.ToolTipText = "Status Byte"
        '
        '_functionStatusLabel
        '
        Me._functionStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                                      System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                                  System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._functionStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._functionStatusLabel.Name = "_functionStatusLabel"
        Me._functionStatusLabel.Size = New System.Drawing.Size(17, 17)
        Me._functionStatusLabel.Text = "0"
        Me._functionStatusLabel.ToolTipText = "Last Function Call Status"
        '
        '_socketStatusLabel
        '
        Me._socketStatusLabel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                                    System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                                System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._socketStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._socketStatusLabel.Name = "_socketStatusLabel"
        Me._socketStatusLabel.Size = New System.Drawing.Size(17, 17)
        Me._socketStatusLabel.Text = "0"
        '
        '_messagesBox
        '
        Me._messagesBox.BackColor = System.Drawing.SystemColors.Info
        Me._messagesBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._messagesBox.Location = New System.Drawing.Point(3, 3)
        Me._messagesBox.Multiline = True
        Me._messagesBox.Name = "_messagesBox"
        Me._messagesBox.PresetCount = 50
        Me._messagesBox.ReadOnly = True
        Me._messagesBox.ResetCount = 100
        Me._messagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._messagesBox.Size = New System.Drawing.Size(702, 483)
        Me._messagesBox.TabIndex = 1
        '
        '_tabControl
        '
        Me._tabControl.Controls.Add(Me._stationTabPage)
        Me._tabControl.Controls.Add(Me._instrumentsTabPage)
        Me._tabControl.Controls.Add(Me._impedanceTabPage)
        Me._tabControl.Controls.Add(Me._diagnosticsTabPage)
        Me._tabControl.Controls.Add(Me._messagesTabPage)
        Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._tabControl.Location = New System.Drawing.Point(0, 0)
        Me._tabControl.Name = "_tabControl"
        Me._tabControl.SelectedIndex = 0
        Me._tabControl.Size = New System.Drawing.Size(716, 517)
        Me._tabControl.TabIndex = 2
        '
        '_stationTabPage
        '
        Me._stationTabPage.Controls.Add(Me._stationLayout)
        Me._stationTabPage.Location = New System.Drawing.Point(4, 24)
        Me._stationTabPage.Name = "_stationTabPage"
        Me._stationTabPage.Size = New System.Drawing.Size(708, 489)
        Me._stationTabPage.TabIndex = 2
        Me._stationTabPage.Text = "STATION"
        Me._stationTabPage.UseVisualStyleBackColor = True
        '
        '_stationLayout
        '
        Me._stationLayout.ColumnCount = 3
        Me._stationLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._stationLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._stationLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._stationLayout.Controls.Add(Me._testSystemGroupBox, 1, 1)
        Me._stationLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._stationLayout.Location = New System.Drawing.Point(0, 0)
        Me._stationLayout.Name = "_stationLayout"
        Me._stationLayout.RowCount = 3
        Me._stationLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._stationLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._stationLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._stationLayout.Size = New System.Drawing.Size(708, 489)
        Me._stationLayout.TabIndex = 5
        '
        '_testSystemGroupBox
        '
        Me._testSystemGroupBox.Controls.Add(Me._startHostButton)
        Me._testSystemGroupBox.Controls.Add(Me._openStationButton)
        Me._testSystemGroupBox.Controls.Add(Me._resetHardwareButton)
        Me._testSystemGroupBox.Controls.Add(Me._controlModeGroupBox)
        Me._testSystemGroupBox.Controls.Add(Me._closeStationButton)
        Me._testSystemGroupBox.Controls.Add(Me._initializeButton)
        Me._testSystemGroupBox.Location = New System.Drawing.Point(208, 66)
        Me._testSystemGroupBox.Name = "_testSystemGroupBox"
        Me._testSystemGroupBox.Size = New System.Drawing.Size(291, 357)
        Me._testSystemGroupBox.TabIndex = 0
        Me._testSystemGroupBox.TabStop = False
        Me._testSystemGroupBox.Text = "TEST STATION"
        '
        '_startHostButton
        '
        Me._startHostButton.Location = New System.Drawing.Point(17, 302)
        Me._startHostButton.Name = "_startHostButton"
        Me._startHostButton.Size = New System.Drawing.Size(254, 40)
        Me._startHostButton.TabIndex = 7
        Me._startHostButton.Text = "START KXCI"
        Me._toolTip.SetToolTip(Me._startHostButton, "Starts the Keithley External Command Interface")
        Me._startHostButton.UseVisualStyleBackColor = True
        '
        '_openStationButton
        '
        Me._openStationButton.Location = New System.Drawing.Point(17, 142)
        Me._openStationButton.Name = "_openStationButton"
        Me._openStationButton.Size = New System.Drawing.Size(120, 42)
        Me._openStationButton.TabIndex = 6
        Me._openStationButton.Text = "&OPEN"
        Me._openStationButton.UseVisualStyleBackColor = True
        '
        '_resetHardwareButton
        '
        Me._resetHardwareButton.Location = New System.Drawing.Point(17, 247)
        Me._resetHardwareButton.Name = "_resetHardwareButton"
        Me._resetHardwareButton.Size = New System.Drawing.Size(254, 40)
        Me._resetHardwareButton.TabIndex = 5
        Me._resetHardwareButton.Text = "RESET STATION"
        Me._toolTip.SetToolTip(Me._resetHardwareButton, "Calls the reset h/w executable to reset the test station")
        Me._resetHardwareButton.UseVisualStyleBackColor = True
        '   
        '_controlModeGroupBox
        '
        Me._controlModeGroupBox.Controls.Add(Me._forceMultiCvu)
        Me._controlModeGroupBox.Controls.Add(Me._parallelCheckBox)
        Me._controlModeGroupBox.Controls.Add(Me._externalControlInterfaceRadioButton)
        Me._controlModeGroupBox.Controls.Add(Me._lowLevelControlRadioButton)
        Me._controlModeGroupBox.Controls.Add(Me._usingDevicesCheckBox)
        Me._controlModeGroupBox.Location = New System.Drawing.Point(6, 18)
        Me._controlModeGroupBox.Name = "_controlModeGroupBox"
        Me._controlModeGroupBox.Size = New System.Drawing.Size(272, 118)
        Me._controlModeGroupBox.TabIndex = 4
        Me._controlModeGroupBox.TabStop = False
        Me._controlModeGroupBox.Text = "CONTROL MODE"
        '
        '_forceMultiCvu
        '
        Me._forceMultiCvu.AutoSize = True
        Me._forceMultiCvu.Location = New System.Drawing.Point(14, 83)
        Me._forceMultiCvu.Name = "_forceMultiCvu"
        Me._forceMultiCvu.Size = New System.Drawing.Size(248, 19)
        Me._forceMultiCvu.TabIndex = 5
        Me._forceMultiCvu.Text = "EMULATE MULTI-CVU USING ONE CVU"
        Me._forceMultiCvu.UseVisualStyleBackColor = True
        '
        '_parallelCheckBox
        '
        Me._parallelCheckBox.AutoSize = True
        Me._parallelCheckBox.Location = New System.Drawing.Point(149, 54)
        Me._parallelCheckBox.Name = "_parallelCheckBox"
        Me._parallelCheckBox.Size = New System.Drawing.Size(113, 19)
        Me._parallelCheckBox.TabIndex = 4
        Me._parallelCheckBox.Text = "PARALLEL CVU"
        Me._toolTip.SetToolTip(Me._parallelCheckBox, "Turns on Parallel CVU")
        Me._parallelCheckBox.UseVisualStyleBackColor = True
        '
        '_externalControlInterfaceRadioButton
        '
        Me._externalControlInterfaceRadioButton.AutoSize = True
        Me._externalControlInterfaceRadioButton.Location = New System.Drawing.Point(149, 24)
        Me._externalControlInterfaceRadioButton.Name = "_externalControlInterfaceRadioButton"
        Me._externalControlInterfaceRadioButton.Size = New System.Drawing.Size(52, 19)
        Me._externalControlInterfaceRadioButton.TabIndex = 3
        Me._externalControlInterfaceRadioButton.Text = "KXCI"
        Me._toolTip.SetToolTip(Me._externalControlInterfaceRadioButton, "Selects External Control Interface (KXCI) instrument control.")
        Me._externalControlInterfaceRadioButton.UseVisualStyleBackColor = True
        '
        '_lowLevelControlRadioButton
        '
        Me._lowLevelControlRadioButton.AutoSize = True
        Me._lowLevelControlRadioButton.Checked = True
        Me._lowLevelControlRadioButton.Location = New System.Drawing.Point(14, 24)
        Me._lowLevelControlRadioButton.Name = "_lowLevelControlRadioButton"
        Me._lowLevelControlRadioButton.Size = New System.Drawing.Size(92, 19)
        Me._lowLevelControlRadioButton.TabIndex = 3
        Me._lowLevelControlRadioButton.TabStop = True
        Me._lowLevelControlRadioButton.Text = "LOW LEVEL"
        Me._toolTip.SetToolTip(Me._lowLevelControlRadioButton, "Selects the low level (LPT) library for instrument control.")
        Me._lowLevelControlRadioButton.UseVisualStyleBackColor = True
        '
        '_usingDevicesCheckBox
        '
        Me._usingDevicesCheckBox.AutoSize = True
        Me._usingDevicesCheckBox.Checked = Global.isr.Kte.Testers.My.MySettings.Default.UsingDevices
        Me._usingDevicesCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Global.isr.Kte.Testers.My.MySettings.Default, "UsingDevices", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._usingDevicesCheckBox.Location = New System.Drawing.Point(14, 54)
        Me._usingDevicesCheckBox.Name = "_usingDevicesCheckBox"
        Me._usingDevicesCheckBox.Size = New System.Drawing.Size(123, 19)
        Me._usingDevicesCheckBox.TabIndex = 0
        Me._usingDevicesCheckBox.Text = "USE HARDWARE"
        Me._toolTip.SetToolTip(Me._usingDevicesCheckBox, "Check to use hardware or uncheck to emulate.")
        Me._usingDevicesCheckBox.UseVisualStyleBackColor = True
        '
        '_closeStationButton
        '
        Me._closeStationButton.Enabled = False
        Me._closeStationButton.Location = New System.Drawing.Point(151, 142)
        Me._closeStationButton.Name = "_closeStationButton"
        Me._closeStationButton.Size = New System.Drawing.Size(120, 42)
        Me._closeStationButton.TabIndex = 2
        Me._closeStationButton.Text = "&CLOSE"
        Me._closeStationButton.UseVisualStyleBackColor = True
        '
        '_initializeButton
        '
        Me._initializeButton.Location = New System.Drawing.Point(17, 197)
        Me._initializeButton.Name = "_initializeButton"
        Me._initializeButton.Size = New System.Drawing.Size(254, 42)
        Me._initializeButton.TabIndex = 1
        Me._initializeButton.Text = "&INITIALIZE"
        Me._initializeButton.UseVisualStyleBackColor = True
        '
        '_instrumentsTabPage
        '
        Me._instrumentsTabPage.Controls.Add(Me._instrumentsLayout)
        Me._instrumentsTabPage.Location = New System.Drawing.Point(4, 24)
        Me._instrumentsTabPage.Name = "_instrumentsTabPage"
        Me._instrumentsTabPage.Size = New System.Drawing.Size(708, 489)
        Me._instrumentsTabPage.TabIndex = 3
        Me._instrumentsTabPage.Text = "INSTRUMENTS"
        Me._instrumentsTabPage.UseVisualStyleBackColor = True
        '
        '_instrumentsLayout
        '
        Me._instrumentsLayout.ColumnCount = 4
        Me._instrumentsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._instrumentsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._instrumentsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._instrumentsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._instrumentsLayout.Controls.Add(Me._instrumentGroupBox, 1, 1)
        Me._instrumentsLayout.Controls.Add(Me._impedanceGroupBox, 2, 1)
        Me._instrumentsLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._instrumentsLayout.Location = New System.Drawing.Point(0, 0)
        Me._instrumentsLayout.Name = "_instrumentsLayout"
        Me._instrumentsLayout.RowCount = 3
        Me._instrumentsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._instrumentsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._instrumentsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._instrumentsLayout.Size = New System.Drawing.Size(708, 489)
        Me._instrumentsLayout.TabIndex = 0
        '
        '_instrumentGroupBox
        '
        Me._instrumentGroupBox.Controls.Add(Me._selectInstrumentButton)
        Me._instrumentGroupBox.Controls.Add(Me._availabelInstrumentsListBoxLabel)
        Me._instrumentGroupBox.Controls.Add(Me._availabelInstrumentsListBox)
        Me._instrumentGroupBox.Controls.Add(Me._addInstrumentButton)
        Me._instrumentGroupBox.Controls.Add(Me._channelCountNumeric)
        Me._instrumentGroupBox.Controls.Add(Me._instrumentNumberNumeric)
        Me._instrumentGroupBox.Controls.Add(Me._identifierTextBox)
        Me._instrumentGroupBox.Controls.Add(Me._channelCountNumericLabel)
        Me._instrumentGroupBox.Controls.Add(Me._instrumentFamilyNameTextBox)
        Me._instrumentGroupBox.Controls.Add(Me._instrumentNumberNumericLabel)
        Me._instrumentGroupBox.Controls.Add(Me._instrumentFamilyNameTextBoxLabel)
        Me._instrumentGroupBox.Controls.Add(Me._identifiedTextBoxLabel)
        Me._instrumentGroupBox.Enabled = False
        Me._instrumentGroupBox.Location = New System.Drawing.Point(64, 55)
        Me._instrumentGroupBox.Name = "_instrumentGroupBox"
        Me._instrumentGroupBox.Size = New System.Drawing.Size(218, 251)
        Me._instrumentGroupBox.TabIndex = 1
        Me._instrumentGroupBox.TabStop = False
        Me._instrumentGroupBox.Text = "INSTRUMENT"
        '
        '_selectInstrumentButton
        '
        Me._selectInstrumentButton.Location = New System.Drawing.Point(135, 146)
        Me._selectInstrumentButton.Name = "_selectInstrumentButton"
        Me._selectInstrumentButton.Size = New System.Drawing.Size(75, 28)
        Me._selectInstrumentButton.TabIndex = 9
        Me._selectInstrumentButton.Text = "SELECT"
        Me._toolTip.SetToolTip(Me._selectInstrumentButton, "Selects an instrument to configure")
        Me._selectInstrumentButton.UseVisualStyleBackColor = True
        '
        '_availabelInstrumentsListBoxLabel
        '
        Me._availabelInstrumentsListBoxLabel.AutoSize = True
        Me._availabelInstrumentsListBoxLabel.Location = New System.Drawing.Point(10, 128)
        Me._availabelInstrumentsListBoxLabel.Name = "_availabelInstrumentsListBoxLabel"
        Me._availabelInstrumentsListBoxLabel.Size = New System.Drawing.Size(95, 15)
        Me._availabelInstrumentsListBoxLabel.TabIndex = 8
        Me._availabelInstrumentsListBoxLabel.Text = "INSTRUMENTS"
        '
        '_availabelInstrumentsListBox
        '
        Me._availabelInstrumentsListBox.FormattingEnabled = True
        Me._availabelInstrumentsListBox.ItemHeight = 15
        Me._availabelInstrumentsListBox.Location = New System.Drawing.Point(9, 146)
        Me._availabelInstrumentsListBox.Name = "_availabelInstrumentsListBox"
        Me._availabelInstrumentsListBox.Size = New System.Drawing.Size(120, 94)
        Me._availabelInstrumentsListBox.TabIndex = 7
        '
        '_addInstrumentButton
        '
        Me._addInstrumentButton.Location = New System.Drawing.Point(125, 55)
        Me._addInstrumentButton.Name = "_addInstrumentButton"
        Me._addInstrumentButton.Size = New System.Drawing.Size(84, 29)
        Me._addInstrumentButton.TabIndex = 6
        Me._addInstrumentButton.Text = "ADD"
        Me._toolTip.SetToolTip(Me._addInstrumentButton, "Adds and selects an instrument")
        Me._addInstrumentButton.UseVisualStyleBackColor = True
        '
        '_channelCountNumeric
        '
        Me._channelCountNumeric.Location = New System.Drawing.Point(166, 24)
        Me._channelCountNumeric.Maximum = New Decimal(New Integer() {8, 0, 0, 0})
        Me._channelCountNumeric.Name = "_channelCountNumeric"
        Me._channelCountNumeric.Size = New System.Drawing.Size(41, 21)
        Me._channelCountNumeric.TabIndex = 3
        Me._channelCountNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._channelCountNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_instrumentNumberNumeric
        '
        Me._instrumentNumberNumeric.Location = New System.Drawing.Point(78, 59)
        Me._instrumentNumberNumeric.Maximum = New Decimal(New Integer() {4, 0, 0, 0})
        Me._instrumentNumberNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._instrumentNumberNumeric.Name = "_instrumentNumberNumeric"
        Me._instrumentNumberNumeric.Size = New System.Drawing.Size(41, 21)
        Me._instrumentNumberNumeric.TabIndex = 3
        Me._instrumentNumberNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._instrumentNumberNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_identifierTextBox
        '
        Me._identifierTextBox.Location = New System.Drawing.Point(87, 95)
        Me._identifierTextBox.Name = "_identifierTextBox"
        Me._identifierTextBox.ReadOnly = True
        Me._identifierTextBox.Size = New System.Drawing.Size(69, 21)
        Me._identifierTextBox.TabIndex = 5
        Me._toolTip.SetToolTip(Me._identifierTextBox, "Instrument Identifier")
        '
        '_channelCountNumericLabel
        '
        Me._channelCountNumericLabel.AutoSize = True
        Me._channelCountNumericLabel.Location = New System.Drawing.Point(111, 27)
        Me._channelCountNumericLabel.Name = "_channelCountNumericLabel"
        Me._channelCountNumericLabel.Size = New System.Drawing.Size(52, 15)
        Me._channelCountNumericLabel.TabIndex = 2
        Me._channelCountNumericLabel.Text = "COUNT:"
        Me._channelCountNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_instrumentFamilyNameTextBox
        '
        Me._instrumentFamilyNameTextBox.Location = New System.Drawing.Point(61, 24)
        Me._instrumentFamilyNameTextBox.Name = "_instrumentFamilyNameTextBox"
        Me._instrumentFamilyNameTextBox.ReadOnly = True
        Me._instrumentFamilyNameTextBox.Size = New System.Drawing.Size(42, 21)
        Me._instrumentFamilyNameTextBox.TabIndex = 1
        Me._instrumentFamilyNameTextBox.Text = "CVU"
        Me._instrumentFamilyNameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_instrumentNumberNumericLabel
        '
        Me._instrumentNumberNumericLabel.AutoSize = True
        Me._instrumentNumberNumericLabel.Location = New System.Drawing.Point(10, 62)
        Me._instrumentNumberNumericLabel.Name = "_instrumentNumberNumericLabel"
        Me._instrumentNumberNumericLabel.Size = New System.Drawing.Size(67, 15)
        Me._instrumentNumberNumericLabel.TabIndex = 2
        Me._instrumentNumberNumericLabel.Text = "CHANNEL:"
        Me._instrumentNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_instrumentFamilyNameTextBoxLabel
        '
        Me._instrumentFamilyNameTextBoxLabel.AutoSize = True
        Me._instrumentFamilyNameTextBoxLabel.Location = New System.Drawing.Point(6, 27)
        Me._instrumentFamilyNameTextBoxLabel.Name = "_instrumentFamilyNameTextBoxLabel"
        Me._instrumentFamilyNameTextBoxLabel.Size = New System.Drawing.Size(52, 15)
        Me._instrumentFamilyNameTextBoxLabel.TabIndex = 0
        Me._instrumentFamilyNameTextBoxLabel.Text = "FAMILY:"
        Me._instrumentFamilyNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_identifiedTextBoxLabel
        '
        Me._identifiedTextBoxLabel.AutoSize = True
        Me._identifiedTextBoxLabel.Location = New System.Drawing.Point(10, 98)
        Me._identifiedTextBoxLabel.Name = "_identifiedTextBoxLabel"
        Me._identifiedTextBoxLabel.Size = New System.Drawing.Size(76, 15)
        Me._identifiedTextBoxLabel.TabIndex = 4
        Me._identifiedTextBoxLabel.Text = "IDENTIFIER:"
        Me._identifiedTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_impedanceGroupBox
        '
        Me._impedanceGroupBox.Controls.Add(Me._enabledCheckBox)
        Me._impedanceGroupBox.Controls.Add(Me._instrumentStatusLabel)
        Me._impedanceGroupBox.Controls.Add(Me._compensationGroupBox)
        Me._impedanceGroupBox.Controls.Add(Me._cableComboBox)
        Me._impedanceGroupBox.Controls.Add(Me._cableComboBoxLabel)
        Me._impedanceGroupBox.Controls.Add(Me._speedComboBox)
        Me._impedanceGroupBox.Controls.Add(Me._speedComboBoxLabel)
        Me._impedanceGroupBox.Controls.Add(Me._modelComboBox)
        Me._impedanceGroupBox.Controls.Add(Me._modelComboBoxLabel)
        Me._impedanceGroupBox.Controls.Add(Me._filterNumericLabel)
        Me._impedanceGroupBox.Controls.Add(Me._delayNumericLabel)
        Me._impedanceGroupBox.Controls.Add(Me._apertureNumericLabel)
        Me._impedanceGroupBox.Controls.Add(Me._currentRangeNumericLabel)
        Me._impedanceGroupBox.Controls.Add(Me._sourceLevelNumericLabel)
        Me._impedanceGroupBox.Controls.Add(Me._frequencyNumericLabel)
        Me._impedanceGroupBox.Controls.Add(Me._sourceBiasNumericLabel)
        Me._impedanceGroupBox.Controls.Add(Me._filterNumeric)
        Me._impedanceGroupBox.Controls.Add(Me._delayNumeric)
        Me._impedanceGroupBox.Controls.Add(Me._apertureNumeric)
        Me._impedanceGroupBox.Controls.Add(Me._currentRangeNumeric)
        Me._impedanceGroupBox.Controls.Add(Me._sourceLevelNumeric)
        Me._impedanceGroupBox.Controls.Add(Me._frequencyNumeric)
        Me._impedanceGroupBox.Controls.Add(Me._sourceBiasNumeric)
        Me._impedanceGroupBox.Controls.Add(Me._applyButton)
        Me._impedanceGroupBox.Enabled = False
        Me._impedanceGroupBox.Location = New System.Drawing.Point(288, 55)
        Me._impedanceGroupBox.Name = "_impedanceGroupBox"
        Me._impedanceGroupBox.Size = New System.Drawing.Size(356, 379)
        Me._impedanceGroupBox.TabIndex = 4
        Me._impedanceGroupBox.TabStop = False
        Me._impedanceGroupBox.Text = "IMPEDANCE INSTUMENT CONFIGURATION"
        '
        '_enabledCheckBox
        '
        Me._enabledCheckBox.AutoSize = True
        Me._enabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._enabledCheckBox.Location = New System.Drawing.Point(93, 31)
        Me._enabledCheckBox.Name = "_enabledCheckBox"
        Me._enabledCheckBox.Size = New System.Drawing.Size(85, 19)
        Me._enabledCheckBox.TabIndex = 0
        Me._enabledCheckBox.Text = "ENABLED:"
        Me._enabledCheckBox.UseVisualStyleBackColor = True
        '
        '_instrumentStatusLabel
        '
        Me._instrumentStatusLabel.BackColor = System.Drawing.SystemColors.ControlLight
        Me._instrumentStatusLabel.Location = New System.Drawing.Point(259, 63)
        Me._instrumentStatusLabel.Name = "_instrumentStatusLabel"
        Me._instrumentStatusLabel.Size = New System.Drawing.Size(81, 58)
        Me._instrumentStatusLabel.TabIndex = 23
        Me._instrumentStatusLabel.Text = "Instrument Status"
        Me._instrumentStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_compensationGroupBox
        '
        Me._compensationGroupBox.Controls.Add(Me._loadCompensationCheckBox)
        Me._compensationGroupBox.Controls.Add(Me._openCompensationCheckBox)
        Me._compensationGroupBox.Controls.Add(Me._shortCompensationCheckBox)
        Me._compensationGroupBox.Location = New System.Drawing.Point(7, 328)
        Me._compensationGroupBox.Name = "_compensationGroupBox"
        Me._compensationGroupBox.Size = New System.Drawing.Size(339, 47)
        Me._compensationGroupBox.TabIndex = 21
        Me._compensationGroupBox.TabStop = False
        Me._compensationGroupBox.Text = "COMPENSATION"
        '
        '_loadCompensationCheckBox
        '
        Me._loadCompensationCheckBox.AutoSize = True
        Me._loadCompensationCheckBox.Location = New System.Drawing.Point(254, 20)
        Me._loadCompensationCheckBox.Name = "_loadCompensationCheckBox"
        Me._loadCompensationCheckBox.Size = New System.Drawing.Size(58, 19)
        Me._loadCompensationCheckBox.TabIndex = 2
        Me._loadCompensationCheckBox.Text = "LOAD"
        Me._toolTip.SetToolTip(Me._loadCompensationCheckBox, "Enables load compensation")
        Me._loadCompensationCheckBox.UseVisualStyleBackColor = True
        '
        '_openCompensationCheckBox
        '
        Me._openCompensationCheckBox.AutoSize = True
        Me._openCompensationCheckBox.Location = New System.Drawing.Point(135, 20)
        Me._openCompensationCheckBox.Name = "_openCompensationCheckBox"
        Me._openCompensationCheckBox.Size = New System.Drawing.Size(60, 19)
        Me._openCompensationCheckBox.TabIndex = 1
        Me._openCompensationCheckBox.Text = "OPEN"
        Me._toolTip.SetToolTip(Me._openCompensationCheckBox, "Enables open compensation.")
        Me._openCompensationCheckBox.UseVisualStyleBackColor = True
        '
        '_shortCompensationCheckBox
        '
        Me._shortCompensationCheckBox.AutoSize = True
        Me._shortCompensationCheckBox.Location = New System.Drawing.Point(16, 20)
        Me._shortCompensationCheckBox.Name = "_shortCompensationCheckBox"
        Me._shortCompensationCheckBox.Size = New System.Drawing.Size(68, 19)
        Me._shortCompensationCheckBox.TabIndex = 0
        Me._shortCompensationCheckBox.Text = "SHORT"
        Me._toolTip.SetToolTip(Me._shortCompensationCheckBox, "Check to enable short compensation")
        Me._shortCompensationCheckBox.UseVisualStyleBackColor = True
        '
        '_cableComboBox
        '
        Me._cableComboBox.FormattingEnabled = True
        Me._cableComboBox.Location = New System.Drawing.Point(164, 302)
        Me._cableComboBox.Name = "_cableComboBox"
        Me._cableComboBox.Size = New System.Drawing.Size(65, 23)
        Me._cableComboBox.TabIndex = 20
        Me._toolTip.SetToolTip(Me._cableComboBox, "Cable length")
        '
        '_cableComboBoxLabel
        '
        Me._cableComboBoxLabel.Location = New System.Drawing.Point(8, 306)
        Me._cableComboBoxLabel.Name = "_cableComboBoxLabel"
        Me._cableComboBoxLabel.Size = New System.Drawing.Size(153, 15)
        Me._cableComboBoxLabel.TabIndex = 19
        Me._cableComboBoxLabel.Text = "CABLE LENGTH:"
        Me._cableComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_speedComboBox
        '
        Me._speedComboBox.FormattingEnabled = True
        Me._speedComboBox.Location = New System.Drawing.Point(164, 192)
        Me._speedComboBox.Name = "_speedComboBox"
        Me._speedComboBox.Size = New System.Drawing.Size(131, 23)
        Me._speedComboBox.TabIndex = 12
        Me._toolTip.SetToolTip(Me._speedComboBox, "Impedance Measurement speed")
        '
        '_speedComboBoxLabel
        '
        Me._speedComboBoxLabel.Location = New System.Drawing.Point(8, 196)
        Me._speedComboBoxLabel.Name = "_speedComboBoxLabel"
        Me._speedComboBoxLabel.Size = New System.Drawing.Size(153, 15)
        Me._speedComboBoxLabel.TabIndex = 11
        Me._speedComboBoxLabel.Text = "MEASUREMENT SPEED:"
        Me._speedComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_modelComboBox
        '
        Me._modelComboBox.FormattingEnabled = True
        Me._modelComboBox.Location = New System.Drawing.Point(164, 163)
        Me._modelComboBox.Name = "_modelComboBox"
        Me._modelComboBox.Size = New System.Drawing.Size(131, 23)
        Me._modelComboBox.TabIndex = 10
        Me._toolTip.SetToolTip(Me._modelComboBox, "Impedance measurement model")
        '
        '_modelComboBoxLabel
        '
        Me._modelComboBoxLabel.Location = New System.Drawing.Point(8, 167)
        Me._modelComboBoxLabel.Name = "_modelComboBoxLabel"
        Me._modelComboBoxLabel.Size = New System.Drawing.Size(153, 15)
        Me._modelComboBoxLabel.TabIndex = 9
        Me._modelComboBoxLabel.Text = "IMPEDANCE MODEL:"
        Me._modelComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_filterNumericLabel
        '
        Me._filterNumericLabel.Location = New System.Drawing.Point(8, 278)
        Me._filterNumericLabel.Name = "_filterNumericLabel"
        Me._filterNumericLabel.Size = New System.Drawing.Size(153, 15)
        Me._filterNumericLabel.TabIndex = 17
        Me._filterNumericLabel.Text = "FILTER FACTOR:"
        Me._filterNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_delayNumericLabel
        '
        Me._delayNumericLabel.Location = New System.Drawing.Point(8, 251)
        Me._delayNumericLabel.Name = "_delayNumericLabel"
        Me._delayNumericLabel.Size = New System.Drawing.Size(153, 15)
        Me._delayNumericLabel.TabIndex = 15
        Me._delayNumericLabel.Text = "DELAY FACTOR:"
        Me._delayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._toolTip.SetToolTip(Me._delayNumericLabel, "Delay factor between 0 and 100.  Delay equals default delay time times the delay f" & 
                "actor.")
        '
        '_apertureNumericLabel
        '
        Me._apertureNumericLabel.Location = New System.Drawing.Point(8, 224)
        Me._apertureNumericLabel.Name = "_apertureNumericLabel"
        Me._apertureNumericLabel.Size = New System.Drawing.Size(153, 15)
        Me._apertureNumericLabel.TabIndex = 13
        Me._apertureNumericLabel.Text = "APERTURE [NPLC]:"
        Me._apertureNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_currentRangeNumericLabel
        '
        Me._currentRangeNumericLabel.Location = New System.Drawing.Point(8, 139)
        Me._currentRangeNumericLabel.Name = "_currentRangeNumericLabel"
        Me._currentRangeNumericLabel.Size = New System.Drawing.Size(153, 15)
        Me._currentRangeNumericLabel.TabIndex = 7
        Me._currentRangeNumericLabel.Text = "SENSE RANGE [uA]:"
        Me._currentRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_sourceLevelNumericLabel
        '
        Me._sourceLevelNumericLabel.Location = New System.Drawing.Point(8, 112)
        Me._sourceLevelNumericLabel.Name = "_sourceLevelNumericLabel"
        Me._sourceLevelNumericLabel.Size = New System.Drawing.Size(153, 15)
        Me._sourceLevelNumericLabel.TabIndex = 5
        Me._sourceLevelNumericLabel.Text = "AC VOLTAGE [mV]:"
        Me._sourceLevelNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_frequencyNumericLabel
        '
        Me._frequencyNumericLabel.Location = New System.Drawing.Point(8, 85)
        Me._frequencyNumericLabel.Name = "_frequencyNumericLabel"
        Me._frequencyNumericLabel.Size = New System.Drawing.Size(153, 15)
        Me._frequencyNumericLabel.TabIndex = 3
        Me._frequencyNumericLabel.Text = "FREQUENCY [Hz]:"
        Me._frequencyNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_sourceBiasNumericLabel
        '
        Me._sourceBiasNumericLabel.Location = New System.Drawing.Point(8, 58)
        Me._sourceBiasNumericLabel.Name = "_sourceBiasNumericLabel"
        Me._sourceBiasNumericLabel.Size = New System.Drawing.Size(153, 15)
        Me._sourceBiasNumericLabel.TabIndex = 1
        Me._sourceBiasNumericLabel.Text = "DC BIAS [v]:"
        Me._sourceBiasNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_filterNumeric
        '
        Me._filterNumeric.DecimalPlaces = 1
        Me._filterNumeric.Location = New System.Drawing.Point(164, 275)
        Me._filterNumeric.Name = "_filterNumeric"
        Me._filterNumeric.Size = New System.Drawing.Size(65, 21)
        Me._filterNumeric.TabIndex = 18
        Me._toolTip.SetToolTip(Me._filterNumeric, "Filter factor between 0 and 100.")
        Me._filterNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_delayNumeric
        '
        Me._delayNumeric.DecimalPlaces = 1
        Me._delayNumeric.Location = New System.Drawing.Point(164, 248)
        Me._delayNumeric.Name = "_delayNumeric"
        Me._delayNumeric.Size = New System.Drawing.Size(65, 21)
        Me._delayNumeric.TabIndex = 16
        Me._delayNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_apertureNumeric
        '
        Me._apertureNumeric.DecimalPlaces = 3
        Me._apertureNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._apertureNumeric.Location = New System.Drawing.Point(164, 221)
        Me._apertureNumeric.Maximum = New Decimal(New Integer() {10002, 0, 0, 196608})
        Me._apertureNumeric.Minimum = New Decimal(New Integer() {6, 0, 0, 196608})
        Me._apertureNumeric.Name = "_apertureNumeric"
        Me._apertureNumeric.Size = New System.Drawing.Size(65, 21)
        Me._apertureNumeric.TabIndex = 14
        Me._toolTip.SetToolTip(Me._apertureNumeric, "Aperture in power line cycles between 0.006 and 10.002")
        Me._apertureNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_currentRangeNumeric
        '
        Me._currentRangeNumeric.Increment = New Decimal(New Integer() {100, 0, 0, 0})
        Me._currentRangeNumeric.Location = New System.Drawing.Point(164, 136)
        Me._currentRangeNumeric.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._currentRangeNumeric.Name = "_currentRangeNumeric"
        Me._currentRangeNumeric.Size = New System.Drawing.Size(76, 21)
        Me._currentRangeNumeric.TabIndex = 8
        Me._toolTip.SetToolTip(Me._currentRangeNumeric, "Current sense range in micro amperes. A zero value sets to auto range.")
        Me._currentRangeNumeric.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        '_sourceLevelNumeric
        '
        Me._sourceLevelNumeric.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me._sourceLevelNumeric.Location = New System.Drawing.Point(164, 109)
        Me._sourceLevelNumeric.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._sourceLevelNumeric.Name = "_sourceLevelNumeric"
        Me._sourceLevelNumeric.Size = New System.Drawing.Size(76, 21)
        Me._sourceLevelNumeric.TabIndex = 6
        Me._toolTip.SetToolTip(Me._sourceLevelNumeric, "AC drive voltage between 10 and 100 mV RMS")
        Me._sourceLevelNumeric.Value = New Decimal(New Integer() {50, 0, 0, 0})
        '
        '_frequencyNumeric
        '
        Me._frequencyNumeric.Increment = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._frequencyNumeric.Location = New System.Drawing.Point(164, 82)
        Me._frequencyNumeric.Maximum = New Decimal(New Integer() {10000000, 0, 0, 0})
        Me._frequencyNumeric.Minimum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._frequencyNumeric.Name = "_frequencyNumeric"
        Me._frequencyNumeric.Size = New System.Drawing.Size(76, 21)
        Me._frequencyNumeric.TabIndex = 4
        Me._toolTip.SetToolTip(Me._frequencyNumeric, "Frequency between 10K and 10M Hz")
        Me._frequencyNumeric.Value = New Decimal(New Integer() {10000, 0, 0, 0})
        '
        '_sourceBiasNumeric
        '
        Me._sourceBiasNumeric.DecimalPlaces = 1
        Me._sourceBiasNumeric.Location = New System.Drawing.Point(164, 55)
        Me._sourceBiasNumeric.Maximum = New Decimal(New Integer() {30, 0, 0, 0})
        Me._sourceBiasNumeric.Minimum = New Decimal(New Integer() {30, 0, 0, -2147483648})
        Me._sourceBiasNumeric.Name = "_sourceBiasNumeric"
        Me._sourceBiasNumeric.Size = New System.Drawing.Size(76, 21)
        Me._sourceBiasNumeric.TabIndex = 2
        Me._toolTip.SetToolTip(Me._sourceBiasNumeric, "Source DC bias between -30 and +30 volts")
        Me._sourceBiasNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_applyButton
        '
        Me._applyButton.Location = New System.Drawing.Point(259, 20)
        Me._applyButton.Name = "_applyButton"
        Me._applyButton.Size = New System.Drawing.Size(85, 36)
        Me._applyButton.TabIndex = 22
        Me._applyButton.Text = "&APPLY"
        Me._applyButton.UseVisualStyleBackColor = True
        '
        '_impedanceTabPage
        '
        Me._impedanceTabPage.Controls.Add(Me._impedanceLayout)
        Me._impedanceTabPage.Location = New System.Drawing.Point(4, 24)
        Me._impedanceTabPage.Name = "_impedanceTabPage"
        Me._impedanceTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._impedanceTabPage.Size = New System.Drawing.Size(708, 489)
        Me._impedanceTabPage.TabIndex = 0
        Me._impedanceTabPage.Text = "IMPEDANCE"
        Me._impedanceTabPage.UseVisualStyleBackColor = True
        '
        '_impedanceLayout
        '
        Me._impedanceLayout.ColumnCount = 3
        Me._impedanceLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._impedanceLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._impedanceLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._impedanceLayout.Controls.Add(Me._measurementsGroupBox, 1, 2)
        Me._impedanceLayout.Controls.Add(Me._measureGroupBox, 1, 1)
        Me._impedanceLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._impedanceLayout.Location = New System.Drawing.Point(3, 3)
        Me._impedanceLayout.Name = "_impedanceLayout"
        Me._impedanceLayout.RowCount = 4
        Me._impedanceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.99999!))
        Me._impedanceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._impedanceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._impedanceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.00001!))
        Me._impedanceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._impedanceLayout.Size = New System.Drawing.Size(702, 483)
        Me._impedanceLayout.TabIndex = 6
        '
        '_measurementsGroupBox
        '
        Me._measurementsGroupBox.Controls.Add(Me._measurementstListView)
        Me._measurementsGroupBox.Location = New System.Drawing.Point(125, 210)
        Me._measurementsGroupBox.Name = "_measurementsGroupBox"
        Me._measurementsGroupBox.Size = New System.Drawing.Size(451, 190)
        Me._measurementsGroupBox.TabIndex = 6
        Me._measurementsGroupBox.TabStop = False
        Me._measurementsGroupBox.Text = "MEASUREMENTS"
        '
        '_measurementstListView
        '
        Me._measurementstListView.AllColumns.Add(Me._instrumentColumn)
        Me._measurementstListView.AllColumns.Add(Me._measurementModelColumn)
        Me._measurementstListView.AllColumns.Add(Me._primaryMeasurementColumn)
        Me._measurementstListView.AllColumns.Add(Me._secondaryMeasurementColumn)
        Me._measurementstListView.AllColumns.Add(Me._measurementStatusColumn)
        Me._measurementstListView.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me._instrumentColumn, Me._measurementModelColumn, Me._primaryMeasurementColumn, Me._secondaryMeasurementColumn, Me._measurementStatusColumn})
        Me._measurementstListView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._measurementstListView.GridLines = True
        Me._measurementstListView.ItemRenderer = Nothing
        Me._measurementstListView.Location = New System.Drawing.Point(3, 17)
        Me._measurementstListView.Name = "_measurementstListView"
        Me._measurementstListView.ShowGroups = False
        Me._measurementstListView.Size = New System.Drawing.Size(445, 170)
        Me._measurementstListView.TabIndex = 0
        Me._measurementstListView.UseAlternatingBackColors = True
        Me._measurementstListView.UseCompatibleStateImageBehavior = False
        Me._measurementstListView.View = System.Windows.Forms.View.Details
        Me._measurementstListView.VirtualMode = True
        '
        '_instrumentColumn
        '
        Me._instrumentColumn.Text = "INSTRUMENT"
        Me._instrumentColumn.Width = 99
        '
        '_measurementModelColumn
        '
        Me._measurementModelColumn.Text = "MODEL"
        Me._measurementModelColumn.Width = 65
        '
        '_primaryMeasurementColumn
        '
        Me._primaryMeasurementColumn.Text = "PRIMARY"
        Me._primaryMeasurementColumn.Width = 79
        '
        '_secondaryMeasurementColumn
        '
        Me._secondaryMeasurementColumn.Text = "SECONDARY"
        Me._secondaryMeasurementColumn.Width = 97
        '
        '_measurementStatusColumn
        '
        Me._measurementStatusColumn.Text = "STATUS"
        Me._measurementStatusColumn.Width = 85
        '
        '_measureGroupBox
        '
        Me._measureGroupBox.Controls.Add(Me._measureParallelButton)
        Me._measureGroupBox.Controls.Add(Me._measureButton)
        Me._measureGroupBox.Controls.Add(Me._primaryValueTestBoxLabel)
        Me._measureGroupBox.Controls.Add(Me._primaryValueTestBox)
        Me._measureGroupBox.Controls.Add(Me._measurementStatusTextBox)
        Me._measureGroupBox.Controls.Add(Me._measurementStatusTextBoxLabel)
        Me._measureGroupBox.Controls.Add(Me._secondaryValueTestBox)
        Me._measureGroupBox.Controls.Add(Me._secondaryValueTestBoxLabel)
        Me._measureGroupBox.Location = New System.Drawing.Point(125, 81)
        Me._measureGroupBox.Name = "_measureGroupBox"
        Me._measureGroupBox.Size = New System.Drawing.Size(451, 123)
        Me._measureGroupBox.TabIndex = 5
        Me._measureGroupBox.TabStop = False
        Me._measureGroupBox.Text = "MEASURE"
        '
        '_measureParallelButton
        '
        Me._measureParallelButton.Location = New System.Drawing.Point(14, 82)
        Me._measureParallelButton.Name = "_measureParallelButton"
        Me._measureParallelButton.Size = New System.Drawing.Size(423, 33)
        Me._measureParallelButton.TabIndex = 5
        Me._measureParallelButton.Text = "MEASURE PARELLEL CVU"
        Me._measureParallelButton.UseVisualStyleBackColor = True
        '
        '_measureButton
        '
        Me._measureButton.Location = New System.Drawing.Point(9, 22)
        Me._measureButton.Name = "_measureButton"
        Me._measureButton.Size = New System.Drawing.Size(139, 43)
        Me._measureButton.TabIndex = 0
        Me._measureButton.Text = "&MEASURE"
        Me._measureButton.UseVisualStyleBackColor = True
        '
        '_primaryValueTestBoxLabel
        '
        Me._primaryValueTestBoxLabel.AutoSize = True
        Me._primaryValueTestBoxLabel.Location = New System.Drawing.Point(163, 26)
        Me._primaryValueTestBoxLabel.Name = "_primaryValueTestBoxLabel"
        Me._primaryValueTestBoxLabel.Size = New System.Drawing.Size(87, 15)
        Me._primaryValueTestBoxLabel.TabIndex = 1
        Me._primaryValueTestBoxLabel.Text = "CAPACITANCE"
        '
        '_primaryValueTestBox
        '
        Me._primaryValueTestBox.Location = New System.Drawing.Point(166, 43)
        Me._primaryValueTestBox.Name = "_primaryValueTestBox"
        Me._primaryValueTestBox.ReadOnly = True
        Me._primaryValueTestBox.Size = New System.Drawing.Size(80, 21)
        Me._primaryValueTestBox.TabIndex = 2
        Me._toolTip.SetToolTip(Me._primaryValueTestBox, "Cap impedance in ohms")
        '
        '_measurementStatusTextBox
        '
        Me._measurementStatusTextBox.Location = New System.Drawing.Point(362, 43)
        Me._measurementStatusTextBox.Name = "_measurementStatusTextBox"
        Me._measurementStatusTextBox.ReadOnly = True
        Me._measurementStatusTextBox.Size = New System.Drawing.Size(80, 21)
        Me._measurementStatusTextBox.TabIndex = 4
        Me._toolTip.SetToolTip(Me._measurementStatusTextBox, "Measurement Status")
        '
        '_measurementStatusTextBoxLabel
        '
        Me._measurementStatusTextBoxLabel.AutoSize = True
        Me._measurementStatusTextBoxLabel.Location = New System.Drawing.Point(376, 25)
        Me._measurementStatusTextBoxLabel.Name = "_measurementStatusTextBoxLabel"
        Me._measurementStatusTextBoxLabel.Size = New System.Drawing.Size(53, 15)
        Me._measurementStatusTextBoxLabel.TabIndex = 3
        Me._measurementStatusTextBoxLabel.Text = "STATUS"
        '
        '_secondaryValueTestBox
        '
        Me._secondaryValueTestBox.Location = New System.Drawing.Point(264, 43)
        Me._secondaryValueTestBox.Name = "_secondaryValueTestBox"
        Me._secondaryValueTestBox.ReadOnly = True
        Me._secondaryValueTestBox.Size = New System.Drawing.Size(80, 21)
        Me._secondaryValueTestBox.TabIndex = 4
        Me._toolTip.SetToolTip(Me._secondaryValueTestBox, "Phase in radians")
        '
        '_secondaryValueTestBoxLabel
        '
        Me._secondaryValueTestBoxLabel.AutoSize = True
        Me._secondaryValueTestBoxLabel.Location = New System.Drawing.Point(263, 26)
        Me._secondaryValueTestBoxLabel.Name = "_secondaryValueTestBoxLabel"
        Me._secondaryValueTestBoxLabel.Size = New System.Drawing.Size(82, 15)
        Me._secondaryValueTestBoxLabel.TabIndex = 3
        Me._secondaryValueTestBoxLabel.Text = "RESISTANCE"
        '
        '_diagnosticsTabPage
        '
        Me._diagnosticsTabPage.Controls.Add(Me._diagnosticsLayout)
        Me._diagnosticsTabPage.Location = New System.Drawing.Point(4, 24)
        Me._diagnosticsTabPage.Name = "_diagnosticsTabPage"
        Me._diagnosticsTabPage.Size = New System.Drawing.Size(708, 489)
        Me._diagnosticsTabPage.TabIndex = 4
        Me._diagnosticsTabPage.Text = "DIAGNOSTICS"
        Me._diagnosticsTabPage.UseVisualStyleBackColor = True
        '
        '_diagnosticsLayout
        '
        Me._diagnosticsLayout.ColumnCount = 3
        Me._diagnosticsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._diagnosticsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._diagnosticsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._diagnosticsLayout.Controls.Add(Me._interactiveGroupBox, 1, 1)
        Me._diagnosticsLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._diagnosticsLayout.Location = New System.Drawing.Point(0, 0)
        Me._diagnosticsLayout.Name = "_diagnosticsLayout"
        Me._diagnosticsLayout.RowCount = 3
        Me._diagnosticsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._diagnosticsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._diagnosticsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._diagnosticsLayout.Size = New System.Drawing.Size(708, 489)
        Me._diagnosticsLayout.TabIndex = 0
        '
        '_interactiveGroupBox
        '
        Me._interactiveGroupBox.Controls.Add(Me._speedTextBox)
        Me._interactiveGroupBox.Controls.Add(Me._speedTextBoxLabel)
        Me._interactiveGroupBox.Controls.Add(Me._busyStatusLabel)
        Me._interactiveGroupBox.Controls.Add(Me._errorAvailableLabel)
        Me._interactiveGroupBox.Controls.Add(Me._messageAvailabelLabel)
        Me._interactiveGroupBox.Controls.Add(Me._srqLabel)
        Me._interactiveGroupBox.Controls.Add(Me._sendReceiveButton)
        Me._interactiveGroupBox.Controls.Add(Me._replyTextBox)
        Me._interactiveGroupBox.Controls.Add(Me._commandComboBox)
        Me._interactiveGroupBox.Controls.Add(Me.Label1)
        Me._interactiveGroupBox.Location = New System.Drawing.Point(155, 143)
        Me._interactiveGroupBox.Name = "_interactiveGroupBox"
        Me._interactiveGroupBox.Size = New System.Drawing.Size(397, 203)
        Me._interactiveGroupBox.TabIndex = 0
        Me._interactiveGroupBox.TabStop = False
        Me._interactiveGroupBox.Text = "KXCI INTERACTIVE"
        '
        '_speedTextBox
        '
        Me._speedTextBox.Location = New System.Drawing.Point(113, 175)
        Me._speedTextBox.Name = "_speedTextBox"
        Me._speedTextBox.ReadOnly = True
        Me._speedTextBox.Size = New System.Drawing.Size(100, 21)
        Me._speedTextBox.TabIndex = 6
        '
        '_speedTextBoxLabel
        '
        Me._speedTextBoxLabel.AutoSize = True
        Me._speedTextBoxLabel.Location = New System.Drawing.Point(16, 177)
        Me._speedTextBoxLabel.Name = "_speedTextBoxLabel"
        Me._speedTextBoxLabel.Size = New System.Drawing.Size(95, 15)
        Me._speedTextBoxLabel.TabIndex = 5
        Me._speedTextBoxLabel.Text = "Execution Time:"
        Me._speedTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_busyStatusLabel
        '
        Me._busyStatusLabel.AutoSize = True
        Me._busyStatusLabel.BackColor = System.Drawing.SystemColors.Control
        Me._busyStatusLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._busyStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._busyStatusLabel.Location = New System.Drawing.Point(56, 155)
        Me._busyStatusLabel.Name = "_busyStatusLabel"
        Me._busyStatusLabel.Size = New System.Drawing.Size(41, 17)
        Me._busyStatusLabel.TabIndex = 4
        Me._busyStatusLabel.Text = "BUSY"
        Me._toolTip.SetToolTip(Me._busyStatusLabel, "Busy")
        '
        '_errorAvailableLabel
        '
        Me._errorAvailableLabel.AutoSize = True
        Me._errorAvailableLabel.BackColor = System.Drawing.SystemColors.Control
        Me._errorAvailableLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._errorAvailableLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._errorAvailableLabel.Location = New System.Drawing.Point(99, 155)
        Me._errorAvailableLabel.Name = "_errorAvailableLabel"
        Me._errorAvailableLabel.Size = New System.Drawing.Size(31, 17)
        Me._errorAvailableLabel.TabIndex = 4
        Me._errorAvailableLabel.Text = "EAV"
        Me._toolTip.SetToolTip(Me._errorAvailableLabel, "Error Available")
        '
        '_messageAvailabelLabel
        '
        Me._messageAvailabelLabel.AutoSize = True
        Me._messageAvailabelLabel.BackColor = System.Drawing.SystemColors.Control
        Me._messageAvailabelLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._messageAvailabelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._messageAvailabelLabel.Location = New System.Drawing.Point(132, 155)
        Me._messageAvailabelLabel.Name = "_messageAvailabelLabel"
        Me._messageAvailabelLabel.Size = New System.Drawing.Size(34, 17)
        Me._messageAvailabelLabel.TabIndex = 4
        Me._messageAvailabelLabel.Text = "MAV"
        Me._toolTip.SetToolTip(Me._messageAvailabelLabel, "Data Available")
        '
        '_srqLabel
        '
        Me._srqLabel.AutoSize = True
        Me._srqLabel.BackColor = System.Drawing.SystemColors.Control
        Me._srqLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._srqLabel.Cursor = System.Windows.Forms.Cursors.Hand
        Me._srqLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._srqLabel.Location = New System.Drawing.Point(19, 155)
        Me._srqLabel.Name = "_srqLabel"
        Me._srqLabel.Size = New System.Drawing.Size(35, 17)
        Me._srqLabel.TabIndex = 4
        Me._srqLabel.Text = "SRQ"
        Me._toolTip.SetToolTip(Me._srqLabel, "Service Requested")
        '
        '_sendReceiveButton
        '
        Me._sendReceiveButton.Location = New System.Drawing.Point(251, 155)
        Me._sendReceiveButton.Name = "_sendReceiveButton"
        Me._sendReceiveButton.Size = New System.Drawing.Size(129, 37)
        Me._sendReceiveButton.TabIndex = 3
        Me._sendReceiveButton.Text = "SEND / RECEIVE"
        Me._sendReceiveButton.UseVisualStyleBackColor = True
        '
        '_replyTextBox
        '
        Me._replyTextBox.Location = New System.Drawing.Point(19, 54)
        Me._replyTextBox.Multiline = True
        Me._replyTextBox.Name = "_replyTextBox"
        Me._replyTextBox.ReadOnly = True
        Me._replyTextBox.Size = New System.Drawing.Size(361, 95)
        Me._replyTextBox.TabIndex = 2
        '
        '_commandComboBox
        '
        Me._commandComboBox.FormattingEnabled = True
        Me._commandComboBox.Items.AddRange(New Object() {":CVU:ACV 0.05", ":CVU:ACZ:RANGE 0.001", ":CVU:BIAS:DCV:SAMPLE 1.0,1", ":CVU:CHANNEL 1", ":CVU:CHANNELS?", ":CVU:CORRECT 1, 1, 1", ":CVU:DATA:Z?", ":CVU:DATA:VOLT?", ":CVU:DATA:FREQ?", ":CVU:DATA:STATUS?", ":CVU:DATA:TSTAMP?", ":CVU:DCV 1.0", ":CVU:FREQ 10000", ":CVU:LENGTH 1.5", ":CVU:MEASZ?", ":CVU:MODE 1", ":CVU:MODEL 2", ":CVU:PARALLEL:ENABLE 0", ":CVU:PARALLEL:ENABLE 1", ":CVU:PARALLEL:RUN", ":CVU:RESET", ":CVU:SAMPLE:HOLDT 0", ":CVU:SAMPLE:INTERVAL 0", ":CVU:SPEED 1", ":CVU:SPEED 3,1.0,1.0,1.002", ":CVU:TEST:COMPLETE? 1", ":CVU:TEST:RUN"})
        Me._commandComboBox.Location = New System.Drawing.Point(92, 25)
        Me._commandComboBox.Name = "_commandComboBox"
        Me._commandComboBox.Size = New System.Drawing.Size(288, 23)
        Me._commandComboBox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "COMMAND:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_messagesTabPage
        '
        Me._messagesTabPage.Controls.Add(Me._messagesBox)
        Me._messagesTabPage.Location = New System.Drawing.Point(4, 24)
        Me._messagesTabPage.Name = "_messagesTabPage"
        Me._messagesTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._messagesTabPage.Size = New System.Drawing.Size(708, 489)
        Me._messagesTabPage.TabIndex = 1
        Me._messagesTabPage.Text = "MESSAGES"
        Me._messagesTabPage.UseVisualStyleBackColor = True
        '
        '_errorProvider
        '
        Me._errorProvider.ContainerControl = Me
        '
        '_refreshTimer
        '
        Me._refreshTimer.Interval = 1000
        '
        'TestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(716, 539)
        Me.Controls.Add(Me._tabControl)
        Me.Controls.Add(Me._statusStrip)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "TestPanel"
        Me.Text = "Keithley Test Environment Interface Tester"
        Me._statusStrip.ResumeLayout(False)
        Me._statusStrip.PerformLayout()
        Me._tabControl.ResumeLayout(False)
        Me._stationTabPage.ResumeLayout(False)
        Me._stationLayout.ResumeLayout(False)
        Me._testSystemGroupBox.ResumeLayout(False)
        Me._controlModeGroupBox.ResumeLayout(False)
        Me._controlModeGroupBox.PerformLayout()
        Me._instrumentsTabPage.ResumeLayout(False)
        Me._instrumentsLayout.ResumeLayout(False)
        Me._instrumentGroupBox.ResumeLayout(False)
        Me._instrumentGroupBox.PerformLayout()
        CType(Me._channelCountNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._instrumentNumberNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._impedanceGroupBox.ResumeLayout(False)
        Me._impedanceGroupBox.PerformLayout()
        Me._compensationGroupBox.ResumeLayout(False)
        Me._compensationGroupBox.PerformLayout()
        CType(Me._filterNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._delayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._apertureNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._currentRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._sourceLevelNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._frequencyNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._sourceBiasNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._impedanceTabPage.ResumeLayout(False)
        Me._impedanceLayout.ResumeLayout(False)
        Me._measurementsGroupBox.ResumeLayout(False)
        CType(Me._measurementstListView, System.ComponentModel.ISupportInitialize).EndInit()
        Me._measureGroupBox.ResumeLayout(False)
        Me._measureGroupBox.PerformLayout()
        Me._diagnosticsTabPage.ResumeLayout(False)
        Me._diagnosticsLayout.ResumeLayout(False)
        Me._interactiveGroupBox.ResumeLayout(False)
        Me._interactiveGroupBox.PerformLayout()
        Me._messagesTabPage.ResumeLayout(False)
        Me._messagesTabPage.PerformLayout()
        CType(Me._errorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _statusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _statusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _messagesBox As isr.Controls.MessagesBox
    Private WithEvents _tabControl As System.Windows.Forms.TabControl
    Private WithEvents _impedanceTabPage As System.Windows.Forms.TabPage
    Private WithEvents _secondaryValueTestBox As System.Windows.Forms.TextBox
    Private WithEvents _toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _secondaryValueTestBoxLabel As System.Windows.Forms.Label
    Private WithEvents _primaryValueTestBox As System.Windows.Forms.TextBox
    Private WithEvents _primaryValueTestBoxLabel As System.Windows.Forms.Label
    Private WithEvents _measureButton As System.Windows.Forms.Button
    Private WithEvents _messagesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _errorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _measureGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _impedanceGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _stationTabPage As System.Windows.Forms.TabPage
    Private WithEvents _stationLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _testSystemGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _usingDevicesCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _initializeButton As System.Windows.Forms.Button
    Private WithEvents _impedanceLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _applyButton As System.Windows.Forms.Button
    Private WithEvents _closeStationButton As System.Windows.Forms.Button
    Private WithEvents _sourceBiasNumericLabel As System.Windows.Forms.Label
    Private WithEvents _sourceBiasNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _frequencyNumericLabel As System.Windows.Forms.Label
    Private WithEvents _frequencyNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _currentRangeNumericLabel As System.Windows.Forms.Label
    Private WithEvents _sourceLevelNumericLabel As System.Windows.Forms.Label
    Private WithEvents _currentRangeNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _sourceLevelNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _modelComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _modelComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _speedComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _speedComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _compensationGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _loadCompensationCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _openCompensationCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _shortCompensationCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _instrumentsTabPage As System.Windows.Forms.TabPage
    Private WithEvents _instrumentsLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _instrumentGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _addInstrumentButton As System.Windows.Forms.Button
    Private WithEvents _instrumentNumberNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _identifierTextBox As System.Windows.Forms.TextBox
    Private WithEvents _instrumentFamilyNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents _instrumentNumberNumericLabel As System.Windows.Forms.Label
    Private WithEvents _instrumentFamilyNameTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _identifiedTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _cableComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _cableComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _apertureNumericLabel As System.Windows.Forms.Label
    Private WithEvents _apertureNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _delayNumericLabel As System.Windows.Forms.Label
    Private WithEvents _delayNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _filterNumericLabel As System.Windows.Forms.Label
    Private WithEvents _filterNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _selectInstrumentButton As System.Windows.Forms.Button
    Private WithEvents _availabelInstrumentsListBoxLabel As System.Windows.Forms.Label
    Private WithEvents _availabelInstrumentsListBox As System.Windows.Forms.ListBox
    Private WithEvents _measurementsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _measurementstListView As BrightIdeasSoftware.FastObjectListView
    Private WithEvents _instrumentColumn As BrightIdeasSoftware.OLVColumn
    Private WithEvents _measurementModelColumn As BrightIdeasSoftware.OLVColumn
    Private WithEvents _primaryMeasurementColumn As BrightIdeasSoftware.OLVColumn
    Private WithEvents _secondaryMeasurementColumn As BrightIdeasSoftware.OLVColumn
    Private WithEvents _controlModeGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _externalControlInterfaceRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _lowLevelControlRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _resetHardwareButton As System.Windows.Forms.Button
    Private WithEvents _measurementStatusTextBox As System.Windows.Forms.TextBox
    Private WithEvents _measurementStatusTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _measurementStatusColumn As BrightIdeasSoftware.OLVColumn
    Private WithEvents _parallelCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _measureParallelButton As System.Windows.Forms.Button
    Private WithEvents _forceMultiCvu As System.Windows.Forms.CheckBox
    Private WithEvents _instrumentStatusLabel As System.Windows.Forms.Label
    Private WithEvents _diagnosticsTabPage As System.Windows.Forms.TabPage
    Private WithEvents _diagnosticsLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _interactiveGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _commandComboBox As System.Windows.Forms.ComboBox
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents _statusByteLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _functionStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _socketStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _srqLabel As System.Windows.Forms.Label
    Private WithEvents _sendReceiveButton As System.Windows.Forms.Button
    Private WithEvents _replyTextBox As System.Windows.Forms.TextBox
    Private WithEvents _busyStatusLabel As System.Windows.Forms.Label
    Private WithEvents _errorAvailableLabel As System.Windows.Forms.Label
    Private WithEvents _messageAvailabelLabel As System.Windows.Forms.Label
    Private WithEvents _refreshTimer As System.Windows.Forms.Timer
    Private WithEvents _openStationButton As System.Windows.Forms.Button
    Private WithEvents _speedTextBox As System.Windows.Forms.TextBox
    Private WithEvents _speedTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _startHostButton As System.Windows.Forms.Button
    Private WithEvents _channelCountNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _channelCountNumericLabel As System.Windows.Forms.Label
    Private WithEvents _enabledCheckBox As System.Windows.Forms.CheckBox

End Class
