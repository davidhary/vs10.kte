﻿''' <summary>
''' Implements a low-level <see cref="IInstrument">Instrument</see> 
''' using the Keithley Test Environment Linear Parametric Test (LPT) library.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
Public Class LowLevelInstrument

    Inherits BaseInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " IINSTRUMENT "

    Public Overrides Function AwaitTestCompletion(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean
        MyBase.OperationStatus = isr.Kte.OperationStatus.Success
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function Exists() As Boolean
        Return isr.Kte.Lpt.SafeNativeMethods.IsModuleExists(MyBase.InstrumentId)
    End Function

    Public Overrides Function SelectChannel() As Boolean
        Return True
    End Function

    Public Overrides Function MeasureImpedance(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean
        MyBase.InitiateTest()
        MyBase.Impedance = New Impedance(isr.Kte.Lpt.SafeNativeMethods.MeasureImpedance(MyBase.InstrumentId,
                                         CType(MyBase.MeasurementModel.ActualValue, ImpedanceModel),
                                         CType(MyBase.MeasurementSpeed.CacheValue, MeasurementSpeed)))
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If Not MyBase.Impedance.HasValue OrElse MyBase.LastOperationFailed Then
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.MessageQueue.Enqueue("Instrument {0} failed measuring impedance. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function QueryTestCompleted() As Boolean
        MyBase.OperationStatus = isr.Kte.OperationStatus.Success
        Return True
    End Function

    Public Overrides Function ResetKnownState() As Boolean
        isr.Kte.Lpt.SafeNativeMethods.ResetKnownState(MyBase.InstrumentId)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        Return MyBase.ResetKnownState()
    End Function

    Public Overrides Function SelectImpedanceInstrument(ByVal channelNumber As Integer) As Boolean

        MyBase.InstrumentFamilyName = "CVU"
        MyBase.ChannelNumber = channelNumber
        MyBase.InstrumentName = BaseInstrument.BuildImpedanceInstrumentName(channelNumber)
        Dim id As Integer? = isr.Kte.Lpt.SafeNativeMethods.InstrumentIdGetter(MyBase.InstrumentName)
        If id.HasValue Then
            MyBase.InstrumentId = id.Value
            ' check if instrument exists.
            If isr.Kte.Lpt.SafeNativeMethods.IsModuleExists(MyBase.InstrumentId) Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.Success
            Else
                MyBase.MessageQueue.Enqueue("Failed locating instrument '{0}'", MyBase.InstrumentName)
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
        Else
            MyBase.MessageQueue.Enqueue("Failed getting instrument id for instrument name {0}. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
        End If
        Return MyBase.LastOperationSuccess
    End Function

#End Region

#Region " IINSTRUMENT STATUS "

    ''' <summary>
    ''' Returns overall status details
    ''' </summary>
    Public Overrides Function LastStatusDetails() As String
        Return isr.Kte.Lpt.SafeNativeMethods.LastStatusDetails()
    End Function

#End Region

#Region " IINSTRUMENT: IMPEDANCE "

    Public Overrides Function ApertureGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.ApertureGetter(MyBase.InstrumentId, MyBase.Aperture.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.Aperture.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.Aperture.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting aperture from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function ApertureSetter() As Boolean
        Dim value As Single = MyBase.Aperture.CacheValue
        MyBase.Aperture.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.ApertureSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.ApertureGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting aperture to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function CableLengthGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.CableLengthGetter(MyBase.InstrumentId, MyBase.CableLength.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.CableLength.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.CableLength.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting cable length correction from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function CableLengthSetter() As Boolean
        Dim value As Single = MyBase.CableLength.CacheValue
        MyBase.CableLength.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.CableLengthSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.CableLengthGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting cable length to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function DelayFactorGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.DelayFactorGetter(MyBase.InstrumentId, MyBase.DelayFactor.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.DelayFactor.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.DelayFactor.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting delay factor from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function DelayFactorSetter() As Boolean
        Dim value As Single = MyBase.DelayFactor.CacheValue
        MyBase.DelayFactor.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.DelayFactorSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.DelayFactorGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting delay factor to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function FilterFactorGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.FilterFactorGetter(MyBase.InstrumentId, MyBase.FilterFactor.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.FilterFactor.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.FilterFactor.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting filter factor from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function FilterFactorSetter() As Boolean
        Dim value As Single = MyBase.FilterFactor.CacheValue
        MyBase.FilterFactor.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.FilterFactorSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.FilterFactorGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting filter factor to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function HoldTimeGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.HoldTimeGetter(MyBase.InstrumentId, MyBase.HoldTime.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.HoldTime.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.HoldTime.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting Hold Time from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function HoldTimeSetter() As Boolean
        Dim value As Single = MyBase.HoldTime.CacheValue
        MyBase.HoldTime.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.HoldTimeSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.HoldTimeGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Hold Time to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function ImpedanceGetter() As Boolean
        ' nothing to do at this time as the impedance is fetched with the measurement.
        Return True
    End Function

    Public Overrides Function LoadCompensationEnabledGetter() As Boolean
        Dim value As Boolean? = isr.Kte.Lpt.SafeNativeMethods.LoadCompensationGetter(MyBase.InstrumentId, CSng(IIf(MyBase.LoadCompensationEnabled.CacheValue, 1, 0)))
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.LoadCompensationEnabled.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.LoadCompensationEnabled.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting load compensation condition from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function LoadCompensationEnabledSetter() As Boolean
        Dim value As Boolean = MyBase.LoadCompensationEnabled.CacheValue
        MyBase.LoadCompensationEnabled.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LoadCompensationSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.LoadCompensationEnabledGetter()
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Instrument {0} failed getting load compensation after setting it to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting load compensation to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function MeasurementModelGetter() As Boolean
        Dim value As Integer? = isr.Kte.Lpt.SafeNativeMethods.MeasurementModelGetter(MyBase.InstrumentId, MyBase.MeasurementModel.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.MeasurementModel.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.MeasurementModel.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting measurement model from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="value")>
    Protected Overrides Function MeasurementModelSetter() As Boolean
        Dim value As Integer = MyBase.MeasurementModel.CacheValue
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function MeasurementSpeedGetter() As Boolean
        Dim value As Integer? = isr.Kte.Lpt.SafeNativeMethods.MeasurementSpeedGetter(MyBase.InstrumentId, MyBase.MeasurementSpeed.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.MeasurementSpeed.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.MeasurementSpeed.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting measurement speed from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Sets the measurements speed from the cached value
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="value")>
    Protected Overrides Function MeasurementSpeedSetter() As Boolean
        Dim value As Integer = MyBase.MeasurementSpeed.CacheValue
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function MeasurementStatusGetter() As Boolean
        Dim value As Integer? = isr.Kte.Lpt.SafeNativeMethods.MeasurementStatusGetter(MyBase.InstrumentId, MyBase.MeasurementStatus)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.MeasurementStatus = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.MeasurementStatus = 0
            MyBase.MessageQueue.Enqueue("Failed getting measurement status from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function OpenCompensationEnabledGetter() As Boolean
        Dim value As Boolean? = isr.Kte.Lpt.SafeNativeMethods.OpenCompensationGetter(MyBase.InstrumentId, CSng(IIf(MyBase.OpenCompensationEnabled.CacheValue, 1, 0)))
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.OpenCompensationEnabled.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.OpenCompensationEnabled.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting open compensation condition from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function OpenCompensationEnabledSetter() As Boolean
        Dim value As Boolean = MyBase.OpenCompensationEnabled.CacheValue
        MyBase.OpenCompensationEnabled.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.OpenCompensationSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.OpenCompensationEnabledGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting open compensation to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Gets the sampling bias.  At this point this returns the source bias as the 
    ''' low level instrument suports only single sample measurements.
    ''' </summary>
    Public Overrides Function SamplingBiasGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.SamplingBiasGetter(MyBase.InstrumentId, MyBase.SamplingBias.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.SamplingBias.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.SamplingBias.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting sampling bias from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SamplingBiasSetter() As Boolean
        Dim value As Single = MyBase.SamplingBias.CacheValue
        MyBase.SamplingBias.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.SamplingBiasSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.SamplingBiasGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting sampling bias to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Gets the sampling count.  At this point this returns 1 as the 
    ''' low level instrument suports only single sample measurements.
    ''' </summary>
    Public Overrides Function SamplingCountGetter() As Boolean
        Dim value As Integer? = isr.Kte.Lpt.SafeNativeMethods.SamplingCountGetter(MyBase.InstrumentId, MyBase.SamplingCount.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.SamplingCount.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.SamplingCount.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting sampling Count from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Sets the sampling count for sampling.
    ''' At this time the low level instrument supports a single measurement only.
    ''' </summary>
    Protected Overrides Function SamplingCountSetter() As Boolean
        Dim value As Integer = MyBase.SamplingCount.CacheValue
        MyBase.SamplingCount.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.SamplingCountSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.SamplingCountGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting sampling count frequency to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SamplingIntervalGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.SamplingIntervalGetter(MyBase.InstrumentId, MyBase.SamplingInterval.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.SamplingInterval.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.SamplingInterval.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting Sampling Interval from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SamplingIntervalSetter() As Boolean
        Dim value As Single = MyBase.SamplingInterval.CacheValue
        MyBase.SamplingInterval.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.SamplingIntervalSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.SamplingIntervalGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Sampling Interval to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SenseCurrentRangeGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.SenseCurrentRangeGetter(MyBase.InstrumentId, MyBase.SenseCurrentRange.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.SenseCurrentRange.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.SenseCurrentRange.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting sense current range from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SenseCurrentRangeSetter() As Boolean
        Dim value As Single = MyBase.SenseCurrentRange.CacheValue
        MyBase.SenseCurrentRange.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.SenseCurrentRangeSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.SenseCurrentRangeGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting sense current range to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Public Overrides Function ShortCompensationEnabledGetter() As Boolean
        Dim value As Boolean? = isr.Kte.Lpt.SafeNativeMethods.ShortCompensationGetter(MyBase.InstrumentId, CSng(IIf(MyBase.ShortCompensationEnabled.CacheValue, 1, 0)))
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.ShortCompensationEnabled.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.ShortCompensationEnabled.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting short compensation condition from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Protected Overrides Function ShortCompensationEnabledSetter() As Boolean
        Dim value As Boolean = MyBase.ShortCompensationEnabled.CacheValue
        MyBase.ShortCompensationEnabled.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.ShortCompensationSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.ShortCompensationEnabledGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting short compensation to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SourceBiasGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.DirectVoltageLevelGetter(MyBase.InstrumentId, MyBase.SourceBias.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.SourceBias.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.SourceBias.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting source bias from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SourceBiasSetter() As Boolean
        Dim value As Single = MyBase.SourceBias.CacheValue
        MyBase.SourceBias.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.DirectVoltageLevelSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.SourceBiasGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting source bias to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SourceFrequencyGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.SourceFrequencyGetter(MyBase.InstrumentId, MyBase.SourceFrequency.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.SourceFrequency.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.SourceFrequency.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting source frequency from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SourceFrequencySetter() As Boolean
        Dim value As Single = MyBase.SourceFrequency.CacheValue
        MyBase.SourceFrequency.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.SourceFrequencySetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.SourceFrequencyGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting source frequency to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SourceLevelGetter() As Boolean
        Dim value As Single? = isr.Kte.Lpt.SafeNativeMethods.AlternatingVoltageLevelGetter(MyBase.InstrumentId, MyBase.SourceLevel.CacheValue)
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.LastOperationStatus
        If value.HasValue AndAlso MyBase.LastOperationSuccess Then
            MyBase.SourceLevel.ActualValue = value.Value
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.SourceLevel.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting source level from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SourceLevelSetter() As Boolean
        Dim value As Single = MyBase.SourceLevel.CacheValue
        MyBase.SourceLevel.HasActualValue = False
        MyBase.OperationStatus = isr.Kte.Lpt.SafeNativeMethods.AlternatingVoltageLevelSetter(MyBase.InstrumentId, value)
        If MyBase.LastOperationSuccess Then
            Me.SourceLevelGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting source level to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

#End Region

End Class
