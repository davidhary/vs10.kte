﻿''' <summary>
''' Common class for implementing an <see cref="IInstrument">Instrument</see>.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
''' <history date="02/16/2010" by="David" revision="1.0.3699.x">
''' Remove all default values.
''' </history>
Public MustInherit Class BaseInstrument

    Implements IInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
    Protected Sub New()
        MyBase.New()

        ' defaults to enabled.
        Me._enabled = True

        ' initialize the message queue. 
        Me._messageQueue = New isr.Core.MessageQueue

        Me._presettables = New isr.Core.PresettableCachedCollection()

        Me._aperture = New isr.Core.PresettableFormattableCache(Of Single)("Aperture", 1.002)
        ' Me._aperture.DefaultValue = Me._aperture.CacheValue
        Me._aperture.EpsilonSetter(0.0001)
        Me._aperture.ReadingFormat = "0.####"
        Me._aperture.Setter = New Action(Of Single)(AddressOf ApertureSetter)
        Me._aperture.Getter = New Action(AddressOf ApertureGetter)
        Me._presettables.Add(Me._aperture)

        Me._cableCorrection = CorrectedCableLength.OnePointFiveMeters
        Me._cableLength = New isr.Core.PresettableFormattableCache(Of Single)("CableCorrection", BaseInstrument.ParseCableLength(Me._cableCorrection))
        ' Me._cableLength.DefaultValue = Me._cableLength.CacheValue
        Me._cableLength.Setter = New Action(Of Single)(AddressOf CableLengthSetter)
        Me._cableLength.Getter = New Action(AddressOf CableLengthGetter)
        Me._presettables.Add(Me._cableLength)

        Me._delayFactor = New isr.Core.PresettableFormattableCache(Of Single)("DelayFactor", 0.7)
        ' Me._delayFactor.DefaultValue = Me._delayFactor.CacheValue
        Me._delayFactor.EpsilonSetter(0.001)
        Me._delayFactor.ReadingFormat = "0.###"
        Me._delayFactor.Setter = New Action(Of Single)(AddressOf DelayFactorSetter)
        Me._delayFactor.Getter = New Action(AddressOf DelayFactorGetter)
        Me._presettables.Add(Me._delayFactor)

        Me._filterFactor = New isr.Core.PresettableFormattableCache(Of Single)("FilterFactor", 0.8)
        ' Me._filterFactor.DefaultValue = Me._filterFactor.CacheValue
        Me._filterFactor.EpsilonSetter(0.001)
        Me._filterFactor.ReadingFormat = "0.###"
        Me._filterFactor.Setter = New Action(Of Single)(AddressOf FilterFactorSetter)
        Me._filterFactor.Getter = New Action(AddressOf FilterFactorGetter)
        Me._presettables.Add(Me._filterFactor)

        Me._holdTime = New isr.Core.PresettableFormattableCache(Of Single)("HoldTime", 1)
        ' Me._holdTime.DefaultValue = Me._holdTime.CacheValue
        Me._holdTime.Setter = New Action(Of Single)(AddressOf HoldTimeSetter)
        Me._holdTime.Getter = New Action(AddressOf HoldTimeGetter)
        Me._presettables.Add(Me._holdTime)

        Me._loadCompensationEnabled = New isr.Core.PresettableCache(Of Boolean)("LoadCompensation", False)
        '_loadCompensationEnabled.DefaultValue = Me._loadCompensationEnabled.CacheValue
        Me._loadCompensationEnabled.Setter = New Action(Of Boolean)(AddressOf LoadCompensationEnabledSetter)
        Me._loadCompensationEnabled.Getter = New Action(AddressOf LoadCompensationEnabledGetter)
        Me._presettables.Add(Me._loadCompensationEnabled)

        ' it seems the cartesian model is not the default model. So no default model is set.
        Me._measurementModel = New isr.Core.PresettableFormattableCache(Of Integer)("MeasurementModel", CInt(Kte.ImpedanceModel.ParallelDissipation))
        ' Me._measurementModel.DefaultValue = Me._measurementModel.CacheValue
        Me._measurementModel.Setter = New Action(Of Integer)(AddressOf MeasurementModelSetter)
        Me._measurementModel.Getter = New Action(AddressOf MeasurementModelGetter)
        Me._presettables.Add(Me._measurementModel)

        Me._measurementSpeed = New isr.Core.PresettableFormattableCache(Of Integer)("MeasurementSpeed", Kte.MeasurementSpeed.Normal)
        '_measurementSpeed.DefaultValue = Me._measurementSpeed.CacheValue
        Me._measurementSpeed.Setter = New Action(Of Integer)(AddressOf MeasurementSpeedSetter)
        Me._measurementSpeed.Getter = New Action(AddressOf MeasurementSpeedGetter)
        Me._presettables.Add(Me._measurementSpeed)

        Me._openCompensationEnabled = New isr.Core.PresettableCache(Of Boolean)("OpenCompensation", False)
        '_openCompensationEnabled.DefaultValue = Me._openCompensationEnabled.CacheValue
        Me._openCompensationEnabled.Setter = New Action(Of Boolean)(AddressOf OpenCompensationEnabledSetter)
        Me._openCompensationEnabled.Getter = New Action(AddressOf OpenCompensationEnabledGetter)
        Me._presettables.Add(Me._openCompensationEnabled)

        Me._samplingBias = New isr.Core.PresettableFormattableCache(Of Single)("SamplingBias", 1)
        '_samplingBias.DefaultValue = Me._samplingBias.CacheValue
        Me._samplingBias.EpsilonSetter(0.001)
        Me._samplingBias.ReadingFormat = "0.###"
        Me._samplingBias.Setter = New Action(Of Single)(AddressOf SamplingBiasSetter)
        Me._samplingBias.Getter = New Action(AddressOf SamplingBiasGetter)
        Me._presettables.Add(Me._samplingBias)

        Me._samplingCount = New isr.Core.PresettableFormattableCache(Of Integer)("SamplingCount", 1)
        ' Me._samplingCount.DefaultValue = Me._samplingCount.CacheValue
        Me._samplingCount.Setter = New Action(Of Integer)(AddressOf SamplingCountSetter)
        Me._samplingCount.Getter = New Action(AddressOf SamplingCountGetter)
        Me._presettables.Add(Me._samplingCount)

        Me._samplingInterval = New isr.Core.PresettableFormattableCache(Of Single)("SamplingInterval", 1)
        ' Me._samplingInterval.DefaultValue = Me._samplingInterval.CacheValue
        Me._samplingInterval.EpsilonSetter(0.001)
        Me._samplingInterval.ReadingFormat = "0.###"
        Me._samplingInterval.Setter = New Action(Of Single)(AddressOf SamplingIntervalSetter)
        Me._samplingInterval.Getter = New Action(AddressOf SamplingIntervalGetter)
        Me._presettables.Add(Me._samplingInterval)

        Me._senseCurrentRange = New isr.Core.PresettableFormattableCache(Of Single)("SenseCurrentRange", 0.001)
        ' Me._senseCurrentRange.DefaultValue = Me._senseCurrentRange.CacheValue
        Me._senseCurrentRange.EpsilonSetter(0.0000001)
        Me._senseCurrentRange.ReadingFormat = "0.0#####"
        Me._senseCurrentRange.Setter = New Action(Of Single)(AddressOf SenseCurrentRangeSetter)
        Me._senseCurrentRange.Getter = New Action(AddressOf SenseCurrentRangeGetter)
        Me._presettables.Add(Me._senseCurrentRange)

        Me._settlingTime = 0.1

        Me._shortCompensationEnabled = New isr.Core.PresettableCache(Of Boolean)("ShortCompensation", False)
        '_shortCompensationEnabled.DefaultValue = Me._shortCompensationEnabled.CacheValue
        Me._shortCompensationEnabled.Setter = New Action(Of Boolean)(AddressOf ShortCompensationEnabledSetter)
        Me._shortCompensationEnabled.Getter = New Action(AddressOf ShortCompensationEnabledGetter)
        Me._presettables.Add(Me._shortCompensationEnabled)

        Me._sourceBias = New isr.Core.PresettableFormattableCache(Of Single)("SourceBias", 0)
        ' Me._sourceBias.DefaultValue = Me._sourceBias.CacheValue
        Me._sourceBias.EpsilonSetter(0.001)
        Me._sourceBias.ReadingFormat = "0.###"
        Me._sourceBias.Setter = New Action(Of Single)(AddressOf SourceBiasSetter)
        Me._sourceBias.Getter = New Action(AddressOf SourceBiasGetter)
        Me._presettables.Add(Me._sourceBias)

        Me._sourceFrequency = New isr.Core.PresettableFormattableCache(Of Single)("SourceFrequency", 100000)
        ' Me._sourceFrequency.DefaultValue = Me._sourceFrequency.CacheValue
        Me._sourceFrequency.EpsilonSetter(0.1)
        Me._sourceFrequency.ReadingFormat = "0"
        Me._sourceFrequency.Setter = New Action(Of Single)(AddressOf SourceFrequencySetter)
        Me._sourceFrequency.Getter = New Action(AddressOf SourceFrequencyGetter)
        Me._presettables.Add(Me._sourceFrequency)

        Me._sourceLevel = New isr.Core.PresettableFormattableCache(Of Single)("SourceLevel", 0.03)
        ' Me._sourceLevel.DefaultValue = Me._sourceLevel.CacheValue
        Me._sourceLevel.EpsilonSetter(0.0001)
        Me._sourceLevel.ReadingFormat = "0.0###"
        Me._sourceLevel.Setter = New Action(Of Single)(AddressOf SourceLevelSetter)
        Me._sourceLevel.Getter = New Action(AddressOf SourceLevelGetter)
        Me._presettables.Add(Me._sourceLevel)

    End Sub

#End Region

#Region " HELPER FUNCTIONS "

    ''' <summary>
    ''' Builds an instrument name from instrument family name and number.
    ''' </summary>
    ''' <param name="familyName"></param>
    ''' <param name="instrumentNumber"></param>
    Public Shared Function BuildInstrumentName(ByVal familyName As String, ByVal instrumentNumber As Integer) As String
        Return String.Format(Globalization.CultureInfo.InvariantCulture, "{0}{1}", familyName, instrumentNumber)
    End Function

    ''' <summary>
    ''' Builds an instrument name from instrument family name and number.
    ''' </summary>
    ''' <param name="familyName"></param>
    ''' <param name="instrumentNumber"></param>
    Public Shared Function BuildInstrumentName(ByVal familyName As String, ByVal instrumentNumber As String) As String
        Return BuildInstrumentName(familyName, CInt(instrumentNumber))
    End Function

    ''' <summary>
    ''' Builds an instrument name from instrument family name and number.
    ''' </summary>
    ''' <param name="instrumentNumber"></param>
    Public Shared Function BuildImpedanceInstrumentName(ByVal instrumentNumber As Integer) As String
        Return BuildInstrumentName("CVU", instrumentNumber)
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="status">measurement status</paramref> indicates
    ''' that the measurement failed due to time out.
    ''' </summary>
    Public Shared Function IsMeasurementFailed(ByVal status As Integer) As Boolean
        Return (status And (isr.Kte.MeasurementStatuses.HighCurrentOverflow +
                                            MeasurementStatuses.HighNotLocked +
                                            MeasurementStatuses.HighVoltageOverflow +
                                            MeasurementStatuses.LowCurrentOverflow +
                                            MeasurementStatuses.LowNotLocked +
                                            MeasurementStatuses.LowVoltageOverflow +
                                            MeasurementStatuses.Timeout)) <> 0
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="status">measurement status</paramref> indicates
    ''' that the measurement failed due to time out.
    ''' </summary>
    Public Shared Function IsMeasurementTimedOut(ByVal status As Integer) As Boolean
        Return (status And isr.Kte.MeasurementStatuses.Timeout) <> 0
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="status">measurement status</paramref> indicates
    ''' that the measurement failed due to overflow.
    ''' </summary>
    Public Shared Function IsMeasurementOverflow(ByVal status As Integer) As Boolean
        Return (status And
                (isr.Kte.MeasurementStatuses.HighCurrentOverflow +
                 MeasurementStatuses.HighVoltageOverflow +
                 MeasurementStatuses.LowCurrentOverflow +
                 MeasurementStatuses.LowVoltageOverflow)) <> 0
    End Function

    ''' <summary>
    ''' Returns true if the <paramref name="status">measurement status</paramref> indicates
    ''' that the measurement failed due to time out.
    ''' </summary>
    Public Shared Function IsMeasurementNotLocked(ByVal status As Integer) As Boolean
        Return (status And (MeasurementStatuses.HighNotLocked + MeasurementStatuses.LowNotLocked)) <> 0
    End Function

    ''' <summary>
    ''' Gets the cable length from the specified <see cref="CorrectedCableLength"></see>. 
    ''' </summary>
    ''' <param name="value">Cable length corrected (0, 1.5, or 3 m)</param>
    Public Shared Function ParseCableLength(ByVal value As CorrectedCableLength) As Single

        Dim length As Single = 0
        Select Case value
            Case CorrectedCableLength.DirectConnect
                length = 0
            Case CorrectedCableLength.OnePointFiveMeters
                length = 1.5
            Case CorrectedCableLength.ThreeMeters
                length = 3
        End Select
        Return length

    End Function

    ''' <summary>
    ''' Gets the <see cref="CorrectedCableLength"></see> fromthe cable length
    ''' </summary>
    ''' <param name="value">Cable length corrected (0, 1.5, or 3 m)</param>
    Public Shared Function ParseCableLength(ByVal value As Single) As CorrectedCableLength

        If value < 0.1 Then
            Return CorrectedCableLength.DirectConnect
        ElseIf value < 2 Then
            Return CorrectedCableLength.OnePointFiveMeters
        Else
            Return CorrectedCableLength.ThreeMeters
        End If

    End Function

    ''' <summary>
    ''' Returns the primary value caption for the specified model.
    ''' </summary>
    Public Shared Function PrimaryValueCaption(ByVal model As isr.Kte.ImpedanceModel) As String
        Select Case model
            Case ImpedanceModel.Cartesian
                Return "Impedance"
            Case ImpedanceModel.ParallelConductance
                Return "Capacitance"
            Case ImpedanceModel.ParallelDissipation
                Return "Capacitance"
            Case ImpedanceModel.Polar
                Return "Impedance"
            Case ImpedanceModel.Raw
                Return "Capacitance"
            Case ImpedanceModel.SeriesDissipation
                Return "Capacitance"
            Case ImpedanceModel.SeriesResistance
                Return "Capacitance"
            Case Else
                Return "Not Defined"
        End Select
    End Function

    ''' <summary>
    ''' Returns the secondary value caption for the specified model.
    ''' </summary>
    Public Shared Function SecondaryValueCaption(ByVal model As isr.Kte.ImpedanceModel) As String
        Select Case model
            Case ImpedanceModel.Cartesian
                Return "Reactance"
            Case ImpedanceModel.ParallelConductance
                Return "Conductance"
            Case ImpedanceModel.ParallelDissipation
                Return "Dissipation"
            Case ImpedanceModel.Polar
                Return "Phase"
            Case ImpedanceModel.Raw
                Return "Capacitance"
            Case ImpedanceModel.SeriesDissipation
                Return "Dissipation"
            Case ImpedanceModel.SeriesResistance
                Return "Resistance"
            Case Else
                Return "Not Defined"
        End Select
    End Function

#End Region

#Region " ICOLLECTABLE "

    Private _instrumentId As Integer
    ''' <summary>
    ''' Gets or set the instrument unique ID.
    ''' </summary>
    Public Property InstrumentId() As Integer Implements isr.Kte.IInstrument.UniqueId
        Get
            Return Me._instrumentId
        End Get
        Set(ByVal value As Integer)
            Me._instrumentId = value
        End Set
    End Property

    Private _instrumentFamilyName As String
    ''' <summary>
    ''' Gets or set the instrument family name.
    ''' </summary>
    Public Property InstrumentFamilyName() As String
        Get
            Return Me._instrumentFamilyName
        End Get
        Set(ByVal value As String)
            Me._instrumentFamilyName = value
        End Set
    End Property

    Private _instrumentName As String
    ''' <summary>
    ''' Gets or set the instrument name.
    ''' </summary>
    Public Property InstrumentName() As String Implements isr.Kte.IInstrument.UniqueKey
        Get
            Return Me._instrumentName
        End Get
        Protected Set(ByVal value As String)
            Me._instrumentName = value
        End Set
    End Property

    Private _channelNumber As Integer
    ''' <summary>
    ''' Gets or set the instrument channel number.
    ''' </summary>
    Public Property ChannelNumber() As Integer Implements IInstrument.ChannelNumber
        Get
            Return Me._channelNumber
        End Get
        Protected Set(ByVal value As Integer)
            Me._channelNumber = value
        End Set
    End Property

    Public MustOverride Function SelectChannel() As Boolean Implements IInstrument.SelectChannel

    ''' <summary>
    ''' Returns the instrument Name.
    ''' </summary>
    Public Overrides Function ToString() As String
        Return Me._instrumentName
    End Function

#End Region

#Region " IINDEXABLE "

    Private _index As Integer
    ''' <summary>
    ''' Gets or set the location of the instrument in the collection of instruments.
    ''' </summary>
    Public Property Index() As Integer Implements Core.IIndexable.Index
        Get
            Return Me._index
        End Get
        Set(ByVal value As Integer)
            Me._index = value
        End Set
    End Property

#End Region

#Region " RESETTABLE "

    ''' <summary>
    ''' Resets all parameters to default values.
    ''' </summary>
    Public Overridable Function ResetKnownState() As Boolean Implements IInstrument.Reset
        Me._presettables.Reset()
        Return Me.LastOperationSuccess
    End Function

#End Region

#Region " IINSTRUMENT: IMPEDANCE "

#Region " APERTURE "

    Private _aperture As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the aperture. Readonly for non-custom speed.
    ''' </summary>
    Public Property Aperture() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.Aperture
        Get
            Return Me._aperture
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._aperture = value
        End Set
    End Property
    Public MustOverride Function ApertureGetter() As Boolean Implements IInstrument.ApertureGetter
    Public Overridable Overloads Function ApertureSetter(ByVal value As Single) As Boolean Implements IInstrument.ApertureSetter
        Me.Aperture.CacheValue = value
        If Me.Aperture.HasActualValue AndAlso Me.Aperture.IsActual Then
            Return True
        Else
            Return Me.ApertureSetter()
        End If
    End Function
    Protected MustOverride Overloads Function ApertureSetter() As Boolean Implements IInstrument.ApertureSetter

#End Region

    ''' <summary>
    ''' Gets or sets the corrected cable length.
    ''' </summary>
    Private _cableCorrection As CorrectedCableLength
    Public Property CableCorrection() As CorrectedCableLength Implements IInstrument.CableCorrection
        Get
            Return Me._cableCorrection
        End Get
        Set(ByVal value As CorrectedCableLength)
            Me._cableCorrection = value
            Me._cableLength.CacheValue = BaseInstrument.ParseCableLength(value)
        End Set
    End Property
    Public Function CableCorrectionGetter() As Boolean Implements IInstrument.CableCorrectionGetter
        Return Me.CableLengthSetter
    End Function
    Public Overridable Overloads Function CableCorrectionSetter(ByVal value As CorrectedCableLength) As Boolean Implements IInstrument.CableCorrectionSetter
        Return Me.CableLengthSetter(BaseInstrument.ParseCableLength(value))
    End Function
    Protected Overloads Function CableCorrectionSetter() As Boolean Implements IInstrument.CableCorrectionSetter
        Me.CableLengthSetter()
        Return Me.LastOperationSuccess
    End Function

    Private _cableLength As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the cable length for correction.
    ''' </summary>
    Public Property CableLength() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.CableLength
        Get
            Return Me._cableLength
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._cableLength = value
            If value IsNot Nothing Then
                If value.HasActualValue Then
                    Me._cableCorrection = BaseInstrument.ParseCableLength(value.ActualValue)
                Else
                    Me._cableCorrection = BaseInstrument.ParseCableLength(value.CacheValue)
                End If
            End If
        End Set
    End Property
    Public MustOverride Function CableLengthGetter() As Boolean Implements IInstrument.CableLengthGetter
    Public Overridable Overloads Function CableLengthSetter(ByVal value As Single) As Boolean Implements IInstrument.CableLengthSetter
        Me._cableLength.CacheValue = value
        Me._cableCorrection = BaseInstrument.ParseCableLength(value)
        If Me._cableLength.HasActualValue AndAlso Me._cableLength.IsActual Then
            Return True
        Else
            Return Me.CableLengthSetter()
        End If
    End Function
    Protected MustOverride Overloads Function CableLengthSetter() As Boolean Implements IInstrument.CableLengthSetter

    Private _delayFactor As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the delay factor. Readonly for non-custom speed.
    ''' </summary>
    Public Property DelayFactor() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.DelayFactor
        Get
            Return Me._delayFactor
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._delayFactor = value
        End Set
    End Property
    Public MustOverride Function DelayFactorGetter() As Boolean Implements IInstrument.DelayFactorGetter
    Public Function DelayFactorSetter(ByVal value As Single) As Boolean Implements IInstrument.DelayFactorSetter
        Me._delayFactor.CacheValue = value
        If Me._delayFactor.HasActualValue AndAlso Me._delayFactor.IsActual Then
            Return True
        Else
            Return Me.DelayFactorSetter()
        End If
    End Function
    Protected MustOverride Function DelayFactorSetter() As Boolean Implements IInstrument.DelayFactorSetter

    Private _filterFactor As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the filter factor. Readonly for non-custom speed.
    ''' </summary>
    Public Property FilterFactor() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.FilterFactor
        Get
            Return Me._filterFactor
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._filterFactor = value
        End Set
    End Property
    Public MustOverride Function FilterFactorGetter() As Boolean Implements IInstrument.FilterFactorGetter
    Public Overridable Function FilterFactorSetter(ByVal value As Single) As Boolean Implements IInstrument.FilterFactorSetter
        Me._filterFactor.CacheValue = value
        If Me._filterFactor.HasActualValue AndAlso Me._filterFactor.IsActual Then
            Return True
        Else
            Return Me.FilterFactorSetter()
        End If
    End Function
    Protected MustOverride Function FilterFactorSetter() As Boolean Implements IInstrument.FilterFactorSetter

    Private _holdTime As isr.Core.PresettableFormattableCache(Of Single)
    Public Property HoldTime() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.HoldTime
        Get
            Return Me._holdTime
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._holdTime = value
        End Set
    End Property
    Public MustOverride Function HoldTimeGetter() As Boolean Implements IInstrument.HoldTimeGetter
    Public Overridable Function HoldTimeSetter(ByVal value As Single) As Boolean Implements IInstrument.HoldTimeSetter
        Me._holdTime.CacheValue = value
        If Me._holdTime.HasActualValue AndAlso Me._holdTime.IsActual Then
            Return True
        Else
            Return Me.HoldTimeSetter()
        End If
    End Function
    Protected MustOverride Function HoldTimeSetter() As Boolean Implements IInstrument.HoldTimeSetter

    Private _impedance As isr.Kte.Impedance
    ''' <summary>
    ''' Gets or sets the measured impedance.
    ''' </summary>
    Public Property Impedance() As Impedance Implements IInstrument.Impedance
        Get
            Return Me._impedance
        End Get
        Set(ByVal value As Impedance)
            Me._impedance = value
        End Set
    End Property
    Public MustOverride Function ImpedanceGetter() As Boolean Implements IInstrument.ImpedanceGetter

    Private _loadCompensationEnabled As isr.Core.PresettableCache(Of Boolean)
    ''' <summary>
    ''' Gets or sets the condition enabling the load compensation.
    ''' </summary>
    Public Property LoadCompensationEnabled() As isr.Core.PresettableCache(Of Boolean) Implements IInstrument.LoadCompensationEnabled
        Get
            Return Me._loadCompensationEnabled
        End Get
        Set(ByVal value As isr.Core.PresettableCache(Of Boolean))
            Me._loadCompensationEnabled = value
        End Set
    End Property
    Public MustOverride Function LoadCompensationEnabledGetter() As Boolean Implements IInstrument.LoadCompensationEnabledGetter
    Public Overridable Function LoadCompensationEnabledSetter(ByVal value As Boolean) As Boolean Implements IInstrument.LoadCompensationEnabledSetter
        Me._loadCompensationEnabled.CacheValue = value
        If Me._loadCompensationEnabled.HasActualValue AndAlso Me._loadCompensationEnabled.IsActual Then
            Return True
        Else
            Return Me.LoadCompensationEnabledSetter()
        End If
    End Function
    Protected MustOverride Function LoadCompensationEnabledSetter() As Boolean Implements IInstrument.LoadCompensationEnabledSetter

    Private _measurementModel As isr.Core.PresettableFormattableCache(Of Integer)
    ''' <summary>
    ''' Specifies the impedance measurement model.
    ''' </summary>
    Public Property MeasurementModel() As isr.Core.PresettableFormattableCache(Of Integer) Implements IInstrument.MeasurementModel
        Get
            Return Me._measurementModel
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Integer))
            Me._measurementModel = value
        End Set
    End Property
    Public MustOverride Function MeasurementModelGetter() As Boolean Implements IInstrument.MeasurementModelGetter
    Public Overridable Function MeasurementModelSetter(ByVal value As Integer) As Boolean Implements IInstrument.MeasurementModelSetter
        Me._measurementModel.CacheValue = value
        If Me._measurementModel.HasActualValue AndAlso Me._measurementModel.IsActual Then
            Return True
        Else
            Return Me.MeasurementModelSetter()
        End If
    End Function
    Protected MustOverride Function MeasurementModelSetter() As Boolean Implements IInstrument.MeasurementModelSetter

    Private _measurementSpeed As isr.Core.PresettableFormattableCache(Of Integer)
    ''' <summary>
    ''' Specifies the measurement speed.
    ''' </summary>
    Public Property MeasurementSpeed() As isr.Core.PresettableFormattableCache(Of Integer) Implements IInstrument.MeasurementSpeed
        Get
            Return Me._measurementSpeed
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Integer))
            Me._measurementSpeed = value
        End Set
    End Property
    Public MustOverride Function MeasurementSpeedGetter() As Boolean Implements IInstrument.MeasurementSpeedGetter
    Public Overridable Function MeasurementSpeedSetter(ByVal value As Integer) As Boolean Implements IInstrument.MeasurementSpeedSetter
        Me._measurementSpeed.CacheValue = value
        If Me._measurementSpeed.HasActualValue AndAlso Me._measurementSpeed.IsActual Then
            Return True
        Else
            Return Me.MeasurementSpeedSetter()
        End If
    End Function
    Protected MustOverride Function MeasurementSpeedSetter() As Boolean Implements IInstrument.MeasurementSpeedSetter

    Private _measurementStatus As Integer
    Public Property MeasurementStatus() As Integer Implements IInstrument.MeasurementStatus
        Get
            Return Me._measurementStatus
        End Get
        Set(ByVal value As Integer)
            Me._measurementStatus = value
        End Set
    End Property
    Public MustOverride Function MeasurementStatusGetter() As Boolean Implements IInstrument.MeasurementStatusGetter

    Private _openCompensationEnabled As isr.Core.PresettableCache(Of Boolean)
    ''' <summary>
    ''' Gets or sets the condition enabling open compensation.
    ''' </summary>
    Public Property OpenCompensationEnabled() As isr.Core.PresettableCache(Of Boolean) Implements IInstrument.OpenCompensationEnabled
        Get
            Return Me._openCompensationEnabled
        End Get
        Set(ByVal value As isr.Core.PresettableCache(Of Boolean))
            Me._openCompensationEnabled = value
        End Set
    End Property
    Public MustOverride Function OpenCompensationEnabledGetter() As Boolean Implements IInstrument.OpenCompensationEnabledGetter
    Public Overridable Function OpenCompensationEnabledSetter(ByVal value As Boolean) As Boolean Implements IInstrument.OpenCompensationEnabledSetter
        Me._openCompensationEnabled.CacheValue = value
        If Me._openCompensationEnabled.HasActualValue AndAlso Me._openCompensationEnabled.IsActual Then
            Return True
        Else
            Return Me.OpenCompensationEnabledSetter()
        End If
    End Function
    Protected MustOverride Function OpenCompensationEnabledSetter() As Boolean Implements IInstrument.OpenCompensationEnabledSetter

    Private _samplingBias As isr.Core.PresettableFormattableCache(Of Single)
    Public Property SamplingBias() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.SamplingBias
        Get
            Return Me._samplingBias
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._samplingBias = value
        End Set
    End Property
    Public MustOverride Function SamplingBiasGetter() As Boolean Implements IInstrument.SamplingBiasGetter
    Public Overridable Function SamplingBiasSetter(ByVal value As Single) As Boolean Implements IInstrument.SamplingBiasSetter
        Me._samplingBias.CacheValue = value
        If Me._samplingBias.HasActualValue AndAlso Me._samplingBias.IsActual Then
            Return True
        Else
            Return Me.SamplingBiasSetter()
        End If
    End Function
    Protected MustOverride Function SamplingBiasSetter() As Boolean Implements IInstrument.SamplingBiasSetter

    Private _samplingCount As isr.Core.PresettableFormattableCache(Of Integer)
    Public Property SamplingCount() As isr.Core.PresettableFormattableCache(Of Integer) Implements IInstrument.SamplingCount
        Get
            Return Me._samplingCount
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Integer))
            Me._samplingCount = value
        End Set
    End Property
    Public MustOverride Function SamplingCountGetter() As Boolean Implements IInstrument.SamplingCountGetter
    Public Overridable Function SamplingCountSetter(ByVal value As Integer) As Boolean Implements IInstrument.SamplingCountSetter
        Me._samplingCount.CacheValue = value
        If Me._samplingCount.HasActualValue AndAlso Me._samplingCount.IsActual Then
            Return True
        Else
            Return Me.SamplingCountSetter()
        End If
    End Function
    Protected MustOverride Function SamplingCountSetter() As Boolean Implements IInstrument.SamplingCountSetter

    Private _samplingInterval As isr.Core.PresettableFormattableCache(Of Single)
    Public Property SamplingInterval() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.SamplingInterval
        Get
            Return Me._samplingInterval
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._samplingInterval = value
        End Set
    End Property
    Public MustOverride Function SamplingIntervalGetter() As Boolean Implements IInstrument.SamplingIntervalGetter
    Public Overridable Function SamplingIntervalSetter(ByVal value As Single) As Boolean Implements IInstrument.SamplingIntervalSetter
        Me._samplingInterval.CacheValue = value
        If Me._samplingInterval.HasActualValue AndAlso Me._samplingInterval.IsActual Then
            Return True
        Else
            Return Me.SamplingIntervalSetter()
        End If
    End Function
    Protected MustOverride Function SamplingIntervalSetter() As Boolean Implements IInstrument.SamplingIntervalSetter

    Private _senseCurrentRange As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the AC sense current range. 
    ''' </summary>
    Public Property SenseCurrentRange() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.SenseCurrentRange
        Get
            Return Me._senseCurrentRange
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._senseCurrentRange = value
        End Set
    End Property
    Public MustOverride Function SenseCurrentRangeGetter() As Boolean Implements IInstrument.SenseCurrentRangeGetter
    Public Overridable Function SenseCurrentRangeSetter(ByVal value As Single) As Boolean Implements IInstrument.SenseCurrentRangeSetter
        Me._senseCurrentRange.CacheValue = value
        If Me._senseCurrentRange.HasActualValue AndAlso Me._senseCurrentRange.IsActual Then
            Return True
        Else
            Return Me.SenseCurrentRangeSetter()
        End If
    End Function
    Protected MustOverride Function SenseCurrentRangeSetter() As Boolean Implements IInstrument.SenseCurrentRangeSetter

    Private _settlingTime As Single
    ''' <summary>
    ''' Gets or sets the AC source settling time.
    ''' Used only if measurement follows setup right away.
    ''' </summary>
    Public Property SettlingTime() As Single Implements IInstrument.SettlingTime
        Get
            Return Me._settlingTime
        End Get
        Set(ByVal value As Single)
            Me._settlingTime = value
        End Set
    End Property

    Private _shortCompensationEnabled As isr.Core.PresettableCache(Of Boolean)
    ''' <summary>
    ''' Gets or sets the condition enabling short compensation.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Public Property ShortCompensationEnabled() As isr.Core.PresettableCache(Of Boolean) Implements IInstrument.ShortCompensationEnabled
        Get
            Return Me._shortCompensationEnabled
        End Get
        Set(ByVal value As isr.Core.PresettableCache(Of Boolean))
            Me._shortCompensationEnabled = value
        End Set
    End Property
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Public MustOverride Function ShortCompensationEnabledGetter() As Boolean Implements IInstrument.ShortCompensationEnabledGetter
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Public Overridable Function ShortCompensationEnabledSetter(ByVal value As Boolean) As Boolean Implements IInstrument.ShortCompensationEnabledSetter
        Me._shortCompensationEnabled.CacheValue = value
        If Me._shortCompensationEnabled.HasActualValue AndAlso Me._shortCompensationEnabled.IsActual Then
            Return True
        Else
            Return Me.ShortCompensationEnabledSetter()
        End If
    End Function
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Protected MustOverride Function ShortCompensationEnabledSetter() As Boolean Implements IInstrument.ShortCompensationEnabledSetter

    Private _sourceBias As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the DC source bias level.
    ''' </summary>
    Public Property SourceBias() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.SourceBias
        Get
            Return Me._sourceBias
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._sourceBias = value
        End Set
    End Property
    Public MustOverride Function SourceBiasGetter() As Boolean Implements IInstrument.SourceBiasGetter
    Public Overridable Function SourceBiasSetter(ByVal value As Single) As Boolean Implements IInstrument.SourceBiasSetter
        Me._sourceBias.CacheValue = value
        If Me._sourceBias.HasActualValue AndAlso Me._sourceBias.IsActual Then
            Return True
        Else
            Return Me.SourceBiasSetter()
        End If
    End Function
    Protected MustOverride Function SourceBiasSetter() As Boolean Implements IInstrument.SourceBiasSetter

    Private _sourceFrequency As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the source frequency.
    ''' </summary>
    Public Property SourceFrequency() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.SourceFrequency
        Get
            Return Me._sourceFrequency
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._sourceFrequency = value
        End Set
    End Property
    Public MustOverride Function SourceFrequencyGetter() As Boolean Implements IInstrument.SourceFrequencyGetter
    Public Overridable Function SourceFrequencySetter(ByVal value As Single) As Boolean Implements IInstrument.SourceFrequencySetter
        Me._sourceFrequency.CacheValue = value
        If Me._sourceFrequency.HasActualValue AndAlso Me._sourceFrequency.IsActual Then
            Return True
        Else
            Return Me.SourceFrequencySetter()
        End If
    End Function
    Protected MustOverride Function SourceFrequencySetter() As Boolean Implements IInstrument.SourceFrequencySetter

    Private _sourceLevel As isr.Core.PresettableFormattableCache(Of Single)
    ''' <summary>
    ''' Gets or sets the AC source level. 
    ''' </summary>
    Public Property SourceLevel() As isr.Core.PresettableFormattableCache(Of Single) Implements IInstrument.SourceLevel
        Get
            Return Me._sourceLevel
        End Get
        Set(ByVal value As isr.Core.PresettableFormattableCache(Of Single))
            Me._sourceLevel = value
        End Set
    End Property
    Public MustOverride Function SourceLevelGetter() As Boolean Implements IInstrument.SourceLevelGetter
    Public Overridable Function SourceLevelSetter(ByVal value As Single) As Boolean Implements IInstrument.SourceLevelSetter
        Me._sourceLevel.CacheValue = value
        If Me._sourceLevel.HasActualValue AndAlso Me._sourceLevel.IsActual Then
            Return True
        Else
            Return Me.SourceLevelSetter()
        End If
    End Function
    Protected MustOverride Function SourceLevelSetter() As Boolean Implements IInstrument.SourceLevelSetter

#End Region

#Region " IINSTRUMENT: INSTRUMENT "

    Public ReadOnly Property AllParametersActual() As Boolean Implements IInstrument.AllParametersActual
        Get
            Return Me._presettables.AllActual
        End Get
    End Property

    Public MustOverride Function AwaitTestCompletion(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean Implements IInstrument.AwaitTestCompletion

    Private _presettables As isr.Core.PresettableCachedCollection

    Public Function Apply() As Boolean Implements IInstrument.Apply
        Return Me._presettables.Apply
    End Function

    Private _enabled As Boolean
    ''' <summary>
    ''' Gets or sets the condition indicating if the instrument is enabled. 
    ''' This is a way to use sparse testing. If a test is nop, the instrument is tagged as disabled so its 
    ''' data need not be retrieved even when using parallel measurements. 
    ''' </summary>
    Public Property Enabled() As Boolean Implements IInstrument.Enabled
        Get
            Return Me._enabled
        End Get
        Set(ByVal value As Boolean)
            Me._enabled = value
        End Set
    End Property

    Public MustOverride Function Exists() As Boolean Implements IInstrument.Exists

    ''' <summary>
    ''' Clears tests results and marks the test as not completed.
    ''' </summary>
    Public Overridable Sub InitiateTest() Implements IInstrument.InitiateTest
        Me.Impedance = Impedance.Empty()
        Me._measurementStatus = 0
        Me._lastTestCompleted = False
    End Sub

    Private _lastTestCompleted As Boolean
    Public Function LastTestCompleted() As Boolean Implements IInstrument.LastTestCompleted
        If Not Me._lastTestCompleted Then
            Me._lastTestCompleted = QueryTestCompleted()
        End If
        Return Me._lastTestCompleted
    End Function

    ''' <summary>
    ''' Return the list of names of items which values are not actual.
    ''' </summary>
    Public Function NonActual() As String Implements IInstrument.NonActual
        Dim value As New System.Text.StringBuilder
        Dim names As String = Me._presettables.NonActual
        If Not String.IsNullOrWhiteSpace(names) Then
            If value.Length > 0 Then
                value.Append(",")
            End If
            value.Append(names)
        End If
        Return value.ToString
    End Function

    Public MustOverride Function QueryTestCompleted() As Boolean Implements IInstrument.QueryTestCompleted

    Public Function Refresh() As Boolean Implements IInstrument.Refresh
        Return Me._presettables.Refresh
    End Function

    ''' <summary>
    ''' Measures the impedance and gets the impedance data for a single instrument.
    ''' </summary>
    ''' <param name="timeout">Specifies timeout in MS</param>
    ''' <param name="pollDelay">Specifies the time between polls in MS.</param>
    Public MustOverride Function MeasureImpedance(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean Implements IInstrument.MeasureImpedance

    ''' <summary>
    ''' Gets an indication if the last operation timed out.
    ''' </summary>
    Public Property OperationTimedOut() As Boolean Implements IInstrument.OperationTimedOut

    Public MustOverride Function SelectImpedanceInstrument(ByVal channelNumber As Integer) As Boolean Implements IInstrument.SelectImpedanceInstrument

#End Region

#Region " STATUS "

    ''' <summary>
    ''' Returns true if last operation succeeded.
    ''' This is signified by the <see cref="OperationStatus">last status</see>
    ''' greater than or equal <see cref="OperationStatus.Success">okay</see>
    ''' and <see cref="SocketStatus">socket error</see> equals
    ''' <see cref="Net.Sockets.SocketError.Success">success</see>
    ''' </summary>
    Public ReadOnly Property LastOperationSuccess() As Boolean Implements IInstrument.LastOperationSuccess
        Get
            Return Me._lastStatus >= isr.Kte.OperationStatus.Success AndAlso
                   Me._lastSocketStatus = Net.Sockets.SocketError.Success
        End Get
    End Property

    ''' <summary>
    ''' Returns true if last operation failed.
    ''' This is signified by the <see cref="OperationStatus">last status</see>
    ''' smaller than <see cref="OperationStatus.Success">okay</see> (negative)
    ''' or <see cref="SocketStatus">socket status</see> not 
    ''' equal <see cref="Net.Sockets.SocketError.Success">success</see>
    ''' </summary>
    Public ReadOnly Property LastOperationFailed() As Boolean Implements IInstrument.LastOperationFailed
        Get
            Return Me._lastStatus < isr.Kte.OperationStatus.Success OrElse Me._lastSocketStatus <> Net.Sockets.SocketError.Success
        End Get
    End Property

    Private _lastSocketStatus As Integer
    Public Property SocketStatus() As Integer Implements IInstrument.LastSocketStatus
        Get
            Return Me._lastSocketStatus
        End Get
        Protected Set(ByVal value As Integer)
            Me._lastSocketStatus = value
        End Set
    End Property

    Private _lastStatus As Integer
    Public Property OperationStatus() As Integer Implements IInstrument.LastOperationStatus
        Get
            Return Me._lastStatus
        End Get
        Protected Set(ByVal value As Integer)
            Me._lastStatus = value
        End Set
    End Property

    ''' <summary>
    ''' Returns overall status details
    ''' </summary>
    Public MustOverride Function LastStatusDetails() As String Implements IInstrument.LastStatusDetails

#End Region

#Region " I INSTRUMENT: MESSAGES"

    Private _messageQueue As isr.Core.MessageQueue
    ''' <summary>
    ''' Gets reference to the message queue.
    ''' </summary>
    Public ReadOnly Property MessageQueue() As isr.Core.MessageQueue Implements IInstrument.MessageQueue
        Get
            Return Me._messageQueue
        End Get
    End Property

#End Region

End Class
