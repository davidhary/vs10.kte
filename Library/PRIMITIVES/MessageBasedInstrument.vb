﻿''' <summary>
''' Implements a message-based <see cref="IInstrument">Instrument</see> 
''' using the Keithley Test Environment External Control Interface (KXCI) library.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
''' <history date="12/02/2009" by="David" revision="1.0.3623.x">
''' Do not reset presettable values with the message based instrument forcing the system to reset.
''' </history>
Public Class MessageBasedInstrument

    Inherits BaseInstrument

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " IINSTRUMENT "

    ''' <summary>
    ''' Clears teh last status.
    ''' </summary>
    Private Sub clearLastStatus()
        MyBase.OperationStatus = isr.Kte.OperationStatus.Success
        MyBase.SocketStatus = Net.Sockets.SocketError.Success
    End Sub

    ''' <summary>
    ''' Saves the last operation status.
    ''' </summary>
    Private Sub saveLastStatus()
        MyBase.OperationStatus = Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
    End Sub

    Public Overrides Function AwaitTestCompletion(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean
        Return Kxci.SafeNativeMethods.AwaitCompletion(MyBase.ChannelNumber, timeout, pollDelay)
    End Function

    Public Overrides Function Exists() As Boolean
        ' TO_DO: Implement perhaps using the channel number setter.
        clearLastStatus()
        If MyBase.LastOperationFailed Then
            MyBase.MessageQueue.Enqueue("Failed testing for existance of instrument #{0} using ID = {1}. {2}", MyBase.ChannelNumber, MyBase.InstrumentId, Me.LastStatusDetails())
        End If
        Return True
    End Function

    Public Overrides Function QueryTestCompleted() As Boolean
        Dim completed As Boolean = Kxci.SafeNativeMethods.QueryTestCompleted(MyBase.ChannelNumber)
        saveLastStatus()
        Return completed
    End Function

    ''' <summary>
    ''' Measures impedance in user mode.
    ''' </summary>
    ''' <remarks>Getting the status is not supported in user mode. So
    ''' impedance must be measured in System mode.
    ''' </remarks>
    Public Function MeasureImpedanceUserMode() As Boolean
        ' set user mode
        MyBase.Impedance = New Impedance(isr.Kte.Kxci.SafeNativeMethods.MeasureImpedance())
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not MyBase.Impedance.HasValue OrElse MyBase.LastOperationFailed Then
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.MessageQueue.Enqueue("Instrument {0} failed measuring impedance. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function ImpedanceGetter() As Boolean

        ' get the data from the specified channel
        '  3625.Me.SelectChannel()
        saveLastStatus()
        If MyBase.LastOperationSuccess Then

            ' read the impedance.
            Dim value As String = Kxci.SafeNativeMethods.MeasuredImpedanceGetter("0;0,")
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                MyBase.Impedance = New Impedance(Kxci.SafeNativeMethods.ParseImpedance(value))
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed parsing impedance value '{1}'.", MyBase.InstrumentName, value)
                    Return MyBase.LastOperationSuccess
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed fetching measured impedance.", MyBase.InstrumentName)
                Return MyBase.LastOperationSuccess
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed selecting instrument number {1}.", MyBase.InstrumentName, MyBase.ChannelNumber)
            Return MyBase.LastOperationSuccess
        End If
        Return MyBase.LastOperationSuccess

    End Function

    ''' <summary>
    ''' Measure the impedance with a more detailed status reporting,
    ''' </summary>
    ''' <remarks>
    ''' This requires enabling <see cref="isr.Kte.Kxci.SafeNativeMethods.SystemModeEnabledSetter">system mode</see>.
    ''' Also requires setting sample or sweep modes.
    ''' Also requires selecting the channel. 
    ''' </remarks>
    Public Overrides Function MeasureImpedance(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean

        clearLastStatus()

        If Kxci.SafeNativeMethods.UsingDevices Then

            ' acticate the channel.
            Me.SelectChannel()

            ' issue test start.
            Dim result As New Kxci.QueryResultInfo(Kxci.SafeNativeMethods.SendReceive(":CVU:TEST:RUN"))
            saveLastStatus()

            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Instrument {0} failed starting test.", MyBase.InstrumentName)
                Return MyBase.LastOperationSuccess
            End If

            ' wait a couple of ms before testing for completion
            Threading.Thread.Sleep(pollDelay)

            ' await for operation to timeout or complete.
            Kxci.SafeNativeMethods.AwaitCompletion(MyBase.ChannelNumber, timeout, pollDelay)
            saveLastStatus()

            If MyBase.LastOperationFailed Then
                If Kxci.SafeNativeMethods.OperationTimedOut Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} timed out waiting for test completion.", MyBase.InstrumentName)
                Else
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed waiting for test completion.", MyBase.InstrumentName)
                End If
                Return MyBase.LastOperationSuccess
            End If

            ' get the data from the specified channel
            '  3625.Me.SelectChannel()
            saveLastStatus()
            If MyBase.LastOperationSuccess Then

                ' read the impedance.
                Dim value As String = Kxci.SafeNativeMethods.MeasuredImpedanceGetter("0;0,")
                saveLastStatus()
                If MyBase.LastOperationSuccess Then
                    MyBase.Impedance = New Impedance(Kxci.SafeNativeMethods.ParseImpedance(value))
                    If MyBase.LastOperationFailed Then
                        MyBase.MessageQueue.Enqueue("Instrument {0} failed parsing impedance value '{1}'.", MyBase.InstrumentName, value)
                        Return MyBase.LastOperationSuccess
                    End If
                Else
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed fetching measured impedance.", MyBase.InstrumentName)
                    Return MyBase.LastOperationSuccess
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed selecting instrument number {1}.", MyBase.InstrumentName, MyBase.ChannelNumber)
                Return MyBase.LastOperationSuccess
            End If
        ElseIf Kxci.SafeNativeMethods.OperationTimedOut Then
            MyBase.MessageQueue.Enqueue("Instrument {0} timed out measuring impedance. {1}.", MyBase.InstrumentName, Me.LastStatusDetails())
        Else
            Dim value As String = Kxci.SafeNativeMethods.MeasuredImpedanceGetter("0.00001;0.000001,")
            MyBase.Impedance = New Impedance(Kxci.SafeNativeMethods.ParseImpedance(value))
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Instrument {0} failed parsing impedance value '{1}'.", MyBase.InstrumentName, value)
                Return MyBase.LastOperationSuccess
            End If
        End If

        Return MyBase.LastOperationSuccess

    End Function

    ''' <summary>
    ''' Resets messaged based device to an unknown state because the reset
    ''' conditions are ill-defined for KXCI.
    ''' </summary>
    Public Overrides Function ResetKnownState() As Boolean
        Return isr.Kte.Kxci.SafeNativeMethods.ResetKnownState() And MyBase.ResetKnownState()
    End Function

    Public Overrides Function SelectChannel() As Boolean
        Return Kxci.SafeNativeMethods.ChannelNumberSetter(MyBase.ChannelNumber)
    End Function

    Public Overrides Function SelectImpedanceInstrument(ByVal channelNumber As Integer) As Boolean
        MyBase.InstrumentFamilyName = "CVU"
        MyBase.ChannelNumber = channelNumber
        MyBase.InstrumentName = BaseInstrument.BuildImpedanceInstrumentName(channelNumber)
        Dim id As Integer? = channelNumber ' isr.Kte.kxci.SafeNativeMethods.InstrumentIdGetter(MyBase.InstrumentName)
        If id.HasValue Then
            MyBase.InstrumentId = id.Value
            ' check if instrument exists.
            If Me.Exists() Then
                Me.SelectChannel()
                saveLastStatus()
                If MyBase.LastOperationSuccess Then
                    MyBase.ChannelNumber = Kxci.SafeNativeMethods.ChannelNumberGetter(channelNumber)
                    If MyBase.ChannelNumber <> channelNumber Then
                        MyBase.MessageQueue.Enqueue("Failed selecting instrument channel '{0}'. Station returned channel {1}", channelNumber, MyBase.ChannelNumber)
                    Else
                        ' default to system mode.
                        isr.Kte.Kxci.SafeNativeMethods.SystemModeEnabledSetter(True)
                        If MyBase.LastOperationFailed Then
                            MyBase.MessageQueue.Enqueue("Failed enabling system mode. {0}", Me.LastStatusDetails())
                        End If
                    End If
                Else
                    MyBase.MessageQueue.Enqueue("Failed selecting instrument channel '{0}'.", channelNumber)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Failed locating instrument '{0}'", MyBase.InstrumentName)
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                Return MyBase.LastOperationSuccess
            End If
        Else
            MyBase.MessageQueue.Enqueue("Failed getting instrument id for instrument name {0}. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            Return MyBase.LastOperationSuccess
        End If
        MyBase.InstrumentName = BaseInstrument.BuildImpedanceInstrumentName(MyBase.ChannelNumber)
        Return MyBase.LastOperationSuccess
    End Function

#End Region

#Region " IINSTRUMENT STATUS "

    ''' <summary>
    ''' Returns overall status details
    ''' </summary>
    Public Overrides Function LastStatusDetails() As String
        Return isr.Kte.Kxci.SafeNativeMethods.LastStatusDetails()
    End Function

#End Region

#Region " IINSTRUMENT: IMPEDANCE "

    Public Overrides Function ApertureGetter() As Boolean
        ' get the current compensation factors.
        Dim speed As String = ""
        speed = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, MyBase.FilterFactor.CacheValue, MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not String.IsNullOrWhiteSpace(speed) AndAlso MyBase.LastOperationSuccess Then
            MyBase.Aperture.ActualValue = isr.Kte.Kxci.SafeNativeMethods.Aperture(speed)
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.Aperture.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting speed from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function ApertureSetter() As Boolean
        Dim value As Single = MyBase.Aperture.CacheValue
        MyBase.Aperture.HasActualValue = False
        ' get the current compensation factors.
        Dim speed As String = ""
        speed = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, MyBase.FilterFactor.CacheValue, MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If MyBase.LastOperationSuccess Then
            speed = isr.Kte.Kxci.SafeNativeMethods.UpdateAperture(speed, value)
            isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedSetter(speed)
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                ' update all speed related values
                Me.MeasurementSpeedGetter()
                Me.DelayFactorGetter()
                Me.FilterFactorGetter()
                Me.ApertureGetter()
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed getting aperture after setting it to {1}", MyBase.InstrumentName, value)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed setting aperture to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed getting speed. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function CableLengthGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.CableLengthGetter(MyBase.CableLength.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.CableLength.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.CableLength.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting cable length correction from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting cable length correction from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function CableLengthSetter() As Boolean
        Dim value As Single = MyBase.CableLength.CacheValue
        MyBase.CableLength.HasActualValue = False
        Kxci.SafeNativeMethods.CableLengthSetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.CableLengthGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting cable length to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function DelayFactorGetter() As Boolean
        ' get the current compensation factors.
        Dim speed As String = ""
        speed = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, MyBase.FilterFactor.CacheValue, MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not String.IsNullOrWhiteSpace(speed) AndAlso MyBase.LastOperationSuccess Then
            MyBase.DelayFactor.ActualValue = isr.Kte.Kxci.SafeNativeMethods.DelayFactor(speed)
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.DelayFactor.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting speed from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function DelayFactorSetter() As Boolean
        Dim value As Single = MyBase.DelayFactor.CacheValue
        MyBase.DelayFactor.HasActualValue = False
        ' get the current compensation factors.
        Dim speed As String = ""
        speed = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, MyBase.FilterFactor.CacheValue, MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If MyBase.LastOperationSuccess Then
            speed = isr.Kte.Kxci.SafeNativeMethods.UpdateDelayFactor(speed, value)
            Kxci.SafeNativeMethods.MeasurementSpeedSetter(speed)
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                ' update all speed related values
                Me.MeasurementSpeedGetter()
                Me.DelayFactorGetter()
                Me.FilterFactorGetter()
                Me.ApertureGetter()
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed getting Delay Factor after setting it to {1}", MyBase.InstrumentName, value)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Delay Factor to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed getting speed. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function FilterFactorGetter() As Boolean
        ' get the current compensation factors.
        Dim speed As String = ""
        speed = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, MyBase.FilterFactor.CacheValue, MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not String.IsNullOrWhiteSpace(speed) AndAlso MyBase.LastOperationSuccess Then
            MyBase.FilterFactor.ActualValue = isr.Kte.Kxci.SafeNativeMethods.FilterFactor(speed)
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.FilterFactor.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting speed from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function FilterFactorSetter() As Boolean
        Dim value As Single = MyBase.FilterFactor.CacheValue
        MyBase.FilterFactor.HasActualValue = False
        ' get the current compensation factors.
        Dim speed As String = ""
        speed = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, MyBase.FilterFactor.CacheValue, MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If MyBase.LastOperationSuccess Then
            speed = isr.Kte.Kxci.SafeNativeMethods.UpdateFilterFactor(speed, value)
            Kxci.SafeNativeMethods.MeasurementSpeedSetter(speed)
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                ' update all speed related values
                Me.MeasurementSpeedGetter()
                Me.DelayFactorGetter()
                Me.FilterFactorGetter()
                Me.ApertureGetter()
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed getting Filter Factor after setting it to {1}", MyBase.InstrumentName, value)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Filter Factor to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed getting speed. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function HoldTimeGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.HoldTimeGetter(MyBase.HoldTime.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.HoldTime.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.HoldTime.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting hold time from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting hold time from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function HoldTimeSetter() As Boolean
        Dim value As Single = MyBase.HoldTime.CacheValue
        MyBase.HoldTime.HasActualValue = False
        Kxci.SafeNativeMethods.HoldTimeSetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.HoldTimeGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting hold time to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function LoadCompensationEnabledGetter() As Boolean
        ' get the current compensation factors.
        Dim compensation As String = isr.Kte.Kxci.SafeNativeMethods.CompensationGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateCompensation(MyBase.LoadCompensationEnabled.CacheValue, MyBase.OpenCompensationEnabled.CacheValue, MyBase.ShortCompensationEnabled.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not String.IsNullOrWhiteSpace(compensation) AndAlso MyBase.LastOperationSuccess Then
            MyBase.LoadCompensationEnabled.ActualValue = isr.Kte.Kxci.SafeNativeMethods.LoadCompensationEnabled(compensation)
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.LoadCompensationEnabled.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting load compensation condition from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function LoadCompensationEnabledSetter() As Boolean
        Dim value As Boolean = MyBase.LoadCompensationEnabled.CacheValue
        MyBase.LoadCompensationEnabled.HasActualValue = False
        ' get the current compensation factors.
        Dim compensation As String = isr.Kte.Kxci.SafeNativeMethods.CompensationGetter( 
              isr.Kte.Kxci.SafeNativeMethods.UpdateCompensation(MyBase.LoadCompensationEnabled.CacheValue, 
                                                                MyBase.OpenCompensationEnabled.CacheValue, 
                                                                MyBase.ShortCompensationEnabled.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If MyBase.LastOperationSuccess Then
            compensation = isr.Kte.Kxci.SafeNativeMethods.UpdateLoadCompensation(compensation, value)
            Kxci.SafeNativeMethods.CompensationSetter(compensation)
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                ' update all compensation values
                Me.LoadCompensationEnabledGetter()
                Me.OpenCompensationEnabledGetter()
                Me.ShortCompensationEnabledGetter()
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed getting load compensation after setting it to {1}", MyBase.InstrumentName, value)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed setting load compensation to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed getting compensation. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function MeasurementModelGetter() As Boolean
        Try
            Dim value As Integer? = isr.Kte.Kxci.SafeNativeMethods.MeasurementModelGetter(MyBase.MeasurementModel.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.MeasurementModel.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.MeasurementModel.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting Measurement Model from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting Measurement Model from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function MeasurementModelSetter() As Boolean
        Dim value As Integer = MyBase.MeasurementModel.CacheValue
        MyBase.MeasurementModel.HasActualValue = False
        Kxci.SafeNativeMethods.MeasurementModelSetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.MeasurementModelGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Measurement Model to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function MeasurementSpeedGetter() As Boolean
        ' get the current compensation factors.
        Dim speed As String = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, MyBase.FilterFactor.CacheValue, MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not String.IsNullOrWhiteSpace(speed) AndAlso MyBase.LastOperationSuccess Then
            MyBase.MeasurementSpeed.ActualValue = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeed(speed)
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.MeasurementSpeed.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting speed from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function MeasurementSpeedSetter() As Boolean
        Dim value As Integer = MyBase.MeasurementSpeed.CacheValue
        MyBase.MeasurementSpeed.HasActualValue = False
        ' get the current compensation factors.
        Dim speed As String = isr.Kte.Kxci.SafeNativeMethods.MeasurementSpeedGetter( 
                isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(MyBase.MeasurementSpeed.CacheValue, MyBase.DelayFactor.CacheValue, 
                                                                      MyBase.FilterFactor.CacheValue, 
                                                                      MyBase.Aperture.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If MyBase.LastOperationSuccess Then
            speed = isr.Kte.Kxci.SafeNativeMethods.UpdateMeasurementSpeed(speed, value)
            Kxci.SafeNativeMethods.MeasurementSpeedSetter(speed)
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                ' update all speed related values
                Me.MeasurementSpeedGetter()
                Me.DelayFactorGetter()
                Me.FilterFactorGetter()
                Me.ApertureGetter()
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed getting Measruement Speed after setting it to {1}", MyBase.InstrumentName, value)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Measruement Speed to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed getting speed. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function MeasurementStatusGetter() As Boolean
        Try
            Dim value As Integer? = isr.Kte.Kxci.SafeNativeMethods.MeasurementStatusGetter(MyBase.MeasurementStatus)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.MeasurementStatus = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                ' MyBase.SourceBias.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting measurement status from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting measurement status from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function OpenCompensationEnabledGetter() As Boolean
        ' get the current compensation factors.
        Dim compensation As String = isr.Kte.Kxci.SafeNativeMethods.CompensationGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateCompensation(MyBase.LoadCompensationEnabled.CacheValue, MyBase.OpenCompensationEnabled.CacheValue, MyBase.ShortCompensationEnabled.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not String.IsNullOrWhiteSpace(compensation) AndAlso MyBase.LastOperationSuccess Then
            MyBase.OpenCompensationEnabled.ActualValue = isr.Kte.Kxci.SafeNativeMethods.OpenCompensationEnabled(compensation)
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.OpenCompensationEnabled.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting Open compensation condition from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function OpenCompensationEnabledSetter() As Boolean
        Dim value As Boolean = MyBase.OpenCompensationEnabled.CacheValue
        MyBase.OpenCompensationEnabled.HasActualValue = False
        ' get the current compensation factors.
        Dim compensation As String = isr.Kte.Kxci.SafeNativeMethods.CompensationGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateCompensation(MyBase.LoadCompensationEnabled.CacheValue, MyBase.OpenCompensationEnabled.CacheValue, MyBase.ShortCompensationEnabled.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If MyBase.LastOperationSuccess Then
            compensation = isr.Kte.Kxci.SafeNativeMethods.UpdateOpenCompensation(compensation, value)
            Kxci.SafeNativeMethods.CompensationSetter(compensation)
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                ' update all compensation values
                Me.LoadCompensationEnabledGetter()
                Me.OpenCompensationEnabledGetter()
                Me.ShortCompensationEnabledGetter()
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed getting Open compensation after setting it to {1}", MyBase.InstrumentName, value)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Open compensation to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed getting compensation. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SamplingBiasGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.SamplingBiasGetter(MyBase.SamplingBias.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.SamplingBias.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.SamplingBias.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting sample bias from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting saample bias from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SamplingBiasSetter() As Boolean
        Dim value As Single = MyBase.SamplingBias.CacheValue
        MyBase.SamplingBias.HasActualValue = False
        Dim count As Integer = MyBase.SamplingCount.CacheValue
        If MyBase.SamplingCount.HasActualValue Then
            count = MyBase.SamplingCount.ActualValue
        End If
        Kxci.SafeNativeMethods.SamplingSetter(value, count)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.SamplingBiasGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting sampling bias to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SamplingCountGetter() As Boolean
        Try
            Dim value As Integer? = isr.Kte.Kxci.SafeNativeMethods.SamplingCountGetter(MyBase.SamplingCount.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.SamplingCount.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.SamplingCount.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting sampling count from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting sampling count from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SamplingCountSetter() As Boolean
        Dim value As Integer = MyBase.SamplingCount.CacheValue
        MyBase.SamplingCount.HasActualValue = False
        Dim bias As Single = MyBase.SamplingBias.CacheValue
        If MyBase.SamplingBias.HasActualValue Then
            bias = MyBase.SamplingBias.ActualValue
        End If
        Kxci.SafeNativeMethods.SamplingSetter(bias, value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.SamplingCountGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting sampling bias to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SamplingIntervalGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.SamplingIntervalGetter(MyBase.SamplingInterval.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.SamplingInterval.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.SamplingInterval.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting sampling interval from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting sampling interval from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SamplingIntervalSetter() As Boolean
        Dim value As Single = MyBase.SamplingInterval.CacheValue
        MyBase.SamplingInterval.HasActualValue = False
        Kxci.SafeNativeMethods.SamplingIntervalSetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.SamplingIntervalGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting sampling interval to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SenseCurrentRangeGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.SenseCurrentRangeGetter(MyBase.SenseCurrentRange.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.SenseCurrentRange.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.SenseCurrentRange.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting Sense Current Range from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting Sense Current Range from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SenseCurrentRangeSetter() As Boolean
        Dim value As Single = MyBase.SenseCurrentRange.CacheValue
        MyBase.SenseCurrentRange.HasActualValue = False
        Kxci.SafeNativeMethods.SenseCurrentRangeSetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.SenseCurrentRangeGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Sense Current Range to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")> 
    Public Overrides Function ShortCompensationEnabledGetter() As Boolean
        ' get the current compensation factors.
        Dim compensation As String = isr.Kte.Kxci.SafeNativeMethods.CompensationGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateCompensation(MyBase.LoadCompensationEnabled.CacheValue, MyBase.OpenCompensationEnabled.CacheValue, MyBase.ShortCompensationEnabled.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If Not String.IsNullOrWhiteSpace(compensation) AndAlso MyBase.LastOperationSuccess Then
            MyBase.ShortCompensationEnabled.ActualValue = isr.Kte.Kxci.SafeNativeMethods.ShortCompensationEnabled(compensation)
        Else
            If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            End If
            MyBase.ShortCompensationEnabled.HasActualValue = False
            MyBase.MessageQueue.Enqueue("Failed getting Short compensation condition from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")> 
    Protected Overrides Function ShortCompensationEnabledSetter() As Boolean
        Dim value As Boolean = MyBase.ShortCompensationEnabled.CacheValue
        MyBase.ShortCompensationEnabled.HasActualValue = False
        ' get the current compensation factors.
        Dim compensation As String = isr.Kte.Kxci.SafeNativeMethods.CompensationGetter(isr.Kte.Kxci.SafeNativeMethods.UpdateCompensation(MyBase.LoadCompensationEnabled.CacheValue, MyBase.OpenCompensationEnabled.CacheValue, MyBase.ShortCompensationEnabled.CacheValue))
        MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
        MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
        If MyBase.LastOperationSuccess Then
            compensation = isr.Kte.Kxci.SafeNativeMethods.UpdateShortCompensation(compensation, value)
            Kxci.SafeNativeMethods.CompensationSetter(compensation)
            saveLastStatus()
            If MyBase.LastOperationSuccess Then
                ' update all compensation values
                Me.LoadCompensationEnabledGetter()
                Me.OpenCompensationEnabledGetter()
                Me.ShortCompensationEnabledGetter()
                If MyBase.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Instrument {0} failed getting Short compensation after setting it to {1}", MyBase.InstrumentName, value)
                End If
            Else
                MyBase.MessageQueue.Enqueue("Instrument {0} failed setting Short compensation to {1}", MyBase.InstrumentName, value)
            End If
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed getting compensation. {1}", MyBase.InstrumentName, Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SourceBiasGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.DirectVoltageGetter(MyBase.SourceBias.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.SourceBias.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.SourceBias.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting source bias from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting source bias from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SourceBiasSetter() As Boolean
        Dim value As Single = MyBase.SourceBias.CacheValue
        MyBase.SourceBias.HasActualValue = False
        Kxci.SafeNativeMethods.DirectVoltageSetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.SourceBiasGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting source bias to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SourceFrequencyGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.SourceFrequencyGetter(MyBase.SourceFrequency.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.SourceFrequency.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.SourceFrequency.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting source Frequency from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting source Frequency from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SourceFrequencySetter() As Boolean
        Dim value As Single = MyBase.SourceFrequency.CacheValue
        MyBase.SourceFrequency.HasActualValue = False
        Kxci.SafeNativeMethods.SourceFrequencySetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.SourceFrequencyGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting source Frequency to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function SourceLevelGetter() As Boolean
        Try
            Dim value As Single? = isr.Kte.Kxci.SafeNativeMethods.AlternatingVoltageGetter(MyBase.SourceLevel.CacheValue)
            MyBase.OperationStatus = isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus
            MyBase.SocketStatus = Kxci.SafeNativeMethods.LastSocketStatus
            If value.HasValue AndAlso MyBase.LastOperationSuccess Then
                MyBase.SourceLevel.ActualValue = value.Value
            Else
                If MyBase.OperationStatus = isr.Kte.OperationStatus.Success Then
                    MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                End If
                MyBase.SourceLevel.HasActualValue = False
                MyBase.MessageQueue.Enqueue("Failed getting alternating voltage Level from instrument {0}; {1}", MyBase.InstrumentName, Me.LastStatusDetails())
            End If
        Catch ex As FunctionCallException
            MyBase.OperationStatus = isr.Kte.OperationStatus.ErrorAvailable
            MyBase.MessageQueue.Enqueue("Exception occurred getting alternating voltage from instrument {0}; Details: {1}", MyBase.InstrumentName, ex.Message)
        End Try
        Return MyBase.LastOperationSuccess
    End Function

    Protected Overrides Function SourceLevelSetter() As Boolean
        Dim value As Single = MyBase.SourceLevel.CacheValue
        MyBase.SourceLevel.HasActualValue = False
        Kxci.SafeNativeMethods.AlternatingVoltageSetter(value)
        saveLastStatus()
        If MyBase.LastOperationSuccess Then
            Me.SourceLevelGetter()
        Else
            MyBase.MessageQueue.Enqueue("Instrument {0} failed setting alternating voltage level to {1}", MyBase.InstrumentName, value)
        End If
        Return MyBase.LastOperationSuccess
    End Function

#End Region

End Class
