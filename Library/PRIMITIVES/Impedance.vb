﻿''' <summary>
''' Holds the primary and secondary components of the impedance measurement.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/30/2009" by="David" revision="1.0.3376.x">
''' Created
''' </history>
Public Structure Impedance

    Private _primaryReading As String
    ''' <summary>
    ''' Gets or sets the primary reading.
    ''' </summary>
    Public Property PrimaryReading() As String
        Get
            Return Me._primaryReading
        End Get
        Set(ByVal value As String)
            Me._primaryReading = value
            Me._primaryValue = CDbl(value)
        End Set
    End Property

    Private _secondaryReading As String
    ''' <summary>
    ''' Gets or sets the secondary balue
    ''' </summary>
    Public Property SecondaryReading() As String
        Get
            Return Me._secondaryReading
        End Get
        Set(ByVal value As String)
            Me._secondaryReading = value
            Me._secondaryValue = CDbl(value)
        End Set
    End Property

    Private _primaryValue As Double
    ''' <summary>
    ''' Gets or sets the primary value.
    ''' </summary>
    Public Property PrimaryValue() As Double
        Get
            Return Me._primaryValue
        End Get
        Set(ByVal value As Double)
            Me._primaryValue = value
            Me._primaryReading = value.ToString(Globalization.CultureInfo.CurrentCulture)
        End Set
    End Property

    Private _secondaryValue As Double
    ''' <summary>
    ''' Gets or sets the secondary balue
    ''' </summary>
    Public Property SecondaryValue() As Double
        Get
            Return Me._secondaryValue
        End Get
        Set(ByVal value As Double)
            Me._secondaryValue = value
            Me._secondaryReading = value.ToString(Globalization.CultureInfo.CurrentCulture)
        End Set
    End Property

    Private _hasValue As Boolean
    ''' <summary>
    ''' Gets the condition indicating if the impedance has values.
    ''' </summary>
    Public ReadOnly Property HasValue() As Boolean
        Get
            Return Me._hasValue
        End Get
    End Property

    ''' <summary>
    ''' Returns an empty impedance value.
    ''' </summary>
    Public Shared Function Empty() As Impedance
        Return New Impedance("", "")
    End Function

    ''' <summary>
    ''' Constructs a new impedance.
    ''' </summary>
    ''' <param name="primary"></param>
    ''' <param name="secondary"></param>
    Public Sub New(ByVal primary As Double, ByVal secondary As Double)
        Me._hasValue = True
        Me.PrimaryValue = primary
        Me.SecondaryValue = secondary
    End Sub

    ''' <summary>
    ''' Constructs a new impedance.
    ''' </summary>
    Public Sub New(ByVal value As Impedance)
        Me._hasValue = value.HasValue
        Me.PrimaryValue = value.PrimaryValue
        Me.SecondaryValue = value.SecondaryValue
    End Sub

    ''' <summary>
    ''' Constructs a new impedance.
    ''' </summary>
    ''' <param name="primary"></param>
    ''' <param name="secondary"></param>
    Public Sub New(ByVal primary As String, ByVal secondary As String)
        If String.IsNullOrWhiteSpace(primary) OrElse String.IsNullOrWhiteSpace(secondary) Then
            Me._hasValue = False
        Else
            Me._hasValue = True
            Me.PrimaryReading = primary
            Me.SecondaryReading = secondary
        End If
    End Sub

#Region " EQUALS "

    ''' <summary>Returns True if equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> both X and Y values are the same.</returns>
    Public Shared Operator =(ByVal left As Impedance, ByVal right As Impedance) As Boolean
        Return Impedance.Equals(left, right)
    End Operator

    ''' <summary>Returns True if not equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> both X and Y values are the same.</returns>
    Public Shared Operator <>(ByVal left As Impedance, ByVal right As Impedance) As Boolean
        Return Not Impedance.Equals(left, right)
    End Operator

    ''' <summary>Returns True if equal.</summary>
    ''' <param name="left">The left hand side item to compare for equality</param>
    ''' <param name="right">The left hand side item to compare for equality</param>
    ''' <remarks>The two Coordinate Scales are the same if the have the same 
    ''' <see cref="Type">type</see>.</remarks>
    Public Overloads Shared Function Equals(ByVal left As Impedance, ByVal right As Impedance) As Boolean
        Return left._primaryValue.Equals(right._primaryValue) AndAlso
                left._secondaryValue.Equals(right._secondaryValue)
    End Function

    ''' <summary>Returns True if the value of the <paramref name="obj" /> equals to the
    '''   instance value.</summary>
    ''' <param name="obj">The object to compare for equality with this instance.
    '''   This object should be type <see cref="Impedance"/>.</param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> is the same value as this
    '''   instance; otherwise, <c>False</c></returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        If obj IsNot Nothing AndAlso TypeOf obj Is Impedance Then
            Return Equals(CType(obj, Impedance))
        Else
            Return False
        End If

    End Function

    ''' <summary>Returns True if the value of the <param>compared</param> equals to the
    '''   instance value.</summary>
    ''' <param name="compared">The <see cref="Impedance">Coordinate Scale</see> to compare for 
    ''' equality with this instance.</param>
    ''' <returns>A Boolean data type</returns>
    ''' <remarks>Coordinate Scales are the same if the have the same 
    ''' <see cref="Type">type</see>.</remarks>
    Public Overloads Function Equals(ByVal compared As Impedance) As Boolean
        Return Impedance.Equals(Me, compared)
    End Function

#End Region

    ''' <summary>Returns the hash code for this <see cref="Impedance"/> structure.  
    '''   In this case, the hash code is simply the equivalent hash code for the 
    '''   <see cref="Impedance"/> <see cref="Type"/>.</summary>
    ''' <returns>An <see cref="System.integer">integer</see> value</returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me._primaryValue.GetHashCode() Xor Me._secondaryValue.GetHashCode()
    End Function

    ''' <summary>Returns the impedance in the specified format.</summary>
    Public Function Caption(ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format, Me._primaryValue, Me._secondaryValue)
    End Function

End Structure

