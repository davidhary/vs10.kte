﻿''' <summary>
''' An interface for controlling a Keithley Test Environment (4200) test station.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
Public Interface ITestStation

    Inherits IDisposable, isr.Core.IResettable

    ''' <summary>
    ''' Returns the number of CVU channels.
    ''' </summary>
    ReadOnly Property ChannelCount() As Integer

    ''' <summary>
    ''' Initializes the test system.
    ''' </summary>
    ''' <returns>True if success</returns>
    Function InitializeTestStation() As Boolean

    ''' <summary>
    ''' Start a parallel Test.
    ''' </summary>
    ''' <returns>True if success</returns>
    Function InitiateParallelTest() As Boolean

    ''' <summary>
    ''' Returns true if last operation was okay.
    ''' </summary>
    ReadOnly Property LastOperationSuccess() As Boolean

    ''' <summary>
    ''' Returns true if last operation failed.
    ''' </summary>
    ReadOnly Property LastOperationFailed() As Boolean

    ''' <summary>
    ''' Gets the last status of socket communications. 
    ''' Applies only to the message based instrument
    ''' </summary>
    ReadOnly Property LastSocketStatus() As Integer

    ''' <summary>
    ''' Gets the last operation status.
    ''' </summary>
    Property LastOperationStatus() As Integer

    ''' <summary>
    ''' Gets the last status.
    ''' </summary>
    Function LastStatusDetails() As String

    ''' <summary>
    ''' Gets the last status byte.
    ''' </summary>
    Property LastStatusByte() As Integer

    ''' <summary>
    ''' Gets or sets the condition for enabling parallel mode.
    ''' </summary>
    Property ParallelImpedanceEnabled() As Boolean

    ''' <summary>
    ''' Relinquishes control of the currently selected test station. 
    ''' </summary>
    ''' <returns>True if success</returns>
    Function CloseTestStation() As Boolean

    ''' <summary>
    ''' Gets the open condition of the test station.
    ''' </summary>
    ReadOnly Property IsOpen() As Boolean

    ''' <summary>
    ''' Opens the test system.
    ''' </summary>
    ''' <param name="timeout">Specifies the timeout for opening the station. This allow shte KXCI service 
    ''' time to open.</param>
    Function OpenTestStation(ByVal timeout As Integer) As Boolean

    ''' <summary>
    ''' Returns the Status Byte. 
    ''' </summary>
    Function StatusByteGetter() As Integer

    ''' <summary>
    ''' Returns True if Parallel CVU is support..
    ''' </summary>
    ReadOnly Property SupportsParallel() As Boolean

#Region " INSTRUMENTS "

    ''' <summary>
    ''' Returns reference to the active instrument.
    ''' </summary>
    ReadOnly Property ActiveInstrument() As isr.Kte.IInstrument

    ''' <summary>
    ''' Retursn reference to the <see cref="isr.Kte.InstrumentCollection">collection of instruments.</see>
    ''' </summary>
    Function Instruments() As isr.Kte.InstrumentCollection

    ''' <summary>
    ''' Adds a new instrument or selects an existing instrument.
    ''' </summary>
    ''' <param name="family">Specifies the instrument family name, e.g., CVU.</param>
    ''' <param name="number">Specifies the instrument number within the family.</param>
    ''' <param name="forceMany">Specifies the condition for forcing creating
    ''' more that one instrument on a system that has a single instrument. This is does for testing.</param>
    ''' <returns>True if success or False if failure.</returns>
    Function AddInstrument(ByVal family As String, ByVal number As Integer, ByVal forceMany As Boolean) As Boolean

#End Region

#Region " MESSAGES "

    ''' <summary>
    ''' Gets reference to the message queue.
    ''' </summary>
    ReadOnly Property MessageQueue() As isr.Core.MessageQueue

#End Region

End Interface
