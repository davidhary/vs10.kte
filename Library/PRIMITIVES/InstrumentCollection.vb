﻿''' <summary>
''' Implements a collection of <see cref="IInstrument">instruments</see>.
''' </summary>
''' <history>
''' Created
''' </history>
Public Class InstrumentCollection
    Inherits isr.Core.IndexableCollection(Of IInstrument)
    Implements isr.Core.IResettable

    Public Sub New(ByVal station As ITestStation)
        MyBase.new()
        Me._station = station
    End Sub

    Private _activeInstrument As IInstrument
    ''' <summary>
    ''' Gets reference to the active <see cref="IInstrument">instrument</see>.
    ''' </summary>
    Public ReadOnly Property ActiveInstrument() As IInstrument
        Get
            Return Me._activeInstrument
        End Get
    End Property

    Private _station As ITestStation

    ''' <summary>
    ''' Adds an <see cref="IInstrument">instrument</see> to the collection and sets the active Instrument.
    ''' </summary>
    ''' <param name="item">A candidate <see cref="IInstrument">instrument</see>.</param>
    Public Overloads Sub Add(ByVal item As IInstrument)
        Me._activeInstrument = item
        If item IsNot Nothing Then
            Me._activeInstrument.Index = MyBase.Count
            MyBase.Add(item)
        End If
    End Sub

    ''' <summary>
    ''' Returns true if all instruments are set.
    ''' </summary>
    Public Function AllActual() As Boolean
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled AndAlso Not instrument.AllParametersActual Then
                Return False
            End If
        Next
        Return True
    End Function

    ''' <summary>
    ''' Returns true if the instrument with the specified name exists.
    ''' </summary>
    ''' <param name="instrumentName">.</param>
    Public Function Exists(ByVal instrumentName As String) As Boolean
        For Each instrument As IInstrument In MyBase.Items
            If instrument.UniqueKey = instrumentName Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' Selects the active <see cref="IInstrument">instrument</see> by its unique key.
    ''' </summary>
    ''' <param name="Name">.</param>
    Public Function SelectFirstInstrument(ByVal name As String) As IInstrument
        For Each instrument As IInstrument In MyBase.Items
            If instrument.UniqueKey = name Then
                Me._activeInstrument = instrument
            End If
        Next
        Return Me._activeInstrument
    End Function

    ''' <summary>
    ''' Selects the active <see cref="IInstrument">instrument</see> by its unique key.
    ''' </summary>
    ''' <param name="item">A candidate <see cref="IInstrument">instrument</see>.</param>
    Public Function SelectActiveInstrument(ByVal item As isr.Core.IIndexable) As IInstrument
        If item Is Nothing Then
            Throw New ArgumentNullException("item")
        End If
        Me._activeInstrument = Me.Items(item.Index)
        Return Me._activeInstrument
    End Function

    ''' <summary>
    ''' Selects the active <see cref="IInstrument">instrument</see> by its unique key.
    ''' </summary>
    ''' <param name="index">specifes the instrument index</param>
    Public Function SelectActiveInstrument(ByVal index As Integer) As IInstrument
        Me._activeInstrument = Me.Items(index)
        Return Me._activeInstrument
    End Function

    ''' <summary>
    ''' Returns true if the collection has any enabled instruments.
    ''' </summary>
    Public Function HasEnabledInstruments() As Boolean
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' Returns true if any instrument has a measured value.
    ''' </summary>
    Public Function HasImpedanceValues() As Boolean
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled AndAlso instrument.Impedance.HasValue Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' Clears test results on all instrument whether enabled or not.
    ''' </summary>
    Public Function ClearTestResults() As Boolean
        Return Me.initiateTest
    End Function

    ''' <summary>
    ''' Clears test results on all instrument whether enabled or not.
    ''' </summary>
    Private Function initiateTest() As Boolean
        ' clear the test parameter.
        For Each instrument As IInstrument In MyBase.Items
            instrument.InitiateTest()
        Next
        Return True
    End Function

    ''' <summary>
    ''' Resets instruments and starts the parallel test. 
    ''' </summary>
    Private Function initiateParallelTest() As Boolean

        Me.initiateTest()

        ' select the first channel. This may solve the problem we are seeing
        ' where selecting a channel after a test breaks the test.
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                instrument.SelectChannel()
                Exit For
            End If
        Next

        ' enable parallel mode. Must not check condition on parallel enable as it may be off!
        Me._station.ParallelImpedanceEnabled = True

        ' initiate the parallel test
        Me._station.InitiateParallelTest()

        If Me._station.LastOperationFailed Then
            Me._station.MessageQueue.Enqueue("Failed initiating parallel test. Details: {0}", Me._station.LastStatusDetails)
            Return False
        ElseIf Kxci.SafeNativeMethods.LastQueryResult.ErrorOccurred Then
            Me._station.MessageQueue.Enqueue("Failed initiating parallel test. Query result: {0}", Kxci.SafeNativeMethods.LastQueryResult.ErrorDetails)
            Return False
        Else
            Me._station.MessageQueue.Enqueue("Last query result: {0}", Kxci.SafeNativeMethods.LastQueryResult.Reading)
        End If

        Return True

    End Function

    Private Shared _operationTimedOut As Boolean
    ''' <summary>
    ''' Gets an indication if the last operation timed out.
    ''' </summary>
    Public Shared ReadOnly Property OperationTimedOut() As Boolean
        Get
            Return InstrumentCollection._operationTimedOut
        End Get
    End Property

    ''' <summary>
    ''' Waits for test completion to complete on the specified channel.
    ''' </summary>
    ''' <param name="pollDelay"></param>
    Public Function AwaitTestCompletion(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean
        Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(timeout))
        InstrumentCollection._operationTimedOut = False
        Do Until Me._station.LastOperationFailed OrElse Me.LastTestCompleted() OrElse InstrumentCollection._operationTimedOut
            InstrumentCollection._operationTimedOut = DateTime.Now > endTime
            Threading.Thread.Sleep(pollDelay)
        Loop
        If Me._station.LastOperationFailed Then
            Me._station.MessageQueue.Enqueue("Failed waiting for test completion")
        ElseIf InstrumentCollection._operationTimedOut Then
            Me._station.MessageQueue.Enqueue("Timeout waiting for test completion")
            Me._station.LastOperationStatus = isr.Kte.OperationStatus.ErrorAvailable
        End If
        Return Me._station.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Returns true if all tests completed.
    ''' </summary>
    Public Function LastTestCompleted() As Boolean
        For Each instrument As IInstrument In MyBase.Items
            If Not instrument.LastTestCompleted Then
                Return False
            End If
        Next
        Return True
    End Function

    ''' <summary>
    ''' Measures impedance on all enabled instruments.
    ''' </summary>
    ''' <param name="pollDelay"></param>
    ''' <returns>Returns true if none measured or success.</returns>
    Public Function MeasureImpedance(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean

        ' clear results from previous test and mart the instrument test as not completed.
        Me.initiateTest()

        ' if no enabled instrument, this is done.
        If Not HasEnabledInstruments() Then
            Return True
        End If

        If Me._station.ChannelCount <= 1 OrElse MyBase.Count = 1 Then

            ' disable parallel mode.
            If Me._station.ParallelImpedanceEnabled Then
                Me._station.ParallelImpedanceEnabled = False
            End If

            ' this handles both single and emulated parallel modes.
            For Each instrument As IInstrument In MyBase.Items
                If instrument.Enabled Then
                    If instrument.SelectChannel() Then
                        If Not instrument.MeasureImpedance(timeout, pollDelay) Then
                            Exit For
                        End If
                    Else
                        Exit For
                    End If
                End If
            Next

            ' initiate parallel test.
        ElseIf initiateParallelTest() Then

            ' wait minimum test time.
            Threading.Thread.Sleep(pollDelay)

            ' wait for all instruments to complete or timeout.
            If AwaitTestCompletion(timeout, pollDelay) Then

                Me.FetchInstrumentsImpedance()

            End If

        End If
        Return Me._station.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Gets aperture from all instruments to refresh value.
    ''' This applies to Low Level Instruments in non-custom speed.
    ''' </summary>
    Public Function FetchInstrumentsAperture() As Boolean

        ' get status on all measurements
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                instrument.SelectChannel()
                instrument.ApertureGetter()
                If Me._station.LastOperationFailed Then
                    Me._station.MessageQueue.Enqueue("Instrument {0} failed fetching aperture", instrument.UniqueKey)
                End If
            End If
        Next
        Return Me._station.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Gets delay factor from all instruments to refresh value.
    ''' This applies to Low Level Instruments in non-custom speed.
    ''' </summary>
    Public Function FetchInstrumentsDelayFactor() As Boolean

        ' get status on all measurements
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                instrument.SelectChannel()
                instrument.DelayFactorGetter()
                If Me._station.LastOperationFailed Then
                    Me._station.MessageQueue.Enqueue("Instrument {0} failed fetching delay factor", instrument.UniqueKey)
                End If
            End If
        Next
        Return Me._station.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Gets Filter factor from all instruments to refresh value.
    ''' This applies to Low Level Instruments in non-custom speed.
    ''' </summary>
    Public Function FetchInstrumentsFilterFactor() As Boolean

        ' get status on all measurements
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                instrument.SelectChannel()
                instrument.FilterFactorGetter()
                If Me._station.LastOperationFailed Then
                    Me._station.MessageQueue.Enqueue("Instrument {0} failed fetching Filter factor", instrument.UniqueKey)
                End If
            End If
        Next
        Return Me._station.LastOperationSuccess
    End Function

    Public Function FetchInstrumentsImpedance() As Boolean

        ' get status on all measurements
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                instrument.SelectChannel()
                instrument.ImpedanceGetter()
                If Me._station.LastOperationFailed Then
                    Me._station.MessageQueue.Enqueue("Instrument {0} failed fetching impedance", instrument.UniqueKey)
                End If
            End If
        Next
        Return Me._station.LastOperationSuccess

    End Function

    Public Function FetchInstrumentsStatus() As Boolean

        ' get status on all measurements
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                instrument.SelectChannel()
                instrument.MeasurementStatusGetter()
                If Me._station.LastOperationFailed Then
                    Me._station.MessageQueue.Enqueue("Instrument {0} failed fetching status", instrument.UniqueKey)
                End If
            End If
        Next
        Return Me._station.LastOperationSuccess

    End Function

    ''' <summary>
    ''' Reset all instruments.
    ''' </summary>
    Public Function Reset() As Boolean Implements Core.IResettable.Reset
        For Each instrument As IInstrument In MyBase.Items
            If instrument.Enabled Then
                instrument.SelectChannel()
                instrument.Reset()
            End If
        Next
        Return Me._station.LastOperationSuccess
    End Function

    ''' <summary>
    ''' Returns true if has system messages. 
    ''' </summary>
    Public Function HasSystemMessages() As Boolean
        If Me._station.MessageQueue.HasItems Then
            Return True
        End If
        For Each instrument As IInstrument In MyBase.Items
            If instrument.MessageQueue.HasItems Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' Gets all last interface messages.
    ''' </summary>
    Public Function DequeueSystemMessages(ByVal includeStatusDetails As Boolean) As String
        Dim messages As New System.Text.StringBuilder
        If includeStatusDetails Then
            Dim details As String = Me._station.LastStatusDetails
            If String.IsNullOrWhiteSpace(details) Then
                If messages.Length > 0 Then
                    messages.AppendLine()
                End If
                messages.Append(details)
            End If
        End If
        If Me._station.MessageQueue.HasItems Then
            If messages.Length > 0 Then
                messages.AppendLine()
            End If
            messages.AppendLine("Station reports: ")
            messages.Append(Me._station.MessageQueue.DequeueAll)
        End If
        For Each instrument As IInstrument In MyBase.Items
            If instrument.MessageQueue.HasItems Then
                If messages.Length > 0 Then
                    messages.AppendLine()
                End If
                messages.AppendFormat("Instrument {0} reports: ", instrument.UniqueKey)
                messages.Append(instrument.MessageQueue.DequeueAll)
            End If
        Next
        Return messages.ToString
    End Function

End Class
