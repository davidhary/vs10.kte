''' <summary>Handles an function call expection.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/30/09" by="David" revision="1.0.3376.x">
''' Created
''' </history>
<Serializable()> Public Class FunctionCallException
  Inherits BaseException

  Private Const _defMessage As String = "function call failed."

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  ''' <summary>Constructor with no parameters.</summary>
  Public Sub New()
    MyBase.New(FunctionCallException._defMessage)
  End Sub

  ''' <summary>Constructor specifying the Message to be set.</summary>
  ''' <param name="message">Specifies the exception message.</param>
  Public Sub New(ByVal message As String)
    MyBase.New(message)
  End Sub

  ''' <summary>Constructor specifying the Message and InnerException property to be set.</summary>
  ''' <param name="message">Specifies the exception message.</param>
  ''' <param name="innerException">Sets a reference to the InnerException.</param>
  Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
    MyBase.New(message, innerException)
  End Sub

  ''' <summary>Constructor with custom data that uses a custom format string.</summary>
  ''' <param name="format">String expression that specifies the message format.</param>
  ''' <param name="args">Format argumetns</param>
  Public Sub New(ByVal format As String, ByVal ParamArray args() As Object)
    Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
  End Sub

  ''' <summary>Constructor used for deserialization of the exception class.</summary>
  ''' <param name="info">Represents the SerializationInfo of the exception.</param>
  ''' <param name="context">Represents the context information of the exception.</param>
  Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, 
      ByVal context As System.Runtime.Serialization.StreamingContext)
    MyBase.New(info, context)
  End Sub

#End Region

End Class

