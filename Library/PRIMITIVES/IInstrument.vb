﻿''' <summary>
''' An interface for controlling a Keithley Test Environment (4200) instrument.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
Public Interface IInstrument

    Inherits isr.Core.ICollectable, isr.Core.IIndexable, isr.Core.IResettable

#Region " IMPEDANCE "

#Region " APERTURE "

    ''' <summary>
    ''' Gets or sets the aperture. Readonly for non-custom speed.
    ''' </summary>
    Property Aperture() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the aperture.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function ApertureGetter() As Boolean

    ''' <summary>
    ''' Sets the aperture cached value and set the actual value if a new cached value.
    ''' </summary>
    Function ApertureSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual aperture to its cached value irrespective of the actual value.
    ''' </summary>
    Function ApertureSetter() As Boolean

#End Region

    ''' <summary>
    ''' Gets or sets the corrected cable length.
    ''' </summary>
    Property CableCorrection() As CorrectedCableLength

    ''' <summary>
    ''' Gets the Cable Correction.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function CableCorrectionGetter() As Boolean

    ''' <summary>
    ''' Sets the Cable Correction cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function CableCorrectionSetter(ByVal value As CorrectedCableLength) As Boolean

    ''' <summary>
    ''' Sets the actual Cable Correction to its cached value.
    ''' </summary>
    Function CableCorrectionSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the cable length for correction.
    ''' </summary>
    Property CableLength() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the Cable Length.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function CableLengthGetter() As Boolean

    ''' <summary>
    ''' Sets the Cable Length cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function CableLengthSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Cable Length to its cached value.
    ''' </summary>
    Function CableLengthSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the delay factor. Readonly for non-custom speed.
    ''' </summary>
    Property DelayFactor() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the Delay Factor.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function DelayFactorGetter() As Boolean

    ''' <summary>
    ''' Sets the Delay Factor cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function DelayFactorSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Delay Factor to its cached value.
    ''' </summary>
    Function DelayFactorSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the filter factor.
    ''' </summary>
    Property FilterFactor() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the Filter Factor.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function FilterFactorGetter() As Boolean

    ''' <summary>
    ''' Sets the Filter Factor cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function FilterFactorSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Filter Factor to its cached value.
    ''' </summary>
    Function FilterFactorSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the Hold Time.
    ''' </summary>
    Property HoldTime() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the the Hold Time.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function HoldTimeGetter() As Boolean

    ''' <summary>
    ''' Sets the Hold Time cached value. Sets the actual value if a new cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function HoldTimeSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Hold Time to its cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function HoldTimeSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the measured impedance.
    ''' </summary>
    Property Impedance() As isr.Kte.Impedance

    ''' <summary>
    ''' Gets the Measured Impedance.
    ''' </summary>
    ''' <returns>true if success.</returns>
    Function ImpedanceGetter() As Boolean

    ''' <summary>
    ''' Gets or sets the condition enabling the load compensation.
    ''' </summary>
    Property LoadCompensationEnabled() As isr.Core.PresettableCache(Of Boolean)

    ''' <summary>
    ''' Gets the Load Compensation Enabled status.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function LoadCompensationEnabledGetter() As Boolean

    ''' <summary>
    ''' Sets the Load Compensation Enabled status cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function LoadCompensationEnabledSetter(ByVal value As Boolean) As Boolean

    ''' <summary>
    ''' Sets the actual Load Compensation Enabled status to its cached value.
    ''' </summary>
    Function LoadCompensationEnabledSetter() As Boolean

    ''' <summary>
    ''' Specifies the impedance measurement model.
    ''' </summary>
    Property MeasurementModel() As isr.Core.PresettableFormattableCache(Of Integer)

    ''' <summary>
    ''' Gets the Impedance Measurement Model.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function MeasurementModelGetter() As Boolean

    ''' <summary>
    ''' Sets the Impedance Measurement Model cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function MeasurementModelSetter(ByVal value As Integer) As Boolean

    ''' <summary>
    ''' Sets the actual Impedance Measurement Model to its cached value.
    ''' </summary>
    Function MeasurementModelSetter() As Boolean

    ''' <summary>
    ''' Specifies the measurement speed.
    ''' </summary>
    Property MeasurementSpeed() As isr.Core.PresettableFormattableCache(Of Integer)

    ''' <summary>
    ''' Gets the Measurement Speed.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function MeasurementSpeedGetter() As Boolean

    ''' <summary>
    ''' Sets the Measurement Speed cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function MeasurementSpeedSetter(ByVal value As Integer) As Boolean

    ''' <summary>
    ''' Sets the actual Measurement Speed to its cached value.
    ''' </summary>
    Function MeasurementSpeedSetter() As Boolean

    ''' <summary>
    ''' Specifies the measurement Status.
    ''' </summary>
    Property MeasurementStatus() As Integer

    ''' <summary>
    ''' Gets the Measurement Status.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function MeasurementStatusGetter() As Boolean

    ''' <summary>
    ''' Gets or sets the condition enabling open compensation.
    ''' </summary>
    Property OpenCompensationEnabled() As isr.Core.PresettableCache(Of Boolean)

    ''' <summary>
    ''' Gets the Open Compensation Enabled.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function OpenCompensationEnabledGetter() As Boolean

    ''' <summary>
    ''' Sets the Open Compensation Enabled cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function OpenCompensationEnabledSetter(ByVal value As Boolean) As Boolean

    ''' <summary>
    ''' Sets the actual Open Compensation Enabled to its cached value.
    ''' </summary>
    Function OpenCompensationEnabledSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the sampling bias.
    ''' </summary>
    Property SamplingBias() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the actual sample bias.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingBiasGetter() As Boolean

    ''' <summary>
    ''' Sets the sample bias cached value. Sets the actual value if a new cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingBiasSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual sample bias to its cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingBiasSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the sample count.
    ''' </summary>
    Property SamplingCount() As isr.Core.PresettableFormattableCache(Of Integer)

    ''' <summary>
    ''' Gets the actual sample count.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingCountGetter() As Boolean

    ''' <summary>
    ''' Sets the sample count cached value. Sets the actual value if a new cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingCountSetter(ByVal value As Integer) As Boolean

    ''' <summary>
    ''' Sets the actual sample count to its cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingCountSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the sampling interva.
    ''' </summary>
    Property SamplingInterval() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the actual sample interval.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingIntervalGetter() As Boolean

    ''' <summary>
    ''' Sets the sample interval cached value. Sets the actual value if a new cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingIntervalSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual sample interval to its cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SamplingIntervalSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the AC sense current range. 
    ''' </summary>
    Property SenseCurrentRange() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the Sense Current Range.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SenseCurrentRangeGetter() As Boolean

    ''' <summary>
    ''' Sets the Sense Current Range cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function SenseCurrentRangeSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Sense Current Range to its cached value.
    ''' </summary>
    Function SenseCurrentRangeSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the AC source settling time.
    ''' Used only if measurement follows setup right away.
    ''' </summary>
    Property SettlingTime() As Single

    ''' <summary>
    ''' Gets or sets the condition enabling short compensation.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Property ShortCompensationEnabled() As isr.Core.PresettableCache(Of Boolean)

    ''' <summary>
    ''' Gets the Short Compensation Enabled.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Function ShortCompensationEnabledGetter() As Boolean

    ''' <summary>
    ''' Sets the Short Compensation Enabled cached value. Sets the actual value if a new cached value.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Function ShortCompensationEnabledSetter(ByVal value As Boolean) As Boolean

    ''' <summary>
    ''' Sets the actual Short Compensation Enabled to its cached value.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
    Function ShortCompensationEnabledSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the DC source bias level.
    ''' </summary>
    Property SourceBias() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the Source Bias cached value. Sets the actual value if a new cached value.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SourceBiasGetter() As Boolean

    ''' <summary>
    ''' Sets the Source Bias.
    ''' </summary>
    Function SourceBiasSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Source Bias to its cached value.
    ''' </summary>
    Function SourceBiasSetter() As Boolean

    ''' <summary>
    ''' Gets or sets the source frequency.
    ''' </summary>
    Property SourceFrequency() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the Source Frequency.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SourceFrequencyGetter() As Boolean

    ''' <summary>
    ''' Sets the Source Frequency cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function SourceFrequencySetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Source Frequency to its cached value.
    ''' </summary>
    Function SourceFrequencySetter() As Boolean

    ''' <summary>
    ''' Gets or sets the AC source level. 
    ''' </summary>
    Property SourceLevel() As isr.Core.PresettableFormattableCache(Of Single)

    ''' <summary>
    ''' Gets the Source Level.
    ''' </summary>
    ''' <returns>The <see cref="isr.Kte.OperationStatus">operation status</see>.</returns>
    Function SourceLevelGetter() As Boolean

    ''' <summary>
    ''' Sets the Source Level cached value. Sets the actual value if a new cached value.
    ''' </summary>
    Function SourceLevelSetter(ByVal value As Single) As Boolean

    ''' <summary>
    ''' Sets the actual Source Level to its cached value.
    ''' </summary>
    Function SourceLevelSetter() As Boolean

#End Region

#Region " INSTRUMENT "

    ''' <summary>
    ''' Returns the list of parameter names of items which values are not actual.
    ''' </summary>
    Function NonActual() As String

    ''' <summary>
    ''' Returns true if all parameters equal their actual value.
    ''' Namely, all actual parameter values were set to their cached values.
    ''' </summary>
    ReadOnly Property AllParametersActual() As Boolean

    ''' <summary>
    ''' Applies all parameters to the instrument.
    ''' </summary>
    ''' <returns>Ture if any changes were applied.</returns>
    Function Apply() As Boolean

    ''' <summary>
    ''' Waits for test to complete on the specified channel.
    ''' </summary>
    ''' <param name="timeout"></param>
    ''' <param name="pollDelay"></param>
    Function AwaitTestCompletion(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean

    ''' <summary>
    ''' Gets or sets the condition indicating if the instrument is enabled. 
    ''' This is a way to use sparse testing. If a test is nop, the instrument is tagged as disabled so its 
    ''' data need not be retrieved even when using parallel measurements. 
    ''' </summary>
    Property Enabled() As Boolean

    ''' <summary>
    ''' Returns true if the instrument exists in the test station.
    ''' </summary>
    Function Exists() As Boolean

    ''' <summary>
    ''' Initiates a test.
    ''' </summary>
    Sub InitiateTest()

    ''' <summary>
    ''' Gets an indication if the last test completed.
    ''' </summary>
    Function LastTestCompleted() As Boolean

    ''' <summary>
    ''' Measures the impedance and gets the impedance data for a single instrument.
    ''' </summary>
    ''' <param name="timeout">Specifies timeout in MS</param>
    ''' <param name="pollDelay">Specifies the time between polls in MS.</param>
    Function MeasureImpedance(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean

    ''' <summary>
    ''' Gets an indication if the test completed.
    ''' </summary>
    Function QueryTestCompleted() As Boolean

    ''' <summary>
    ''' Gets an indication if the last operation timed out.
    ''' </summary>
    Property OperationTimedOut() As Boolean

    ''' <summary>
    ''' Refresh all parameters by reading back values from the instrument.
    ''' </summary>
    Function Refresh() As Boolean

    ''' <summary>
    ''' Selects an impedance measurement instrument.
    ''' </summary>
    ''' <param name="channelNumber"></param>
    Function SelectImpedanceInstrument(ByVal channelNumber As Integer) As Boolean

    ''' <summary>
    ''' Selects the innstrument channel.
    ''' </summary>
    Function SelectChannel() As Boolean

    ''' <summary>
    ''' Gets the instrument channel number.
    ''' </summary>
    Property ChannelNumber() As Integer

#End Region

#Region " STATUS "

    ''' <summary>
    ''' Returns true if last operation was okay.
    ''' </summary>
    ReadOnly Property LastOperationSuccess() As Boolean

    ''' <summary>
    ''' Returns true if last operation failed.
    ''' </summary>
    ReadOnly Property LastOperationFailed() As Boolean

    ''' <summary>
    ''' Gets the last status.
    ''' </summary>
    Property LastOperationStatus() As Integer

    ''' <summary>
    ''' Gets the last status of socket communications. Applies only to the message based instrument
    ''' </summary>
    Property LastSocketStatus() As Integer

    ''' <summary>
    ''' Gets the last status.
    ''' </summary>
    Function LastStatusDetails() As String

#End Region

#Region " MESSAGES "

    ''' <summary>
    ''' Gets reference to the message queue.
    ''' </summary>
    ReadOnly Property MessageQueue() As isr.Core.MessageQueue

#End Region

End Interface

