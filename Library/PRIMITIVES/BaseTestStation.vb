﻿''' <summary>
''' Common class for implementing an <see cref="ITestStation">test station</see>.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
Public MustInherit Class BaseTestStation

    Implements ITestStation

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructor with no parameters.</summary>
    Protected Sub New()
        MyBase.New()
        Me._messageQueue = New isr.Core.MessageQueue
    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me._instruments IsNot Nothing Then
                        Me._instruments.Clear()
                        Me._instruments = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " ITESTSTATION: STATUS "

    ''' <summary>
    ''' Returns true if last operation succeeded.
    ''' This is signified by the <see cref="OperationStatus">last status</see>
    ''' greater than or equal <see cref="OperationStatus.Success">okay</see>
    ''' and <see cref="LastSocketStatus">socket error</see> equals
    ''' <see cref="Net.Sockets.SocketError.Success">success</see>
    ''' </summary>
    Public ReadOnly Property LastOperationSuccess() As Boolean Implements ITestStation.LastOperationSuccess
        Get
            Return LastOperationStatus >= isr.Kte.OperationStatus.Success AndAlso
                LastSocketStatus = Net.Sockets.SocketError.Success
        End Get
    End Property

    ''' <summary>
    ''' Returns true if last operation failed.
    ''' This is signified by the <see cref="OperationStatus">last status</see>
    ''' smaller than <see cref="OperationStatus.Success">okay</see> (negative)
    ''' or <see cref="LastSocketStatus">socket status</see> not 
    ''' equal <see cref="Net.Sockets.SocketError.Success">success</see>
    ''' </summary>
    Public ReadOnly Property LastOperationFailed() As Boolean Implements ITestStation.LastOperationFailed
        Get
            Return LastOperationStatus < isr.Kte.OperationStatus.Success OrElse LastSocketStatus <> Net.Sockets.SocketError.Success
        End Get
    End Property

    ''' <summary>
    ''' Gets the last status of socket communications. 
    ''' Applies only to the message based instrument
    ''' </summary>
    Public MustOverride ReadOnly Property LastSocketStatus() As Integer Implements ITestStation.LastSocketStatus

    ''' <summary>
    ''' Gets or sets the last operation status.
    ''' </summary>
    Public MustOverride Property LastOperationStatus() As Integer Implements ITestStation.LastOperationStatus

    ''' <summary>
    ''' Returns overall status details
    ''' </summary>
    MustOverride Function LastStatusDetails() As String Implements ITestStation.LastStatusDetails

#End Region

#Region " I TEST STATION: MESSAGES "

    Private _messageQueue As isr.Core.MessageQueue
    ''' <summary>
    ''' Gets reference to the message queue.
    ''' </summary>
    Public ReadOnly Property MessageQueue() As isr.Core.MessageQueue Implements ITestStation.MessageQueue
        Get
            Return Me._messageQueue
        End Get
    End Property

#End Region

#Region " ITESTSTATION: STATION "

    Public MustOverride ReadOnly Property ChannelCount() As Integer Implements ITestStation.ChannelCount

    ''' <summary>
    ''' Turns off the open sentinel and returns the status of the last operation.
    ''' </summary>
    Public Overridable Function CloseTestStation() As Boolean Implements ITestStation.CloseTestStation
        Me._isopen = False
        Return Me.LastOperationSuccess
    End Function

    Private _isopen As Boolean
    ''' <summary>
    ''' Gets the open condition of the test station.
    ''' </summary>
    Public ReadOnly Property IsOpen() As Boolean Implements ITestStation.IsOpen
        Get
            Return Me._isopen
        End Get
    End Property

    ''' <summary>
    ''' Initializes the test state and the collection of instruments.
    ''' </summary>
    Public Overridable Function InitializeTestStation() As Boolean Implements ITestStation.InitializeTestStation

        ' create a new collection of instruments
        Me._instruments = New isr.Kte.InstrumentCollection(Me)

    End Function

    Public MustOverride Function InitiateParallelTest() As Boolean Implements ITestStation.InitiateParallelTest

    ''' <summary>
    ''' Turns on the open sentinel and returns the status of the last operation.
    ''' </summary>
    ''' <param name="timeout">Specifies the timeout for opening the station.</param>
    Public Overridable Function OpenTestStation(ByVal timeout As Integer) As Boolean Implements ITestStation.OpenTestStation
        Me._isopen = True
        Return Me.LastOperationSuccess
    End Function

    Public MustOverride Property ParallelImpedanceEnabled() As Boolean Implements ITestStation.ParallelImpedanceEnabled

    Private LastOperationStatusByte As Integer
    Public Property LastStatusByte() As Integer Implements ITestStation.LastStatusByte
        Get
            Return LastOperationStatusByte
        End Get
        Set(ByVal value As Integer)
            LastOperationStatusByte = value
        End Set
    End Property

    Public MustOverride Function StatusByteGetter() As Integer Implements ITestStation.StatusByteGetter

    Public MustOverride ReadOnly Property SupportsParallel() As Boolean Implements ITestStation.SupportsParallel

#End Region

#Region " ITEST STATION: INSTRUMENTS "

    ''' <summary>
    ''' Returns reference to the active instrument.
    ''' </summary>
    Public ReadOnly Property ActiveInstrument() As IInstrument Implements ITestStation.ActiveInstrument
        Get
            Return Me._instruments.ActiveInstrument
        End Get
    End Property

    ''' <summary>
    ''' Adds a new instrument or selects an existing instrument.
    ''' </summary>
    ''' <param name="family">Specifies the instrument family name, e.g., CVU.</param>
    ''' <param name="number">Specifies the instrument number within the family.</param>
    ''' <param name="forceMany">Specifies the condition for forcing creating
    ''' more that one instrument on a system that has a single instrument. This is does for testing.</param>
    ''' <returns>True if success or False if failure.</returns>
    Public MustOverride Function AddInstrument(ByVal family As String, ByVal number As Integer, 
                                              ByVal forceMany As Boolean) As Boolean Implements ITestStation.AddInstrument

    Private _instruments As isr.Kte.InstrumentCollection
    ''' <summary>
    ''' Gets reference to the collection of instruments.
    ''' </summary>
    Public Function Instruments() As InstrumentCollection Implements ITestStation.Instruments
        Return Me._instruments
    End Function

#End Region

#Region " HARDWARE RESET PROCESS "

    ''' <summary>
    ''' Gets or sets the reset process file name.
    ''' </summary>
    Public Shared Property ResetProcessFilePathName() As String

    ''' <summary>
    ''' Executes the reset process.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="timeout")> 
    Public Shared Function ExecuteResetProcess(ByVal timeout As Integer) As Boolean

        If String.IsNullOrWhiteSpace(MessageBasedStation._ResetProcessFilePathName) Then
            Throw New isr.Kte.BaseException("File path name is required for opening the process.")
        End If

        ' instantiate a new process.
        Using resetProcess As Process = New Process()
            Dim processInfo As ProcessStartInfo = New ProcessStartInfo(BaseTestStation._ResetProcessFilePathName)

            ' execute the process
            resetProcess.StartInfo = processInfo
            Return resetProcess.Start()
        End Using
        ' do not wait for exit. Return resetProcess.WaitForExit(timeout)

    End Function

    ''' <summary>
    ''' Resets instruments to default values.
    ''' </summary>
    Public Function Reset() As Boolean Implements Core.IResettable.Reset

        Return Me.Instruments.Reset()

    End Function

#End Region

End Class

