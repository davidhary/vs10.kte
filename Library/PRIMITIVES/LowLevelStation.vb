﻿''' <summary>
''' Implements a low-level <see cref="ITestStation">TestStation</see> 
''' using the Keithley Test Environment Linear Parameteric Test (LPT) library.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
Public Class LowLevelStation

    Inherits BaseTestStation

#Region " ITEST STATION "

    Private _channelCount As Integer
    Public Overrides ReadOnly Property ChannelCount() As Integer
        Get
            Dim count As Integer = isr.Kte.Lpt.SafeNativeMethods.ChannelCountGetter(4)
            If MyBase.LastOperationFailed Then
                count = Me._channelCount
                MyBase.MessageQueue.Enqueue("Failed getting number of channels. Count set to {1}. {0}", Me.LastStatusDetails(), Me._channelCount)
            End If
            Me._channelCount = count
            Return Me._channelCount
        End Get
    End Property

    Public Overrides Function InitializeTestStation() As Boolean
        ' create a new collection of instruments.
        MyBase.InitializeTestStation()
        If isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus = isr.Kte.OperationStatus.Success Then
            isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus = isr.Kte.OperationStatus.ErrorAvailable
        End If
        Lpt.SafeNativeMethods.InitializeTestStation()
        If MyBase.LastOperationFailed Then
            MyBase.MessageQueue.Enqueue("Failed initializing test station. {0}", Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function InitiateParallelTest() As Boolean
        Lpt.SafeNativeMethods.InitiateParallelTest()
        If MyBase.LastOperationFailed Then
            MyBase.MessageQueue.Enqueue("Failed initiating parallel test. {0}", Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Property ParallelImpedanceEnabled() As Boolean
        Get
            ParallelImpedanceEnabled = isr.Kte.Lpt.SafeNativeMethods.ParallelEnabledGetter()
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed getting parallel impedance condition. {0}", Me.LastStatusDetails())
            End If
        End Get
        Set(ByVal value As Boolean)
            isr.Kte.Lpt.SafeNativeMethods.ParallelEnabledSetter(value)
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed setting parallel impedance condition to {0}. {1}", value, Me.LastStatusDetails())
            End If
        End Set
    End Property

    Public Overrides Function CloseTestStation() As Boolean

        MyBase.CloseTestStation()
        Lpt.SafeNativeMethods.CloseTestStation()
        If MyBase.LastOperationFailed Then
            MyBase.MessageQueue.Enqueue("Failed closing test station. {0}", Me.LastStatusDetails())
        End If
        Return Not MyBase.IsOpen

    End Function

    ''' <summary>
    ''' Opens the test system.
    ''' </summary>
    ''' <param name="timeout">Specifies the timeout for opening the station.</param>
    Public Overrides Function OpenTestStation(ByVal timeout As Integer) As Boolean

        If MyBase.IsOpen Then
            Return MyBase.IsOpen
        End If

        Lpt.SafeNativeMethods.OpenTestStation()
        If MyBase.LastOperationSuccess Then
            MyBase.OpenTestStation(timeout)
        Else
            MyBase.MessageQueue.Enqueue("Failed opening test station. {0}", Me.LastStatusDetails())
            Me.CloseTestStation()
        End If
        Return Me.IsOpen

    End Function

    Public Overrides Function StatusByteGetter() As Integer
        Return 0
    End Function

    Private _supportParallel As Boolean
    Public Overrides ReadOnly Property SupportsParallel() As Boolean
        Get
            Me._supportParallel = Lpt.SafeNativeMethods.SupportsParallel
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed checking if the station supports parallel mode. {0}", Me.LastStatusDetails())
            End If
            Return Me._supportParallel
        End Get
    End Property

#End Region

#Region " ITEST STATION: STATUS "

    ''' <summary>
    ''' Gets the last operation status.
    ''' </summary>
    Public Overrides Property LastOperationStatus() As Integer
        Get
            Return Lpt.SafeNativeMethods.LastOperationStatus
        End Get
        Set(ByVal value As Integer)
            Lpt.SafeNativeMethods.LastOperationStatus = value
        End Set
    End Property

    ''' <summary>
    ''' Gets the last status of socket communications. 
    ''' Applies only to the message based instrument
    ''' </summary>
    Public Overrides ReadOnly Property LastSocketStatus() As Integer
        Get
            Return Lpt.SafeNativeMethods.LastSocketStatus
        End Get
    End Property

    ''' <summary>
    ''' Returns overall status details
    ''' </summary>
    Public Overrides Function LastStatusDetails() As String
        Return isr.Kte.Lpt.SafeNativeMethods.LastStatusDetails()
    End Function

#End Region

#Region " ITEST STATION: INSTRUMENTS "

    ''' <summary>
    ''' Adds a new instrument or selects an existing instrument.
    ''' </summary>
    ''' <param name="family">Specifies the instrument family name, e.g., CVU.</param>
    ''' <param name="number">Specifies the instrument number within the family.</param>
    ''' <param name="forceMany">Specifies the condition for forcing creating
    ''' more that one instrument on a system that has a single instrument. This is does for testing.</param>
    Public Overrides Function AddInstrument(ByVal family As String, ByVal number As Integer, 
                                              ByVal forceMany As Boolean) As Boolean

        Dim instrument As isr.Kte.LowLevelInstrument

        ' check if the requested instrument already exists
        Dim name As String = BaseInstrument.BuildInstrumentName(family, number)

        If MyBase.Instruments IsNot Nothing AndAlso MyBase.Instruments.Exists(name) AndAlso Not forceMany Then

            MyBase.Instruments.SelectFirstInstrument(name)
            MyBase.MessageQueue.Enqueue("Instrument {0} selected", MyBase.ActiveInstrument.UniqueKey)

        Else

            MyBase.MessageQueue.Enqueue("Adding instrument {0}", name)

            ' select the control mode.
            instrument = New isr.Kte.LowLevelInstrument

            ' reset instrument state if the first instrument.
            If MyBase.Instruments.Count = 0 Then
                instrument.ResetKnownState()
                If instrument.LastOperationFailed Then
                    MyBase.MessageQueue.Enqueue("Failed resetting known state. Failure ignored. {0}", instrument.LastStatusDetails())
                End If
            End If

            Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
            instrument.SelectImpedanceInstrument(number)
            If instrument.LastOperationSuccess Then

                MyBase.MessageQueue.Enqueue("Instrument selected in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)

                ' check if in parallel mode
                If Me.ParallelImpedanceEnabled Then

                    ' add instrument to the collection
                    MyBase.Instruments.Add(instrument)

                Else

                    ' remove the last instrument and add a new one.
                    MyBase.Instruments.Clear()
                    MyBase.Instruments.Add(instrument)

                End If

            Else
                MyBase.MessageQueue.Enqueue("Failed selecting instrument; {0}.", instrument.LastStatusDetails())
                Return False
            End If
        End If
        Return True

    End Function

#End Region

End Class
