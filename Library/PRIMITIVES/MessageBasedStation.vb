﻿''' <summary>
''' Implements a message-based <see cref="ITestStation">TestStation</see> 
''' using the Keithley Test Environment External Control Interface (KXCI) library.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/07/2009" by="David" revision="1.0.3384.x">
''' Created
''' </history>
Public Class MessageBasedStation

    Inherits BaseTestStation

#Region " ITEST STATION "

    Private _channelCount As Integer?
    Public Overrides ReadOnly Property ChannelCount() As Integer
        Get
            If Me._channelCount.HasValue Then
                Return Me._channelCount.Value
            End If
            Try
                Me._channelCount = isr.Kte.Kxci.SafeNativeMethods.ChannelCountGetter(4)
            Catch ex As FunctionCallException
                Me._channelCount = 1
                MyBase.MessageQueue.Enqueue("Exception occurred getting number of channels. Count set to {1}. Details = {0}", ex.Message, Me._channelCount)
                If isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus = isr.Kte.OperationStatus.Success Then
                    isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus = isr.Kte.OperationStatus.MessageAvailable
                End If
            End Try
            If MyBase.LastOperationFailed Then
                If Not Me._channelCount.HasValue Then
                    Me._channelCount = 1
                End If
                MyBase.MessageQueue.Enqueue("Failed getting number of channels. Count set to {1}. {0}", Me.LastStatusDetails(), Me._channelCount)
            End If
            Return Me._channelCount.Value
        End Get
    End Property

    Public Overrides Function InitializeTestStation() As Boolean
        Dim timedOut As Boolean
        Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(10000))
        Do
            ' clear cached values
            Me._channelCount = New Integer?
            ' create a new collection of instruments.
            MyBase.InitializeTestStation()
            Threading.Thread.Sleep(500)
            Kxci.SafeNativeMethods.ResetKnownState()
            If MyBase.LastOperationFailed Then
                timedOut = DateTime.Now > endTime
            End If
        Loop Until timedOut OrElse MyBase.LastOperationSuccess
        If MyBase.LastOperationFailed Then
            MyBase.MessageQueue.Enqueue("Failed initializing test station. {0}", Me.LastStatusDetails())
        Else
            ' add all CVU instruments
            For i As Integer = 1 To Me.ChannelCount
                Me.AddInstrument("CVU", i, False)
            Next
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Public Overrides Function InitiateParallelTest() As Boolean
        Kxci.SafeNativeMethods.InitiateParallelTest()
        If MyBase.LastOperationFailed Then
            MyBase.MessageQueue.Enqueue("Failed initiating parallel test. {0}", Me.LastStatusDetails())
        End If
        Return MyBase.LastOperationSuccess
    End Function

    Private _parallelImpedanceEnabled As Boolean
    Public Overrides Property ParallelImpedanceEnabled() As Boolean
        Get
            Me._parallelImpedanceEnabled = isr.Kte.Kxci.SafeNativeMethods.ParallelEnabledGetter(Me._parallelImpedanceEnabled)
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed getting parallel impedance condition. {0}", Me.LastStatusDetails())
            End If
            Return Me._parallelImpedanceEnabled
        End Get
        Set(ByVal value As Boolean)
            Me._parallelImpedanceEnabled = value
            isr.Kte.Kxci.SafeNativeMethods.ParallelEnabledSetter(value)
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed setting parallel impedance condition to {0}. {1}", value, Me.LastStatusDetails())
            End If
        End Set
    End Property

    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Public Overrides Function CloseTestStation() As Boolean
        If MyBase.IsOpen Then
            MyBase.CloseTestStation()
            Kxci.SafeNativeMethods.CloseTestStation()
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed disconnecting test station. {0}", Me.LastStatusDetails())
            End If
        End If
        Return Not Me.IsOpen
    End Function

    ''' <summary>
    ''' Opens the test system.
    ''' </summary>
    ''' <param name="timeout">Specifies the timeout for opening the station.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
    Public Overrides Function OpenTestStation(ByVal timeout As Integer) As Boolean

        If MyBase.IsOpen Then
            Return MyBase.IsOpen
        End If

        ' clear the status
        isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus = isr.Kte.OperationStatus.Success
        isr.Kte.Kxci.SafeNativeMethods.LastSocketStatus = Net.Sockets.SocketError.Success

        ' Check if KXCI is on.  If not turn KXCI on first.
        Try

            If MessageBasedStation.SelectHostProcess() Then

                If Not MessageBasedStation._isHostProcessResponding(timeout, 200) Then
                    MyBase.MessageQueue.Enqueue("Time out waiting for host process.")
                    Return Me.IsOpen
                End If

            Else

                isr.Kte.Kxci.SafeNativeMethods.LastOperationStatus = isr.Kte.OperationStatus.ErrorAvailable
                MyBase.MessageQueue.Enqueue("Unable to open test station because host process has not started. Start KXCI first.")
                Return MyBase.LastOperationSuccess

            End If
        Catch ex As Exception
            MyBase.MessageQueue.Enqueue("Exception occurred selecting host process. Details: {0}", ex)
            Return False
        End Try

        ' try opening a connection to the host process. 
        Dim timedOut As Boolean
        Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(timeout))

        Do
            If Kxci.SafeNativeMethods.OpenTestStation() Then
                MyBase.OpenTestStation(timeout)
            Else
                timedOut = DateTime.Now > endTime
                Threading.Thread.Sleep(500)
            End If
        Loop Until MyBase.IsOpen OrElse timedOut

        If Not MyBase.IsOpen Then
            MyBase.MessageQueue.Enqueue("Time out connecting to the test station. {0}", Me.LastStatusDetails())
        End If

        Return Me.IsOpen

    End Function

    Public Overrides Function StatusByteGetter() As Integer
        Kxci.SafeNativeMethods.StatusByteGetter()
        If MyBase.LastOperationFailed Then
            MyBase.MessageQueue.Enqueue("Failed reading status byte. {0}", Me.LastStatusDetails())
        End If
        Return Kxci.SafeNativeMethods.LastStatusByte
    End Function

    Private _supportParallel As Boolean
    Public Overrides ReadOnly Property SupportsParallel() As Boolean
        Get
            Me._supportParallel = Me.ChannelCount > 1
            If MyBase.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed checking if the station supports parallel mode. {0}", Me.LastStatusDetails())
            End If
            Return Me._supportParallel
        End Get
    End Property

#End Region

#Region " ITEST STATION: STATUS "

    ''' <summary>
    ''' Gets the last operation status.
    ''' </summary>
    Public Overrides Property LastOperationStatus() As Integer
        Get
            Return Kxci.SafeNativeMethods.LastOperationStatus
        End Get
        Set(ByVal value As Integer)
            Kxci.SafeNativeMethods.LastOperationStatus = value
        End Set
    End Property

    ''' <summary>
    ''' Gets the last status of socket communications. 
    ''' Applies only to the message based instrument
    ''' </summary>
    Public Overrides ReadOnly Property LastSocketStatus() As Integer
        Get
            Return Kxci.SafeNativeMethods.LastSocketStatus
        End Get
    End Property

    ''' <summary>
    ''' Returns overall status details
    ''' </summary>
    Public Overrides Function LastStatusDetails() As String
        Return isr.Kte.Kxci.SafeNativeMethods.LastStatusDetails
    End Function

#End Region

#Region " ITEST STATION: INSTRUMENTS "

    ''' <summary>
    ''' Adds a new instrument or selects an existing instrument.
    ''' The new instrument becomes the active instrument.
    ''' </summary>
    ''' <param name="family">Specifies the instrument family name, e.g., CVU.</param>
    ''' <param name="number">Specifies the instrument number within the family.</param>
    ''' <param name="forceMany">Specifies the condition for forcing creating
    ''' more that one instrument on a system that has a single instrument. This is does for testing.</param>
    ''' <history date="12/04/2009" by="David" revision="1.0.3625.x">
    ''' Reset all instruments. 
    ''' </history>
    Public Overrides Function AddInstrument(ByVal family As String, ByVal number As Integer, 
                                              ByVal forceMany As Boolean) As Boolean

        Dim instrument As isr.Kte.MessageBasedInstrument

        ' check if the requested instrument already exists
        Dim name As String = BaseInstrument.BuildInstrumentName(family, number)

        If MyBase.Instruments IsNot Nothing AndAlso MyBase.Instruments.Exists(name) AndAlso Not forceMany Then

            MyBase.Instruments.SelectFirstInstrument(name)
            MyBase.MessageQueue.Enqueue("Instrument {0} selected", MyBase.ActiveInstrument.UniqueKey)

        Else

            MyBase.MessageQueue.Enqueue("Adding instrument {0}", name)

            ' select the control mode.
            instrument = New isr.Kte.MessageBasedInstrument

            ' reset instrument state.
            instrument.ResetKnownState()
            If instrument.LastOperationFailed Then
                MyBase.MessageQueue.Enqueue("Failed resetting known state. Failure ignored. {0}", instrument.LastStatusDetails())
            End If

            Dim timer As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
            instrument.SelectImpedanceInstrument(number)
            If instrument.LastOperationSuccess Then

                MyBase.MessageQueue.Enqueue("Instrument selected in {0:0.00} ms", 1000 * timer.ElapsedTicks / Stopwatch.Frequency)

                ' check if in parallel mode
                If Me.ParallelImpedanceEnabled Then

                    ' add instrument to the collection
                    MyBase.Instruments.Add(instrument)

                Else

                    ' remove the last instrument and add a new one.
                    MyBase.Instruments.Clear()
                    MyBase.Instruments.Add(instrument)

                End If

            Else
                MyBase.MessageQueue.Enqueue("Failed selecting instrument; {0}.", instrument.LastStatusDetails())
                Return False
            End If
        End If
        Return True

    End Function

#End Region

#Region " HOST MANAGEMENT "

    Private Shared _hostProcess As Process

    ''' <summary>
    ''' Close the host process.
    ''' </summary>
    Public Shared Function CloseHostProcess() As Boolean

        If Not isr.Kte.Kxci.SafeNativeMethods.UsingDevices Then
            Return True
        End If

        If MessageBasedStation._hostProcess Is Nothing Then
            ' done if not opened.
            Return True
        End If

        ' check if process is hung.
        If Not MessageBasedStation._hostProcess.HasExited Then

            ' if so, kill the process. 
            ' Needs to close window otherwise the window remains open.
            Dim isProcessHasUserInterface As Boolean = True
            If isProcessHasUserInterface Then
                MessageBasedStation._hostProcess.CloseMainWindow()
            Else
                MessageBasedStation._hostProcess.Kill()
            End If

        End If

        ' close and dispose
        MessageBasedStation._hostProcess.Close()
        MessageBasedStation._hostProcess.Dispose()

        Return True

    End Function

    ''' <summary>
    ''' Gets or sets the KXCI host process program file path name.
    ''' </summary>
    Public Shared Property HostProcessFilePathName() As String

    ''' <summary>
    ''' Gets or sets the arguments for running the host process.
    ''' </summary>
    Public Shared Property HostProcessArguments() As String

    ''' <summary>
    ''' Returns true if the host process started.
    ''' </summary>
    Public Shared Function HasHostProcessStarted(ByVal name As String) As Boolean

        If isr.Kte.Kxci.SafeNativeMethods.UsingDevices Then
            Return Process.GetProcessesByName(name).Count > 0
        Else
            Return True
        End If

    End Function

    ''' <summary>
    ''' Returns true if the host process started.
    ''' </summary>
    Public Shared Function HasHostProcessStarted() As Boolean
        Return HasHostProcessStarted("KXCI")
    End Function

    ''' <summary>
    ''' Returns true if the host process started.
    ''' </summary>
    ''' <param name="timeout">Specifies the time out for detecting if the host process is responding</param>
    ''' <param name="pollInterval">Specifies the poll time between testing,</param>
    Public Shared Function HasHostProcessStarted(ByVal timeout As Integer, ByVal pollInterval As Integer) As Boolean

        Dim timedOut As Boolean
        Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(timeout))
        Do Until HasHostProcessStarted() Or timedOut
            Threading.Thread.Sleep(pollInterval)
            timedOut = DateTime.Now > endTime
        Loop
        Return Not timedOut

    End Function

    ''' <summary>
    ''' Starts the host process and retuns control after the process start and is responding.
    ''' </summary>
    ''' <param name="timeout">Specifies the time out for detecting if the host process is responding</param>
    ''' <param name="pollInterval">Specifies the poll time between testing,</param>
    Private Shared Function _isHostProcessResponding(ByVal timeout As Integer, ByVal pollInterval As Integer) As Boolean

        If Not Kxci.SafeNativeMethods.UsingDevices Then
            Return True
        End If

        ' wait till process is responding.
        Dim timedOut As Boolean
        Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(timeout))
        Do Until MessageBasedStation._hostProcess.Responding Or timedOut
            Threading.Thread.Sleep(pollInterval)
            If Not MessageBasedStation._hostProcess.Responding Then
                timedOut = DateTime.Now > endTime
            End If
        Loop
        MessageBasedStation._hostProcess.WaitForInputIdle(timeout)
        Return Not timedOut

    End Function

    ''' <summary>
    ''' Opens the specified process for receiving inputs.
    ''' </summary>
    ''' <param name="timeout">Specifies the time out for detecting if the host process is responding</param>
    ''' <param name="pollInterval">Specifies the poll time between testing,</param>
    Public Shared Function OpenHostProcess(ByVal timeout As Integer, ByVal pollInterval As Integer) As Boolean

        Dim startHostThread As Threading.Thread

        ' create the machine thread.
        startHostThread = New Threading.Thread(New Threading.ThreadStart(AddressOf MessageBasedStation._startHostProcess))

        ' start the host process. 
        startHostThread.Start()

        ' blocks the current thread until the process thread terminates.
        startHostThread.Join(timeout)

        ' wait till the process start responding.
        If MessageBasedStation._isHostProcessResponding(timeout, pollInterval) Then
            ' wait till the process is seen as an active process
            Return HasHostProcessStarted(timeout, pollInterval)
        Else
            Return False
        End If

    End Function

    ''' <summary>
    ''' Select a host process if exists and returns true. Otherwise, return false.
    ''' </summary>
    Public Shared Function SelectHostProcess() As Boolean
        If Not Kxci.SafeNativeMethods.UsingDevices Then
            Return True
        End If
        Dim processes As Process() = Process.GetProcessesByName("KXCI")
        If processes.Count > 0 Then
            MessageBasedStation._hostProcess = processes(0)
            Return True
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' Start ths process for receiving inputs.
    ''' </summary>
    Private Shared Function _startHostProcess() As Boolean

        If SelectHostProcess() Then
            Return True
        End If

        If String.IsNullOrWhiteSpace(MessageBasedStation._HostProcessFilePathName) Then
            Throw New isr.Kte.BaseException("File path name is required for opening the process.")
        End If

        ' instantiate a new process.
        MessageBasedStation._hostProcess = New Process()

        Dim processInfo As ProcessStartInfo
        If String.IsNullOrWhiteSpace(MessageBasedStation._HostProcessArguments) Then
            processInfo = New ProcessStartInfo(MessageBasedStation._HostProcessFilePathName)
        Else
            processInfo = New ProcessStartInfo(MessageBasedStation._HostProcessFilePathName, MessageBasedStation._HostProcessArguments)
        End If

        'If Not String.IsNullOrWhiteSpace(Me._workingFolderPathName) Then
        '  processInfo.WorkingDirectory = Me._workingFolderPathName
        'End If

        'processInfo.UseShellExecute = False
        'processInfo.RedirectStandardOutput = True
        'processInfo.RedirectStandardError = True
        'processInfo.RedirectStandardInput = True

        ' open minimized.
        processInfo.WindowStyle = ProcessWindowStyle.Minimized

        ' execute the process
        MessageBasedStation._hostProcess.StartInfo = processInfo

        ' start and return true if started.
        Return MessageBasedStation._hostProcess.Start()

    End Function

#End Region

End Class

