﻿Imports System.Runtime.InteropServices
<Module: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Kte.Kxci")> 

Namespace Kxci

    ''' <summary>
    ''' Implements safe application programming interface calls using 
    ''' the Keithley Test Environment External Control Interface library.
    ''' This class suppresses stack walks for unmanaged code permission.
    ''' This class is for methods that are safe for anyone to call. Callers of these methods are 
    ''' not required to do a full security review to ensure that the usage is secure because 
    ''' the methods are harmless for any caller.
    ''' </summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/07/09" by="David" revision="1.0.3384.x">
    ''' Created
    ''' </history> 
    <AttributeUsage(AttributeTargets.Class Or AttributeTargets.Method Or AttributeTargets.Interface)>
    Public NotInheritable Class SafeNativeMethods

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Prevents construction of this class.</summary>
        Private Sub New()
            ' defaults to enabled.
            SafeNativeMethods._parallelEnabled = True
        End Sub

#End Region

#Region " LIBRARY IMPORTS "

        ''' <summary>
        ''' Closes an open connection.
        ''' </summary>
        <DllImport("KXCIclient.dll", EntryPoint:="CloseKXCIConnection", SetLastError:=True)>
        Private Shared Function CloseKXCIConnection() As Integer
        End Function

        ''' <summary>
        ''' Opens an ethernet connection to the Keithley External Control Interface.
        ''' </summary>
        ''' <param name="ipAddress">IP address in sting format "nnn.nn.nn.nn" (that is, 129.22.35.17).
        ''' Use "127.0.0.1" to connect to the local host.</param>
        ''' <param name="portNumber">IP Port assigned in KXCI tab of KCON, e.g., 1225.</param>
        ''' <param name="socketError">Socket <see cref="Net.Sockets.SocketError">error</see> as returned by WSAGetLastError()</param>
        ''' <returns>1 if success.</returns>
        <DllImport("KXCIclient.dll", EntryPoint:="OpenKXCIConnection", SetLastError:=True,
               BestFitMapping:=False, CharSet:=CharSet.Ansi,
               ThrowOnUnmappableChar:=True)>
        Private Shared Function OpenKXCIConnection(<MarshalAs(UnmanagedType.LPStr)> ByVal ipAddress As String,
                                                   ByVal portNumber As Integer,
                                                   ByRef socketError As Integer) As Integer
        End Function

        ''' <summary>
        ''' Synchronously sends a commnad and receives a reply.
        ''' </summary>
        ''' <param name="command">KXCI command string, that is, "DE;CH1;CH2".</param>
        ''' <param name="reply">Data returned by command, if any. If input command results in 
        ''' data to be returned, it is place here.</param>
        ''' <param name="socketError">Socket <see cref="Net.Sockets.SocketError">error</see> as returned by WSAGetLastError()</param>
        ''' <returns>1 if success.</returns>
        <DllImport("KXCIclient.dll", EntryPoint:="SendKXCICommand", SetLastError:=True,
               BestFitMapping:=False, CharSet:=CharSet.Ansi,
               ThrowOnUnmappableChar:=True)>
        Private Shared Function SendKXCICommand(<MarshalAs(UnmanagedType.LPStr)> ByVal command As String,
                                                <MarshalAs(UnmanagedType.VBByRefStr)> ByRef reply As String,
                                                ByRef socketError As Integer) As Integer
        End Function

        ''' <summary>
        ''' Reads the instrument status byte.
        ''' </summary>
        ''' <param name="socketError"></param>
        <DllImport("KXCIclient.dll", EntryPoint:="GetKXCISpollByte", SetLastError:=True)>
        Private Shared Function GetKXCISpollByte(ByRef value As UShort,
                                                 ByRef socketError As Integer) As Integer
        End Function

#End Region

#Region " BASE FUNCTIONS "

        ''' <summary>
        ''' Gets the <see cref="ComponentModel.DescriptionAttribute"/> of an <see cref="[Enum]"/> type value.
        ''' </summary>
        ''' <param name="value">The <see cref="[Enum]"/> type value.</param>
        ''' <returns>A string containing the text of the <see cref="ComponentModel.DescriptionAttribute"/>.</returns>
        Public Shared Function GetDescription(ByVal value As [Enum]) As String

            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If

            Dim description As String = value.ToString()
            Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(description)
            Dim attributes As ComponentModel.DescriptionAttribute() =
                CType(fieldInfo.GetCustomAttributes(GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute())

            If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                description = attributes(0).Description
            End If
            Return description

        End Function

        ''' <summary>
        ''' Returns a socket error details
        ''' </summary>
        Public Shared Function SocketStatusDetails(ByVal status As Integer) As String
            If status = 0 Then
                Return ""
            ElseIf [Enum].IsDefined(GetType(Net.Sockets.SocketError), status) Then
                Return String.Format(Globalization.CultureInfo.CurrentCulture, "Socket error #{0}; {1}",
                                     status, GetDescription(CType(status, Net.Sockets.SocketError)))
            Else
                Return String.Format(Globalization.CultureInfo.CurrentCulture, "Unknown socket error #{0}", status)
            End If
        End Function

        ''' <summary>
        ''' Returns true if last operation succeeded.
        ''' This is signified by the <see cref="LastOperationStatus">last status</see>
        ''' greater than or equal <see cref="OperationStatus.Success">okay</see>
        ''' and <see cref="LastSocketStatus">socket error</see> equals
        ''' <see cref="Net.Sockets.SocketError.Success">success</see>
        ''' </summary>
        Public Shared ReadOnly Property LastOperationSuccess() As Boolean
            Get
                Return SafeNativeMethods.LastOperationStatus >= OperationStatus.Success AndAlso
                       SafeNativeMethods._LastSocketStatus = Net.Sockets.SocketError.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if last operation failed.
        ''' This is signified by the <see cref="LastOperationStatus">last status</see>
        ''' smaller than <see cref="OperationStatus.Success">okay</see> (negative)
        ''' or <see cref="LastSocketStatus">socket status</see> not 
        ''' equal <see cref="Net.Sockets.SocketError.Success">success</see>
        ''' </summary>
        Public Shared ReadOnly Property LastOperationFailed() As Boolean
            Get
                Return Not SafeNativeMethods.LastOperationSuccess
            End Get
        End Property

        ''' <summary>
        ''' Returns a status details
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Public Shared Function OperationStatusDetails(ByVal status As Integer) As String
            Try
                If [Enum].IsDefined(GetType(OperationStatus), status) Then
                    Return String.Format(Globalization.CultureInfo.CurrentCulture, "Opertation status #{0}; {1}",
                                         status, GetDescription(CType(status, OperationStatus)))
                Else
                    Return String.Format(Globalization.CultureInfo.CurrentCulture, "Opertation status #{0}", status)
                End If
            Catch
                Return String.Format(Globalization.CultureInfo.CurrentCulture, "Opertation status #{0}", status)
            End Try
        End Function

        ''' <summary>
        ''' Returns overall status details
        ''' </summary>
        Public Shared Function LastStatusDetails() As String
            Dim details As New System.Text.StringBuilder
            Dim contents As String = OperationStatusDetails(SafeNativeMethods._LastOperationStatus)
            If Not String.IsNullOrWhiteSpace(contents) Then
                details.Append(contents)
            End If
            contents = SocketStatusDetails(SafeNativeMethods._LastSocketStatus)
            If Not String.IsNullOrWhiteSpace(contents) Then
                If details.Length > 0 Then
                    details.Append("; ")
                End If
                details.Append(contents)
            End If
            If SafeNativeMethods._lastQueryResult.ErrorOccurred Then
                If details.Length > 0 Then
                    details.Append("; ")
                End If
                details.Append(SafeNativeMethods._lastQueryResult.ErrorDetails)
            End If
            Return details.ToString
        End Function

        ''' <summary>
        ''' Returns false as KXCI does not support getter functions!
        ''' </summary>
        Public Shared ReadOnly Property SupportsGetters() As Boolean
            Get
                Return False
            End Get
        End Property

        ''' <summary>
        ''' Gets the last Socket Status or Error.. Allows setting the operation status in case of 
        ''' exceptions or for clearing in case of non-socket operations.
        ''' </summary>
        Public Shared Property LastSocketStatus() As Integer

        ''' <summary>
        ''' Gets the last operation status. Allows setting the operation status in case of 
        ''' exceptions or for clearing in case on non-KXCI operations.
        ''' </summary>
        Public Shared Property LastOperationStatus() As Integer

        Private Shared _lastQueryResult As QueryResultInfo
        ''' <summary>
        ''' Returns the last query result.
        ''' </summary>
        Public Shared ReadOnly Property LastQueryResult() As QueryResultInfo
            Get
                Return SafeNativeMethods._lastQueryResult
            End Get
        End Property

        ''' <summary>
        ''' Sends the specified command and returns a single value.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="single")>
        Public Shared Function QuerySingle(ByVal format As String, ByVal ParamArray args() As Object) As Single
            Return CSng(QueryDouble(format, args))
        End Function

        ''' <summary>
        ''' Sends the specified command and returns a Double value.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="Double")>
        Public Shared Function QueryDouble(ByVal format As String, ByVal ParamArray args() As Object) As Double

            Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(format, args))
            If result.HasReturnValue Then
                Dim value As Double = 0
                If Double.TryParse(result.Reading, Globalization.NumberStyles.AllowExponent,
                                    Globalization.CultureInfo.InvariantCulture, value) Then
                    Return value
                Else
                    SafeNativeMethods._lastQueryResult = New QueryResultInfo(OperationStatus.ErrorAvailable, result.QueryCommand, "Failed parsing returned value of '{0}' after sending '{1}'. {2}.", result.Reading, result.QueryCommand, SafeNativeMethods.LastStatusDetails())
                    SafeNativeMethods._LastOperationStatus = SafeNativeMethods._lastQueryResult.ErrorNumber
                End If
            Else
                SafeNativeMethods._lastQueryResult = New QueryResultInfo(OperationStatus.ErrorAvailable, result.QueryCommand, "Command error. Received '{0}' after sending '{1}'. {2}.", result.Reading, result.QueryCommand, SafeNativeMethods.LastStatusDetails())
                SafeNativeMethods._LastOperationStatus = SafeNativeMethods._lastQueryResult.ErrorNumber
            End If

        End Function

        ''' <summary>
        ''' Sends the specified command and returns an integer value.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="integer")>
        Public Shared Function QueryInteger(ByVal format As String, ByVal ParamArray args() As Object) As Integer

            Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(format, args))
            If result.HasReturnValue Then
                Dim value As Integer = 0
                If Integer.TryParse(result.Reading, Globalization.NumberStyles.AllowExponent,
                                    Globalization.CultureInfo.InvariantCulture, value) Then
                    Return value
                Else
                    SafeNativeMethods._lastQueryResult = New QueryResultInfo(OperationStatus.ErrorAvailable, result.QueryCommand, "Failed parsing returned value of '{0}' after sending '{1}'. {2}.", result.Reading, result.QueryCommand, SafeNativeMethods.LastStatusDetails())
                    SafeNativeMethods._LastOperationStatus = SafeNativeMethods._lastQueryResult.ErrorNumber
                End If
            Else
                SafeNativeMethods._lastQueryResult = New QueryResultInfo(OperationStatus.ErrorAvailable, result.QueryCommand, "Command error. Received '{0}' after sending '{1}'. {2}.", result.Reading, result.QueryCommand, SafeNativeMethods.LastStatusDetails())
                SafeNativeMethods._LastOperationStatus = SafeNativeMethods._lastQueryResult.ErrorNumber
            End If

        End Function

        ''' <summary>
        ''' Sends the specified command and returns a reply.
        ''' </summary>
        Public Shared Function SendReceive(ByVal dataLength As Integer,
                      ByVal format As String, ByVal ParamArray args() As Object) As Kxci.QueryResultInfo
            Return SafeNativeMethods.SendReceive(dataLength, String.Format(Globalization.CultureInfo.InvariantCulture, format, args))
        End Function

        ''' <summary>
        ''' Sends the specified command and returns a QueryResultInfo structure.
        ''' </summary>
        Public Shared Function SendReceive(ByVal format As String, ByVal ParamArray args() As Object) As Kxci.QueryResultInfo

            Return SafeNativeMethods.SendReceive(32, String.Format(Globalization.CultureInfo.InvariantCulture, format, args))

        End Function

        Private Shared queryCommandLocker As New Object
        ''' <summary>
        ''' Sends the specified command and returns a QueryResultInfo structure.
        ''' </summary>
        ''' <param name="value">Specifies the output command.</param>
        Public Shared Function SendReceive(ByVal dataLength As Integer, ByVal value As String) As Kxci.QueryResultInfo

            SyncLock queryCommandLocker
                Dim input As String = "".PadRight(dataLength)
                SafeNativeMethods._LastOperationStatus = OperationStatus.Success
                SafeNativeMethods._LastSocketStatus = Net.Sockets.SocketError.Success
                SafeNativeMethods._lastQueryResult = New QueryResultInfo("", "")
                SafeNativeMethods._LastOperationStatus = SafeNativeMethods.SendKXCICommand(value, input, SafeNativeMethods._LastSocketStatus) - 1
                If SafeNativeMethods.LastOperationSuccess Then
                    ' from old send receive.
                    ' input = input.Trim().Trim(C Char(Nothing))
                    input = input.Trim().TrimEnd(CChar(Nothing)).TrimEnd(ControlChars.CrLf.ToCharArray).TrimEnd(","c)
                    If String.IsNullOrWhiteSpace(input) Then
                        SafeNativeMethods._LastOperationStatus = OperationStatus.ErrorAvailable
                        SafeNativeMethods._lastQueryResult = New QueryResultInfo(OperationStatus.ErrorAvailable, value, "Command error. Received 'NULL' after sending '{1}'. {2}.", input, value, SafeNativeMethods.LastStatusDetails())
                    Else
                        SafeNativeMethods._lastQueryResult = New QueryResultInfo(value, input)
                        If SafeNativeMethods._lastQueryResult.ErrorOccurred Then
                            SafeNativeMethods._LastOperationStatus = SafeNativeMethods._lastQueryResult.ErrorNumber
                        End If
                    End If
                Else
                    SafeNativeMethods._lastQueryResult = New QueryResultInfo(OperationStatus.ErrorAvailable, value, "Query Command '{0}' failed. {1}", value, SafeNativeMethods.LastStatusDetails())
                    SafeNativeMethods._LastOperationStatus = SafeNativeMethods._lastQueryResult.ErrorNumber
                End If
                Return SafeNativeMethods._lastQueryResult
            End SyncLock

        End Function

        Private Shared _statusByte As UShort
        ''' <summary>
        ''' Gets the last status byte,
        ''' </summary>
        Public Shared ReadOnly Property LastStatusByte() As Integer
            Get
                Return SafeNativeMethods._statusByte
            End Get
        End Property

        ''' <summary>
        ''' Returns the KXCI Status Byte. 
        ''' </summary>
        Public Shared Function StatusByteGetter() As Integer

            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._LastOperationStatus = OperationStatus.Success
                SafeNativeMethods._LastSocketStatus = Net.Sockets.SocketError.Success
                SafeNativeMethods._LastOperationStatus = SafeNativeMethods.GetKXCISpollByte(SafeNativeMethods._statusByte, SafeNativeMethods._LastSocketStatus) - 1
                If SafeNativeMethods.LastOperationFailed Then
                    Throw New isr.Kte.FunctionCallException("Failed getting status byte. {0}", SafeNativeMethods.LastStatusDetails())
                End If
            End If
            Return SafeNativeMethods._statusByte

        End Function

        ''' <summary>
        ''' Set the KXCI Status Byte for emulation.
        ''' </summary>
        Public Shared Function StatusByteSetter(ByVal value As Integer) As Boolean
            SafeNativeMethods._statusByte = CUShort(value)
        End Function

        ''' <summary>
        ''' Gets or sets the condition for using devices.
        ''' </summary>
        ''' <remarks>
        ''' When true, the application connects to actual devices.  Set to false in case
        ''' testing the application in the absence of devices.
        ''' </remarks>
        Public Shared Property UsingDevices() As Boolean

#End Region

#Region " CVU FUNCTIONS: COMPENSATION "

        Private Shared _compensation As String = "0,0,0"
        Public Shared Function LoadCompensationEnabled(ByVal value As String) As Boolean
            If value Is Nothing Then
                Return False
            Else
                Dim values As String() = value.Split(","c)
                If values.Count > 2 Then
                    Return values(2) = "1"
                Else
                    Return False
                End If
            End If
        End Function
        Public Shared Function UpdateLoadCompensation(ByVal compensation As String, ByVal value As Boolean) As String
            If compensation Is Nothing Then
                Return ""
            Else
                Dim values As String() = compensation.Split(","c)
                If values.Count > 2 Then
                    values(2) = CStr(IIf(value, 1, 0))
                    Return String.Join(","c, values)
                Else
                    Return ""
                End If
            End If
        End Function
        Public Shared Function OpenCompensationEnabled(ByVal value As String) As Boolean
            If value Is Nothing Then
                Return False
            Else
                Dim values As String() = value.Split(","c)
                If values.Count > 0 Then
                    Return values(0) = "1"
                Else
                    Return False
                End If
            End If
        End Function
        Public Shared Function UpdateOpenCompensation(ByVal compensation As String, ByVal value As Boolean) As String
            If compensation Is Nothing Then
                Return ""
            Else
                Dim values As String() = compensation.Split(","c)
                If values.Count > 1 Then
                    values(0) = CStr(IIf(value, 1, 0))
                    Return String.Join(","c, values)
                Else
                    Return ""
                End If
            End If
        End Function
        Public Shared Function ShortCompensationEnabled(ByVal value As String) As Boolean

            If value Is Nothing Then
                Return False
            Else
                Dim values As String() = value.Split(","c)
                If values.Count > 1 Then
                    Return values(1) = "1"
                Else
                    Return False
                End If
            End If
        End Function
        Public Shared Function UpdateShortCompensation(ByVal compensation As String, ByVal value As Boolean) As String
            If compensation Is Nothing Then
                Return ""
            Else
                Dim values As String() = compensation.Split(","c)
                If values.Count > 1 Then
                    values(1) = CStr(IIf(value, 1, 0))
                    Return String.Join(","c, values)
                Else
                    Return ""
                End If
            End If
        End Function
        Public Shared Function UpdateCompensation(ByVal loadCompensation As Boolean, ByVal openCompensation As Boolean, ByVal shortCompensation As Boolean) As String
            UpdateLoadCompensation(SafeNativeMethods._compensation, loadCompensation)
            UpdateOpenCompensation(SafeNativeMethods._compensation, openCompensation)
            Return UpdateShortCompensation(SafeNativeMethods._compensation, shortCompensation)
        End Function

        ''' <summary>
        ''' Gets the compensation.
        ''' </summary>
        Public Shared Function CompensationGetter(ByVal emulatedValue As String) As String
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(32, ":CVU:CORRECT?"))
                    If result.HasReturnValue Then
                        SafeNativeMethods._compensation = result.Reading
                    Else
                        SafeNativeMethods._compensation = emulatedValue
                    End If
                Else
                    SafeNativeMethods._compensation = emulatedValue
                End If
                If String.IsNullOrWhiteSpace(SafeNativeMethods._compensation) Then
                    SafeNativeMethods._LastOperationStatus = OperationStatus.ErrorAvailable
                End If
            Else
                SafeNativeMethods._compensation = emulatedValue
            End If
            Return SafeNativeMethods._compensation
        End Function

        ''' <summary>
        ''' Sets the compensation.
        ''' </summary>
        ''' <remarks>
        ''' Enables/disables open, short, and load correction for the specified CVU card.
        ''' The command is in the form: open[0/1],short[0/1],load[0/1]
        ''' </remarks>
        Public Shared Function CompensationSetter(ByVal value As String) As Boolean
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            SafeNativeMethods._compensation = value
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(32, ":CVU:CORRECT {0}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

#End Region

#Region " CVU FUNCTIONS: MEASUREMENT SPEED "

        Private Shared _measurementSpeed As String = "0,1,1,1.002"
        Public Shared Function MeasurementSpeed(ByVal value As String) As Integer
            If value Is Nothing Then
                Return 0
            Else
                Dim values As String() = value.Split(","c)
                If values.Count > 0 Then
                    Return CInt(values(0))
                Else
                    Return 0
                End If
            End If
        End Function
        Public Shared Function UpdateMeasurementSpeed(ByVal speed As String, ByVal value As Integer) As String
            If speed Is Nothing Then
                Return ""
            Else
                Dim values As String() = speed.Split(","c)
                If values.Length > 0 Then
                    values(0) = CStr(value)
                    Return String.Join(","c, values)
                Else
                    Return ""
                End If
            End If
        End Function
        Public Shared Function DelayFactor(ByVal value As String) As Single
            If value Is Nothing Then
                Return 0
            Else
                Dim values As String() = value.Split(","c)
                If values.Count > 1 Then
                    Return CSng(values(1))
                Else
                    Return 0
                End If
            End If
        End Function
        Public Shared Function UpdateDelayFactor(ByVal speed As String, ByVal value As Single) As String
            If speed Is Nothing Then
                Return ""
            Else
                Dim values As String() = speed.Split(","c)
                If values.Length > 1 Then
                    values(1) = String.Format(Globalization.CultureInfo.InvariantCulture, "{0:0.####}", value)
                    Return String.Join(","c, values)
                Else
                    Return ""
                End If
            End If
        End Function
        Public Shared Function FilterFactor(ByVal value As String) As Single
            If value Is Nothing Then
                Return 0
            Else
                Dim values As String() = value.Split(","c)
                If values.Count > 2 Then
                    Return CSng(values(2))
                Else
                    Return 0
                End If
            End If
        End Function
        Public Shared Function UpdateFilterFactor(ByVal speed As String, ByVal value As Single) As String
            If speed Is Nothing Then
                Return ""
            Else
                Dim values As String() = speed.Split(","c)
                If values.Length > 2 Then
                    values(2) = String.Format(Globalization.CultureInfo.InvariantCulture, "{0:0.####}", value)
                    Return String.Join(","c, values)
                Else
                    Return ""
                End If
            End If
        End Function
        Public Shared Function Aperture(ByVal value As String) As Single
            If value Is Nothing Then
                Return 0
            Else
                Dim values As String() = value.Split(","c)
                If values.Count > 3 Then
                    Return CSng(values(3))
                Else
                    Return 0
                End If
            End If
        End Function
        Public Shared Function UpdateAperture(ByVal speed As String, ByVal value As Single) As String
            If speed Is Nothing Then
                Throw New ArgumentNullException("speed")
            End If
            Dim values As String() = speed.Split(","c)
            values(3) = String.Format(Globalization.CultureInfo.InvariantCulture, "{0:0.####}", value)
            Return String.Join(","c, values)
        End Function
        Public Shared Function UpdateMeasurementSpeed(ByVal speed As Integer, ByVal delay As Single,
                                                      ByVal filter As Single, ByVal aperture As Single) As String
            SafeNativeMethods._measurementSpeed = UpdateMeasurementSpeed(SafeNativeMethods._measurementSpeed, speed)
            SafeNativeMethods._measurementSpeed = UpdateDelayFactor(SafeNativeMethods._measurementSpeed, delay)
            SafeNativeMethods._measurementSpeed = UpdateFilterFactor(SafeNativeMethods._measurementSpeed, filter)
            SafeNativeMethods._measurementSpeed = UpdateAperture(SafeNativeMethods._measurementSpeed, aperture)
            Return SafeNativeMethods._measurementSpeed
        End Function

        ''' <summary>
        ''' Gets the speed.
        ''' </summary>
        Public Shared Function MeasurementSpeedGetter(ByVal emulatedValue As String) As String
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(32, ":CVU:SPEED?"))
                    If result.HasReturnValue Then
                        SafeNativeMethods._measurementSpeed = result.Reading
                    Else
                        SafeNativeMethods._measurementSpeed = emulatedValue
                    End If
                Else
                    SafeNativeMethods._measurementSpeed = emulatedValue
                End If
                If String.IsNullOrWhiteSpace(SafeNativeMethods._measurementSpeed) Then
                    SafeNativeMethods._LastOperationStatus = OperationStatus.ErrorAvailable
                End If
            Else
                SafeNativeMethods._measurementSpeed = emulatedValue
            End If
            Return SafeNativeMethods._measurementSpeed
        End Function

        ''' <summary>
        ''' Sets the speed.
        ''' </summary>
        ''' <param name="value ">Specifies the measurement speed.</param>
        ''' <remarks>
        ''' Enables/disables open, short, and load correction for the specified CVU card.
        ''' The command is in the form: speed[0,1,2,3],delay[0-100],filter[0-100],aperture[0.006-10.002]
        ''' Speed could be set to Fast (0), Normal (1), Quiet (2) or Custom (3). 
        ''' With custom speed the delay (0-100), filter (0-100) and aperture (0.006-10.002) take effect.
        ''' Aperture is specified in power line cycles between 0.006 and 10.002.
        ''' Delay factor is unitless between 0 and 100.
        ''' Filter factor is unitless between 0 and 100.
        ''' </remarks>
        Public Shared Function MeasurementSpeedSetter(ByVal value As String) As Boolean
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            SafeNativeMethods._measurementSpeed = value
            If value IsNot Nothing AndAlso Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:SPEED {0}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

#End Region

#Region " CVU FUNCTIONS: MEASUREMENT "

        ''' <summary>
        ''' Parses the impedance. Sets the <see cref="LastOperationStatus">last operation status</see>
        ''' if failed.
        ''' </summary>
        ''' <remarks>
        ''' Impendace values are in the form "X;Y[,]"
        ''' </remarks>
        Public Shared Function ParseImpedance(ByVal value As String) As Impedance
            SafeNativeMethods._measuredImpedance = value
            If String.IsNullOrWhiteSpace(value) Then
                SafeNativeMethods._LastOperationStatus = OperationStatus.ErrorAvailable
                Return Impedance.Empty()
            Else
                If value.Contains(",") Then
                    value = value.Split(","c)(0)
                End If
                Dim values() As String = value.Split(";"c)
                If values.Count < 2 Then
                    SafeNativeMethods._LastOperationStatus = OperationStatus.ErrorAvailable
                    Return Impedance.Empty()
                Else
                    SafeNativeMethods._primaryValue = CSng(values(0))
                    SafeNativeMethods._secondayValue = CSng(values(1))
                End If
            End If
            Return New Impedance(SafeNativeMethods._primaryValue, SafeNativeMethods._secondayValue)
        End Function

        Private Shared _measuredImpedance As String
        Private Shared _primaryValue As Single
        Private Shared _secondayValue As Single
        ''' <summary>
        ''' Makes an impedance measurement using the preset data.
        ''' Triggers and returns single Z-measurement using current CVU settings.  
        ''' When the command is complete, the single reading is available over GPIB or Ethernet.
        ''' </summary>
        ''' <remarks>
        ''' This command is not supported</remarks>
        Public Shared Function MeasureImpedance() As Impedance
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            Dim value As Impedance = New Impedance(SafeNativeMethods._primaryValue, SafeNativeMethods._secondayValue)
            If Kxci.SafeNativeMethods.UsingDevices Then
                isr.Kte.Kxci.SafeNativeMethods.SystemModeEnabledSetter(False)
                'Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:SPEED {0}", value))
                'Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:MEASZ?"))
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:MEASZ?"))
                If result.HasReturnValue Then
                    SafeNativeMethods._measuredImpedance = result.Reading
                    isr.Kte.Kxci.SafeNativeMethods.SystemModeEnabledSetter(True)
                    value = ParseImpedance(SafeNativeMethods._measuredImpedance)
                Else
                    SafeNativeMethods._measuredImpedance = ""
                End If
            End If
            Return value
        End Function

        Private Shared _operationTimedOut As Boolean
        ''' <summary>
        ''' Gets an indication if the last operation timed out.
        ''' </summary>
        Public Shared ReadOnly Property OperationTimedOut() As Boolean
            Get
                Return SafeNativeMethods._operationTimedOut
            End Get
        End Property

        ''' <summary>
        ''' Waits for operations to complete on the specified channel.
        ''' </summary>
        ''' <param name="pollDelay"></param>
        Public Shared Function AwaitCompletion(ByVal channel As Integer, ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean
            Dim endTime As DateTime = DateTime.Now.Add(TimeSpan.FromMilliseconds(timeout))
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            SafeNativeMethods._operationTimedOut = False
            Do Until QueryTestCompleted(channel) OrElse SafeNativeMethods.LastOperationFailed OrElse SafeNativeMethods._operationTimedOut
                SafeNativeMethods._operationTimedOut = DateTime.Now > endTime
                Threading.Thread.Sleep(pollDelay)
            Loop
            If SafeNativeMethods.LastOperationSuccess AndAlso SafeNativeMethods._operationTimedOut Then
                SafeNativeMethods._LastOperationStatus = OperationStatus.ErrorAvailable
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        ''' <summary>
        ''' Returns true if the last test completed.
        ''' </summary>
        Public Shared Function QueryTestCompleted(ByVal channel As Integer) As Boolean
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If SafeNativeMethods.UsingDevices Then
                Return SafeNativeMethods.QueryInteger(":CVU:TEST:COMPLETE? {0}", channel) = 1
            End If
            Return True
        End Function

#End Region

#Region " CVU FUNCTIONS "

        Private Shared _alternatingVoltage As Single
        ''' <summary>
        ''' Returns the AC source Voltage.
        ''' </summary>
        Public Shared Function AlternatingVoltageGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._alternatingVoltage = SafeNativeMethods.QuerySingle(":CVU:ACV?")
                Else
                    SafeNativeMethods._alternatingVoltage = emulatedValue
                End If
            Else
                SafeNativeMethods._alternatingVoltage = emulatedValue
            End If
            Return SafeNativeMethods._alternatingVoltage
        End Function

        ''' <summary>
        ''' Sets the AC source Voltage.
        ''' </summary>
        ''' <param name="value">Voltage level of the AC drive between 10mV to 100mVRMS</param>
        Public Shared Function AlternatingVoltageSetter(ByVal value As Single) As Boolean
            SafeNativeMethods._alternatingVoltage = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:ACV {0:0.0##}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _cableLength As Single
        ''' <summary>
        ''' Returns the current cable length compensation.
        ''' </summary>
        Public Shared Function CableLengthGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._cableLength = SafeNativeMethods.QuerySingle(":CVU:LENGTH?")
                Else
                    SafeNativeMethods._cableLength = emulatedValue
                End If
            Else
                SafeNativeMethods._cableLength = emulatedValue
            End If
            Return SafeNativeMethods._cableLength
        End Function

        ''' <summary>
        ''' Sets the cable length compensation.
        ''' </summary>
        ''' <param name="value">Specifies the cable length. Must be 0, 1.5 or 3.</param>
        Public Shared Function CableLengthSetter(ByVal value As Single) As Boolean
            SafeNativeMethods._cableLength = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:LENGTH {0:0.0##}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _channelNumber As Integer
        ''' <summary>
        ''' Returns the current instrument.
        ''' </summary>
        Public Shared Function ChannelNumberGetter(ByVal emulatedValue As Integer) As Integer
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._channelNumber = SafeNativeMethods.QueryInteger(":CVU:CHANNEL?")
                Else
                    SafeNativeMethods._channelNumber = emulatedValue
                End If
            Else
                SafeNativeMethods._channelNumber = emulatedValue
            End If
            Return SafeNativeMethods._channelNumber
        End Function

        ''' <summary>
        ''' Selects an impedance instrument.
        ''' </summary>
        ''' <param name="channelNumber"></param>
        Public Shared Function ChannelNumberSetter(ByVal channelNumber As Integer) As Boolean
            SafeNativeMethods._channelNumber = channelNumber
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:CHANNEL {0}", channelNumber))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _channelCount As Integer
        ''' <summary>
        ''' Returns the number of CVU channels.
        ''' </summary>
        Public Shared Function ChannelCountGetter(ByVal emulatedValue As Integer) As Integer
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            SafeNativeMethods._LastSocketStatus = Net.Sockets.SocketError.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._channelCount = SafeNativeMethods.QueryInteger(":CVU:CHANNELS?")
            Else
                SafeNativeMethods._channelCount = emulatedValue
            End If
            Return SafeNativeMethods._channelCount
        End Function

        Private Shared _directVoltage As Single
        ''' <summary>
        ''' Returns the DC source Voltage.
        ''' </summary>
        Public Shared Function DirectVoltageGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._directVoltage = SafeNativeMethods.QuerySingle(":CVU:DCV?")
                Else
                    SafeNativeMethods._directVoltage = emulatedValue
                End If
            Else
                SafeNativeMethods._directVoltage = emulatedValue
            End If
            Return SafeNativeMethods._directVoltage
        End Function

        ''' <summary>
        ''' Sets the DC source Voltage.
        ''' </summary>
        ''' <param name="value">Specifies the direct voltage bias between -30V to +30V. </param>
        Public Shared Function DirectVoltageSetter(ByVal value As Single) As Boolean
            SafeNativeMethods._directVoltage = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:DCV {0:0.0##}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _holdTime As Single
        ''' <summary>
        ''' Returns the source hold time.
        ''' </summary>
        Public Shared Function HoldTimeGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._holdTime = SafeNativeMethods.QuerySingle(":CVU:SAMPLE:HOLDT?")
                Else
                    SafeNativeMethods._holdTime = emulatedValue
                End If
            Else
                SafeNativeMethods._holdTime = emulatedValue
            End If
            Return SafeNativeMethods._holdTime
        End Function

        ''' <summary>
        ''' Sets the hold time for a sampling mode test on the selected card.
        ''' This is only used when executing :CVU:BIAS:DCV:SAMPLE command.
        ''' frequency.
        ''' The starting voltage(s)/current(s) of a sweep may be substantially larger than the 
        ''' voltage/current increments of the sweep. Accordingly, the source settling time required 
        ''' to reach the starting voltage(s)/current(s) of a sweep may be substantially larger than 
        ''' the settling times required to increment the sweep. Setting a Hold Time delay to be applied 
        ''' only at the beginning of each sweep helps compensating for this. 
        ''' </summary>
        ''' <param name="value">Specifies the hold time between 0 and 999 seconds.</param>
        Public Shared Function HoldTimeSetter(ByVal value As Single) As Boolean
            SafeNativeMethods._holdTime = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:SAMPLE:HOLDT {0:0.0##}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _measurementModel As Integer
        ''' <summary>
        ''' Returns the current Measurement Model.
        ''' </summary>
        Public Shared Function MeasurementModelGetter(ByVal emulatedValue As Integer) As Integer
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._measurementModel = SafeNativeMethods.QueryInteger(":CVU:MODEL?")
                Else
                    SafeNativeMethods._measurementModel = emulatedValue
                End If
            Else
                SafeNativeMethods._measurementModel = emulatedValue
            End If
            Return SafeNativeMethods._measurementModel
        End Function

        ''' <summary>
        ''' Sets the Measurement Model.
        ''' </summary>
        ''' <param name="value">Specifies the measurement model.</param>
        Public Shared Function MeasurementModelSetter(ByVal value As Integer) As Boolean
            SafeNativeMethods._measurementModel = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:MODEL {0}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        ''' <summary>
        ''' Returns the Measured impedance of the current device.
        ''' </summary>
        Public Shared Function MeasuredImpedanceGetter(ByVal emulatedValue As String) As String
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(128, ":CVU:DATA:Z?"))
                If result.HasReturnValue Then
                    SafeNativeMethods._measuredImpedance = result.Reading
                Else
                    SafeNativeMethods._measuredImpedance = ""
                End If
                If String.IsNullOrWhiteSpace(SafeNativeMethods._measuredImpedance) AndAlso SafeNativeMethods._LastOperationStatus < OperationStatus.Success Then
                    SafeNativeMethods._LastOperationStatus = OperationStatus.ErrorAvailable
                End If
            Else
                SafeNativeMethods._measuredImpedance = emulatedValue
            End If
            Return SafeNativeMethods._measuredImpedance
        End Function

        Private Shared _measurementStatus As Integer
        ''' <summary>
        ''' Returns the Measurement status of the current device.
        ''' </summary>
        Public Shared Function MeasurementStatusGetter(ByVal emulatedValue As Integer) As Integer
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._measurementStatus = SafeNativeMethods.QueryInteger(":CVU:DATA:STATUS?")
            Else
                SafeNativeMethods._measurementStatus = emulatedValue
            End If
            Return SafeNativeMethods._measurementStatus
        End Function

        Private Shared _measurementTimestamp As Single
        ''' <summary>
        ''' Returns the Measurement timestamp of the current device.
        ''' </summary>
        Public Shared Function MeasurementTimestampGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._measurementTimestamp = SafeNativeMethods.QueryInteger(":CVU:DATA:TSTAMP?")
            Else
                SafeNativeMethods._measurementTimestamp = emulatedValue
            End If
            Return SafeNativeMethods._measurementTimestamp
        End Function

        ''' <summary>
        ''' Resets the device to a knowb state. 
        ''' </summary>
        Public Shared Function ResetKnownState() As Boolean

            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:RESET"))
            End If
            Return SafeNativeMethods.LastOperationSuccess

        End Function

        ''' <summary>
        ''' Selects an impedance instrument.
        ''' </summary>
        ''' <param name="channelNumber"></param>
        Public Shared Function SelectImpedanceInstrument(ByVal channelNumber As Integer) As Boolean
            Return ChannelNumberSetter(channelNumber)
        End Function

        Private Shared _samplingBias As Single
        ''' <summary>
        ''' Returns the sampling source bias.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        Public Shared Function SamplingBiasGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:BIAS:DCV:SAMPLE?"))
                    If result.HasReturnValue Then
                        SafeNativeMethods._samplingBias = CSng(result.Reading.Split(","c)(0))
                    End If
                Else
                    SafeNativeMethods._samplingBias = emulatedValue
                End If
            Else
                SafeNativeMethods._samplingBias = emulatedValue
            End If
            Return SafeNativeMethods._samplingBias
        End Function

        Private Shared _samplingCount As Integer
        ''' <summary>
        ''' Returns the sampling count.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        Public Shared Function SamplingCountGetter(ByVal emulatedValue As Integer) As Integer
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:BIAS:DCV:SAMPLE?"))
                    If result.HasReturnValue Then
                        SafeNativeMethods._samplingCount = CInt(result.Reading.Split(","c)(1))
                    End If
                Else
                    SafeNativeMethods._samplingCount = emulatedValue
                End If
            Else
                SafeNativeMethods._samplingCount = emulatedValue
            End If
            Return SafeNativeMethods._samplingCount
        End Function

        ''' <summary>
        ''' Sets the sample count and DC bias for a DC bias timed sweep.
        ''' </summary>
        ''' <param name="bias"></param>
        ''' <param name="count"></param>
        ''' <remarks>
        ''' Configures the CVU to bias a DC voltage and sample a number of Z-measurements for the CVU card. 
        ''' </remarks>
        Public Shared Function SamplingSetter(ByVal bias As Single, ByVal count As Integer) As Boolean
            SafeNativeMethods._samplingCount = count
            SafeNativeMethods._directVoltage = bias
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:BIAS:DCV:SAMPLE {0:0.0##},{1}", bias, count))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _SamplingInterval As Single
        ''' <summary>
        ''' Returns the delay between samples for the selected card.  
        ''' </summary>
        Public Shared Function SamplingIntervalGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._SamplingInterval = SafeNativeMethods.QuerySingle(":CVU:SAMPLE:INTERVAL?")
                Else
                    SafeNativeMethods._SamplingInterval = emulatedValue
                End If
            Else
                SafeNativeMethods._SamplingInterval = emulatedValue
            End If
            Return SafeNativeMethods._SamplingInterval
        End Function

        ''' <summary>
        ''' Sets the delay between samples for the selected card.  
        ''' This is only used when executing when executing the :CVU:BIAS:DCV:SAMPLE command. 
        ''' Value is in seconds. Valid values are 0 to 999 seconds.
        ''' This interval can be used to provide extra settling time before each measurement.
        ''' </summary>
        ''' <param name="value">Specifies the delay time between samples between 0 and 999 seconds.</param>
        Public Shared Function SamplingIntervalSetter(ByVal value As Single) As Boolean
            SafeNativeMethods._SamplingInterval = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:SAMPLE:INTERVAL {0:0.0##}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _senseCurrentRange As Single
        ''' <summary>
        ''' Returns the current Sense Current Range.
        ''' </summary>
        Public Shared Function SenseCurrentRangeGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._senseCurrentRange = SafeNativeMethods.QuerySingle(":CVU:ACZ:RANGE?")
                Else
                    SafeNativeMethods._senseCurrentRange = emulatedValue
                End If
            Else
                SafeNativeMethods._senseCurrentRange = emulatedValue
            End If
            Return SafeNativeMethods._senseCurrentRange
        End Function

        ''' <summary>
        ''' Sets the Sense Current Range.
        ''' </summary>
        ''' <param name="value">Specifies the range of the measured current at either
        ''' 0 (auto), 1e-6 (1uA), 30e-6 (30uA), or 1e-3 (1mA).</param>
        Public Shared Function SenseCurrentRangeSetter(ByVal value As Single) As Boolean
            SafeNativeMethods._senseCurrentRange = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:ACZ:RANGE {0:0.0#####}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _sourceFrequency As Single
        ''' <summary>
        ''' Returns the AC source frequency.
        ''' </summary>
        Public Shared Function SourceFrequencyGetter(ByVal emulatedValue As Single) As Single
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._sourceFrequency = SafeNativeMethods.QuerySingle(":CVU:FREQ?")
                Else
                    SafeNativeMethods._sourceFrequency = emulatedValue
                End If
            Else
                SafeNativeMethods._sourceFrequency = emulatedValue
            End If
            Return SafeNativeMethods._sourceFrequency
        End Function

        ''' <summary>
        ''' Sets the AC source frequency.
        ''' </summary>
        ''' <param name="value">Specifies the source drive frequency. 
        ''' Values are coerced to one of the 28 discrete frequencies or 37 discrete frequencies 
        ''' between 10KHz and 10MHz for the 4200-CVU and 4210-CVU, respectively.</param>
        Public Shared Function SourceFrequencySetter(ByVal value As Single) As Boolean
            SafeNativeMethods._sourceFrequency = value
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:FREQ {0:0.#}", value))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _SystemModeEnabled As Boolean
        ''' <summary>
        ''' Gets the status of System Mode.
        ''' </summary>
        Public Shared Function SystemModeEnabledGetter(ByVal emulatedValue As Boolean) As Boolean
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                If Kxci.SafeNativeMethods.SupportsGetters Then
                    SafeNativeMethods._SystemModeEnabled = SafeNativeMethods.QueryInteger(":CVU:MODE?") = 1
                Else
                    SafeNativeMethods._SystemModeEnabled = emulatedValue
                End If
            Else
                SafeNativeMethods._SystemModeEnabled = emulatedValue
            End If
            Return SafeNativeMethods._SystemModeEnabled
        End Function

        ''' <summary>
        ''' Sets the status of System Mode.
        ''' </summary>
        ''' <remarks>In system mode, user commands are cached and execute at system time
        ''' This is akeen to asynchronous mode.
        ''' </remarks>
        Public Shared Function SystemModeEnabledSetter(ByVal value As Boolean) As Boolean
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            SafeNativeMethods._SystemModeEnabled = value
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:MODE {0}", IIf(value, 1, 0)))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

#End Region

#Region " STATION FUNCTIONS "

        ''' <summary>
        ''' Initiates a parallel test.
        ''' </summary>
        Public Shared Function InitiateParallelTest() As Boolean
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:PARALLEL:RUN"))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        Private Shared _parallelEnabled As Boolean
        ''' <summary>
        ''' Gets the status of parallel measurments.
        ''' </summary>
        ''' <remarks>
        ''' This command fails.
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="emulatedValue")>
        Public Shared Function ParallelEnabledGetter(ByVal emulatedValue As Boolean) As Boolean
            Return SafeNativeMethods._parallelEnabled
        End Function

        ''' <summary>
        ''' Sets the status of parallel measurments.
        ''' </summary>
        Public Shared Function ParallelEnabledSetter(ByVal value As Boolean) As Boolean
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            SafeNativeMethods._parallelEnabled = value
            If Kxci.SafeNativeMethods.UsingDevices Then
                Dim result As New QueryResultInfo(SafeNativeMethods.SendReceive(":CVU:PARALLEL:ENABLE {0}", IIf(value, 1, 0)))
            End If
            Return SafeNativeMethods.LastOperationSuccess
        End Function

        ''' <summary>
        ''' Relinquishes control of the currently selected test station. 
        ''' Closes the KXCI connection.
        ''' </summary>
        Public Shared Function CloseTestStation() As Boolean

            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            If SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._LastOperationStatus = 1 - SafeNativeMethods.CloseKXCIConnection()
            End If
            Return SafeNativeMethods.LastOperationSuccess

        End Function

        ''' <summary>
        ''' Enables the first test station.  
        ''' Opens a local KXCI connection.
        ''' </summary>
        Public Shared Function OpenTestStation() As Boolean

            Return SafeNativeMethods.OpenTestStation("127.0.0.1", 1225)

        End Function

        ''' <summary>
        ''' Enables the first test station.  
        ''' Opens the KXCI connection.
        ''' </summary>
        Public Shared Function OpenTestStation(ByVal address As String, ByVal portNumber As Integer) As Boolean

            ' clear the status
            SafeNativeMethods._LastOperationStatus = OperationStatus.Success
            SafeNativeMethods._LastSocketStatus = Net.Sockets.SocketError.Success

            If SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._LastOperationStatus = SafeNativeMethods.OpenKXCIConnection(address, portNumber, SafeNativeMethods._LastSocketStatus) - 1
            End If
            Return SafeNativeMethods.LastOperationSuccess

        End Function

        ''' <summary>
        ''' Returns True if Parallel CVU is support..
        ''' </summary>
        Public Shared ReadOnly Property SupportsParallel() As Boolean
            Get
                Try
                    Return isr.Kte.Kxci.SafeNativeMethods.ChannelCountGetter(4) > 0
                Catch ex As FunctionCallException
                    SafeNativeMethods._LastOperationStatus = OperationStatus.Success
                    Return False
                End Try
            End Get
        End Property

#End Region

    End Class

    ''' <summary>
    ''' A structure for reading message based query results..
    ''' </summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="12/04/09" by="David" revision="1.0.3625.x">
    ''' Created
    ''' </history>  
    Public Structure QueryResultInfo

        Public Sub New(ByVal value As QueryResultInfo)
            Me._queryCommand = value.QueryCommand
            Me._reading = value.Reading
            Me._errorDetails = value.ErrorDetails
            Me._errorNumber = value.ErrorNumber
            Me._hasReturnValue = value.HasReturnValue
        End Sub

        ''' <summary>
        ''' A constructor reporting an error.
        ''' </summary>
        ''' <param name="errorNumber"></param>
        Public Sub New(ByVal errorNumber As Integer, ByVal queryCommand As String, ByVal format As String, ByVal ParamArray args() As Object)
            Me._queryCommand = queryCommand
            Me._reading = ""
            Me._errorDetails = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            Me._errorNumber = errorNumber
            Me._hasReturnValue = False
        End Sub

        Public Sub New(ByVal queryCommand As String, ByVal reading As String)
            Me._queryCommand = queryCommand
            Me._reading = reading
            Me.ParseReading(Me._reading)
        End Sub

        ''' <summary>
        ''' Parses the reading. Readings have the following format<para>
        ''' ACK -- when a command has not values.
        ''' </para><para>
        ''' ERROR: ..... (-xxx) - when the query command returned an error.
        ''' </para><para>
        ''' Value or message - when a query succeeded and has returned a value.</para>
        ''' </summary>
        Public Function ParseReading(ByVal value As String) As Boolean

            Me._reading = value
            Me._errorDetails = ""
            Me._errorNumber = 0
            Me._hasReturnValue = False
            If String.IsNullOrWhiteSpace(value) Then
                Return True
            End If
            Select Case True
                Case value.StartsWith("ACK", StringComparison.Ordinal)
                    Me._hasReturnValue = False
                Case value.StartsWith("ERROR:", StringComparison.Ordinal)
                    Dim startIndex As Integer = value.IndexOf("(", 0, 1, StringComparison.Ordinal) + 1
                    Dim endIndex As Integer = value.IndexOf(")", startIndex, 1, StringComparison.Ordinal)
                    Dim errorRead As String = value.Substring(startIndex, endIndex - startIndex)
                    If Not Integer.TryParse(errorRead, Me._errorNumber) Then
                        Me._errorNumber = -9999
                        value = value & ". Failed parsing error number '" & errorRead & "'"
                    End If
                Case Else
                    Me._hasReturnValue = True
            End Select
            Return True

        End Function

        Private _errorDetails As String
        ''' <summary>
        ''' Returns the query result error Details.
        ''' </summary>
        Public ReadOnly Property ErrorDetails() As String
            Get
                Return Me._errorDetails
            End Get
        End Property

        Private _errorNumber As Integer
        ''' <summary>
        ''' Returns the query result error number.
        ''' </summary>
        Public ReadOnly Property ErrorNumber() As Integer
            Get
                Return Me._errorNumber
            End Get
        End Property

        Public ReadOnly Property ErrorOccurred() As Boolean
            Get
                Return Me._errorNumber <> 0
            End Get
        End Property

        Private _hasReturnValue As Boolean
        ''' <summary>
        ''' Gets the indication that the query command returned a value.
        ''' </summary>
        Public ReadOnly Property HasReturnValue() As Boolean
            Get
                Return Me._hasReturnValue
            End Get
        End Property

        Private _reading As String
        ''' <summary>
        ''' Gets the returned value.
        ''' </summary>
        Public ReadOnly Property Reading() As String
            Get
                Return Me._reading
            End Get
        End Property

        Private _queryCommand As String
        ''' <summary>
        ''' Gets the output value.
        ''' </summary>
        Public ReadOnly Property QueryCommand() As String
            Get
                Return Me._queryCommand
            End Get
        End Property

#Region " EQUALS "

        ''' <summary>Determines if the two specified <see cref="T:Object">objects</see> 
        ''' have the same value.
        ''' </summary>
        ''' <param name="left">The left value.</param>
        ''' <param name="right">The right value.</param><returns></returns>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")>
        Public Overloads Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
            If right Is Nothing Then
                Return left Is Nothing
            ElseIf left Is Nothing Then
                Return False
            Else
                Return right.Equals(left)
            End If
        End Function

        ''' <summary>Determines if the two specified <see cref="T:QueryResultInfo">Query Result Info</see> 
        ''' have the same value.
        ''' </summary>
        ''' <param name="left">The left value.</param>
        ''' <param name="right">The right value.</param><returns></returns>
        Public Overloads Shared Function Equals(ByVal left As QueryResultInfo, ByVal right As QueryResultInfo) As Boolean
            Return left.Equals(right)
        End Function

        ''' <summary>
        ''' Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        ''' </summary>
        ''' <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" />.</param>
        ''' <returns><c>True</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>False</c>.</returns>
        ''' <exception cref="T:System.NullReferenceException">
        ''' The <paramref name="obj" /> parameter is null.
        '''   </exception>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse QueryResultInfo.Equals(Me, CType(obj, QueryResultInfo)))
        End Function

        ''' <summary>
        ''' Determines whether the specified <see cref="QueryResultInfo" /> is equal to this instance.
        ''' </summary>
        ''' <param name="value">The value.</param>
        Public Overloads Function Equals(ByVal value As QueryResultInfo) As Boolean
            Return QueryCommand.Equals(value.QueryCommand) AndAlso
                   Reading.Equals(value.Reading)
        End Function

        ''' <summary>
        ''' Returns a hash code for this instance.
        ''' </summary>
        ''' <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        Public Overrides Function GetHashCode() As Integer
            Return QueryCommand.GetHashCode Xor Reading.GetHashCode
        End Function

        ''' <summary>
        ''' Implements the operator =.
        ''' </summary>
        ''' <param name="left">The left.</param>
        ''' <param name="right">The right.</param>
        ''' <returns>The result of the operator.</returns>
        Public Overloads Shared Operator =(ByVal left As QueryResultInfo, ByVal right As QueryResultInfo) As Boolean
            Return right.Equals(left)
        End Operator

        ''' <summary>
        ''' Implements the operator &lt;&gt;.
        ''' </summary>
        ''' <param name="left">The left.</param>
        ''' <param name="right">The right.</param>
        ''' <returns>The result of the operator.</returns>
        Public Overloads Shared Operator <>(ByVal left As QueryResultInfo, ByVal right As QueryResultInfo) As Boolean
            Return Not right.Equals(left)
        End Operator

#End Region

    End Structure

End Namespace

