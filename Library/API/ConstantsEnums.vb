Imports System
Imports System.ComponentModel

#Region " TYPES "

''' <summary>
''' Enumerates the measurement speeds.
''' </summary>
Public Enum MeasurementSpeed

    ''' <summary>Fast  (KI_CVU_SPEED_FAST).</summary>
    <System.ComponentModel.Description("Fast")> Fast = 0

    ''' <summary>Normal (KI_CVU_SPEED_NORMAL).</summary>
    <System.ComponentModel.Description("Normal")> Normal = 1

    ''' <summary>Normal (KI_CVU_SPEED_QUIET).</summary>
    <System.ComponentModel.Description("Quiet")> Quiet = 2

    ''' <summary>Custom (KI_CVU_SPEED_CUSTOM).</summary>
    <System.ComponentModel.Description("Custom")> Custom = 3

End Enum

''' <summary>
''' Enumerates the operations status. LPT Lib functions return a status
''' calue. A value of 0 seems to be a pass. 
''' </summary>
Public Enum OperationStatus
    <System.ComponentModel.Description("Success")> Success = 0
    <System.ComponentModel.Description("Success - Message available")> MessageAvailable = 1
    <System.ComponentModel.Description("Failure")> ErrorAvailable = -1
End Enum

''' <summary>
''' Enumerates the base impedance measurement types.
''' </summary>
Public Enum BaseImpedanceModel

    ''' <summary>Polar: Impedance and Phase</summary>
    <System.ComponentModel.Description("Polar")> Polar
    ''' <summary>Cartesian: Resistance and Reactance</summary>
    <System.ComponentModel.Description("Cartesian")> Cartesian
    ''' <summary>Parallel: Capacitance and Conductance or Dissipation Factor</summary>
    <System.ComponentModel.Description("Parallel")> Parallel
    ''' <summary>Series: Capacitance and Resistance or Dissipation Factor</summary>
    <System.ComponentModel.Description("Series")> Series
    ''' <summary>Raw = Cartesian</summary>
    <System.ComponentModel.Description("Raw")> Raw

End Enum

''' <summary>
''' Enumerates the impedance measurement types.
''' </summary>
Public Enum ImpedanceModel

    ''' <summary>Polar: Impedance and phase (KI_CVU_TYPE_ZTH)</summary>
    <System.ComponentModel.Description("Polar")> Polar = 0
    ''' <summary>Cartesian: Resistance and Reactance (KI_CVU_TYPE_RJX)</summary>
    <System.ComponentModel.Description("Cartesian")> Cartesian = 1
    ''' <summary>Parallel: Capacitance and Conductance (KI_CVU_TYPE_CPGP)</summary>
    <System.ComponentModel.Description("Parallel")> ParallelConductance = 2
    ''' <summary>Serial: Capacitance and Resistance (KI_CVU_TYPE_CSRS)</summary>
    <System.ComponentModel.Description("Series")> SeriesResistance = 3
    ''' <summary>Parallel Dissipation: Capacitance and Dissipation Factor (KI_CVU_TYPE_CPD)</summary>
    <System.ComponentModel.Description("Parallel Dissipation")> ParallelDissipation = 4
    ''' <summary>Series Dissipation Capacitance and Dissipation Factor (KI_CVU_TYPE_CSD)</summary>
    <System.ComponentModel.Description("Series Dissipation")> SeriesDissipation = 5
    ''' <summary>Raw (KI_CVU_TYPE_RAW)</summary>
    <System.ComponentModel.Description("Raw")> Raw = 6

End Enum

''' <summary>
''' Enumerates the status information.
''' </summary>
<CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue")> 
<Flags()> Public Enum MeasurementStatuses
    <System.ComponentModel.Description("Lowest Range (1uA)")> LowestRange = &H0
    <System.ComponentModel.Description("Middle Range (30uA)")> MiddleRange = &H1
    <System.ComponentModel.Description("Highest Range (1mA)")> HighestRange = &H2
    <System.ComponentModel.Description("Measurement timeout occurred (CVU_STATUS_MEAS_TIMEOUT)")> Timeout = &H10000000
    <System.ComponentModel.Description("High Voltage overflow (CVU_STATUS_CVHV1_VOFLO)")> HighVoltageOverflow = &H2000000
    <System.ComponentModel.Description("High current overflow (CVU_STATUS_CVHI1_IOFLO)")> HighCurrentOverflow = &H1000000
    <System.ComponentModel.Description("High not locked (CVU_STATUS_CVHI1_UNLOCK)")> HighNotLocked = &H8000000
    <System.ComponentModel.Description("Low not locked (CVU_STATUS_CVLO1_UNLOCK)")> LowNotLocked = &H80000
    <System.ComponentModel.Description("Low Voltage overflow (CVU_STATUS_CVLO1_VOFLO)")> LowVoltageOverflow = &H20000
    <System.ComponentModel.Description("Low Current overflow (CVU_STATUS_CVLO1_IOFLO)")> LowCurrentOverflow = &H10000
End Enum


''' <summary>
''' Enumerates the status parameter for reading or setting instrument parameters.
''' </summary>
Public Enum StatusParameter
    <System.ComponentModel.Description("No Faults")> None = 0
    <System.ComponentModel.Description("Impedance measure mode (KI_CVU_MEASURE_MODEL)")> MeasurementModel = 14000
    <System.ComponentModel.Description("Measurement speed (KI_CVU_MEASURE_SPEED)")> MeasurementSpeed = 14001
    <System.ComponentModel.Description("Delay factor (KI_CVU_DELAY_FACTOR)")> DelayFactor = 14002
    <System.ComponentModel.Description("Filter factor (KI_CVU_FILTER_FACTOR)")> FilterFactor = 14003
    <System.ComponentModel.Description("A/D aperture time (KI_CVU_APERTURE)")> Aperture = 14004
    <System.ComponentModel.Description("Cable length corrected (KI_CVU_CABLE_CORRECT)")> CableCorrection = 14005
    <System.ComponentModel.Description("Open compensation (KI_CVU_OPEN_COMPENSATE)")> OpenCompensation = 14006
    <System.ComponentModel.Description("Short compensation (KI_CVU_SHORT_COMPENSATE)")> ShortCompensation = 14007
    <System.ComponentModel.Description("Load compensation (KI_CVU_LOAD_COMPENSATE)")> LoadCompensation = 14008

    <System.ComponentModel.Description("(KI_CVU_DC_SRC_HI)")> SourceHigh = 14009
    <System.ComponentModel.Description("(KI_CVU_DC_SRC_LO)")> SourceLow = 14010
    <System.ComponentModel.Description("(KI_CVU_AC_SRC_HI)")> ACSourceHigh = 14011
    <System.ComponentModel.Description("(KI_CVU_AC_MEAS_LO)")> ACMeasureLow = 14012

    <System.ComponentModel.Description("Measurement status (KI_CVU_MEASURE_STATUS)")> MeasurementStatus = 15000
    <System.ComponentModel.Description("Measurement timestamp (KI_CVU_MEASURE_TSTAMP)")> MeasurementTimestamp = 15001
    <System.ComponentModel.Description("Drive frequency (KI_CVU_FREQUENCY)")> MeasurementFrequency = 15002
    <System.ComponentModel.Description("AC current range (KI_CVU_ACI_RANGE)")> ACCurrentRange = 15003
    <System.ComponentModel.Description("AC actual current range (KI_CVU_ACI_PRESENT_RANGE)")> ACCurrentPresentRange = 15004
    <System.ComponentModel.Description("AC voltage level (KI_CVU_ACV_LEVEL)")> ACVoltageLevel = 15005
    <System.ComponentModel.Description("DC bias voltage leve (KI_CVU_DCV_LEVEL)")> DCVoltageLevel = 15006
    <System.ComponentModel.Description("DC Voltage Offset (KI_CVU_DCV_OFFSET)")> DCVoltageOffset = 15007
End Enum

''' <summary>
''' Enumerates the cable lengths for compensation.
''' </summary>
Public Enum CorrectedCableLength
    <System.ComponentModel.Description("0m")> DirectConnect = 0
    <System.ComponentModel.Description("1.5m")> OnePointFiveMeters = 1
    <System.ComponentModel.Description("3.0m")> ThreeMeters = 3
End Enum

''' <summary>
''' Enumerates the available current ranges.
''' </summary>
Public Enum CurrentRange
    <System.ComponentModel.Description("Auto")> AutoRange = 0
    <System.ComponentModel.Description("1uA")> LowRange = 1
    <System.ComponentModel.Description("30uA")> MediumRange = 30
    <System.ComponentModel.Description("1mA")> HighRange = 1000
End Enum

#End Region

