Imports System.Runtime.InteropServices
<Module: CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope:="namespace", Target:="isr.Kte.Lpt")> 

Namespace Lpt

    ''' <summary>
    ''' Implements safe application programming interface calls using 
    ''' the Keithley Test Environment Linear Parametric Test library.
    ''' This class suppresses stack walks for unmanaged code permission.
    ''' This class is for methods that are safe for anyone to call. Callers of these methods are 
    ''' not required to do a full security review to ensure that the usage is secure because 
    ''' the methods are harmless for any caller.
    ''' </summary>
    ''' <license>
    ''' (c) 2009 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="03/30/09" by="David" revision="1.0.3376.x">
    ''' Created
    ''' </history>
    <AttributeUsage(AttributeTargets.Class Or AttributeTargets.Method Or AttributeTargets.Interface)>
    Public NotInheritable Class SafeNativeMethods

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Prevents construction of this class.</summary>
        Private Sub New()
        End Sub

#End Region

#Region " LIBRARY IMPORTS "

        ''' <summary>
        ''' Sets all sources to a zero state. 
        ''' </summary>
        ''' <remarks>
        ''' Clears all sources sequentially in the reverse order from which they were originally forced. 
        ''' For C-V testing, this function will turn off the DC bias voltage. 
        ''' See devclr function in Section 8 for more information. 
        ''' devclr is implicitly called by devint.
        ''' </remarks>
        <DllImport("lptlib.dll", EntryPoint:="devclr", SetLastError:=True)>
        Private Shared Function devclr() As Int32
        End Function

        ''' <summary>
        ''' Resets the instruments and clears the system by opening all relays and disconnecting the pathways. 
        ''' Meters and sources are reset to the default states. 
        ''' Refer to the specific hardware manuals for listings of available ranges together with the default 
        ''' conditions and ranges for the instrumentation. 
        ''' </summary>
        ''' <remarks>
        ''' This function will reset all instruments in the system to their default states. 
        ''' This function will perform the following actions prior to resetting the instruments:<para>
        '''  1) Clear all sources by calling devclr.</para><para>
        '''  2) Clear the matrix cross-points by calling clrcon.</para><para>
        '''  3) Clear the trigger tables by calling clrtrg.</para><para>
        '''  4) Clear the sweep tables by calling clrscn.</para><para>
        '''  5) Reset GPIB instruments by sending the string defined with kibdefint.</para><para>
        '''  6) Stops the pulse generator card, and selects the Standard pulse mode and its default settings (like *RST).</para>
        '''  devint is implicitly called by execut and tstdsl.
        ''' <para>
        ''' This function will reset all instruments in the system to their default states. The Model 4200-CVU returns to the following states: </para><para>
        '''   30mV RMS AC signal</para><para>
        '''   0V DCV bias</para><para>
        '''   100kHz frequency</para><para>
        '''   Auto range</para><para>
        '''   Cable length correction set to 0M. </para><para>
        '''   Open/Short/Load compensation disabled. </para><para>
        ''' See devint function in Section 8 for more information.</para><para>
        ''' </para> 
        ''' </remarks>
        <DllImport("lptlib.dll", EntryPoint:="devint", SetLastError:=True)>
        Private Shared Function devint() As Int32
        End Function

        ''' <summary>
        ''' Deselects a test station. 
        ''' </summary>
        ''' <remarks>
        ''' Relinquishes control of an individual test station, 
        ''' a new test station must now be selected using tstsel before any subsequent test control functions are run. 
        ''' The tstdsl command has the same effect as the tstsel (0) command. 
        ''' Note that tstdsl is not required for use in a UTM.
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        <DllImport("lptlib.dll", EntryPoint:="tstdsl", SetLastError:=True)>
        Private Shared Function tstdsl() As Int32
        End Function

        ''' <summary>
        ''' Enables or disables a test station.  
        ''' </summary>
        ''' <param name="value">Specifies test station number, 0 or 1.</param>
        ''' <remarks>
        ''' Is normally called at the beginning of a test program. 
        ''' tstsel (1) will select the first test station and load the instrumentation configuration. 
        ''' tstsel (0) deselects the test station. 
        ''' tstsel is not required for use in a UTM.
        ''' </remarks>
        <DllImport("lptlib.dll", EntryPoint:="tstsel", SetLastError:=True)>
        Private Shared Function tstsel(ByVal value As Integer) As Int32
        End Function

        ''' <summary>
        ''' Get the instrument identifier (ID) from the instrument name string. 
        ''' </summary>
        ''' <param name="value">The instrument name</param>
        ''' <param name="id">The returned instrument identifier</param>
        <DllImport("lptlib.dll", EntryPoint:="getinstid", SetLastError:=True,
           BestFitMapping:=False, CharSet:=CharSet.Ansi,
           ThrowOnUnmappableChar:=True)>
        Private Shared Function getinstid(<MarshalAs(UnmanagedType.LPStr)> ByVal value As String, ByRef id As Integer) As Int32
        End Function

#If FULL Then
        <DllImport("IEEE_32M.DLL", EntryPoint:="_ieee_receive@16", CharSet:=CharSet.Ansi, 
             CallingConvention:=CallingConvention.Winapi, PreserveSig:=False, SetLastError:=True, ExactSpelling:=True)> 
        Friend Shared Sub Receive(<MarshalAs(UnmanagedType.VBByRefStr)> ByRef r As String, ByVal maxLength As Int32, ByRef length As Int32, ByRef status As Gpib_lastStatus)
        End Sub
        int WINAPI getinstattr( int inst_id, char *attrstr, char *attrvalstr );
#End If

        ''' <summary>
        ''' All instruments in the system configuration have specific attributes. GPIB address is an 
        ''' example of an attribute. The values of these attributes change as the system configuration
        ''' is changed. Therefore, by getting the values of key attributes at run time, user modules
        ''' can be developed in a configuration-independent manner. Given an instrument 
        ''' identification code and an attribute name string, this module returns the specified attribute value string.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="attributeName">The instrument attribute name string, such as
        ''' GPIBADDR, NUMOFPINS or MODELNUM</param>
        ''' <param name="attributeValue">The value string of the requested attribute.
        ''' If the requested attribute exists, the returned string will match one of the values shown 
        ''' in the Attribute value string column of Table 8-8. If the requested attribute does not exist, 
        ''' the attrvalstr parameter will set to a null string. 
        ''' </param>
        <DllImport("lptlib.dll", EntryPoint:="getinstattr", SetLastError:=True,
           BestFitMapping:=False, CharSet:=CharSet.Ansi,
           ThrowOnUnmappableChar:=True)>
        Private Shared Function getinstattr(ByVal id As Integer,
                                            <MarshalAs(UnmanagedType.LPStr)> ByVal attributeName As String,
                                            <MarshalAs(UnmanagedType.VBByRefStr)> ByRef attributeValue As String) As Int32
        End Function

        ''' <summary>
        ''' Returns various parameters pertaining to the state of the Model 4200-CVU. 
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="parameter">Parameter to be queried.
        ''' KI_CVU_LOAD_COMPENSATE Load compensation (ON or OFF)
        ''' KI_CVU_OPEN_COMPENSATE Open compensation (ON or OFF
        ''' KI_CVU_SHORT_COMPENSATE Short compensation (ON or OFF)
        ''' KI_CVU_CABLE_CORRECT Current length setting for which the CVU card is correcting (0, 1.5 or 3).
        ''' KI_CVU_ACI_RANGE AC current range (0 for auto range, or 1.5µA, 50µA or 1.5 mA for fixed range)
        ''' KI_CVU_PRESENT_RANGE AC current range (1.5µA, 50µA or 1.5 mA)
        ''' KI_CVU_ACV_LEVEL AC voltage level (10 mV to 100 mV RMS)
        ''' KI_CVU_APERTURE or  A/D aperture time (0.006 to 10.002 PLCs)
        ''' KI_INTGPLC  Integration (NPLC= 1/aperture time)
        ''' KI_CVU_DCV_LEVEL DC bias voltage level (-30 V to 30 V)
        ''' KI_CVU_DELAY_FACTOR Delay factor (0 to 100)
        ''' KI_CVU_FILTER_FACTOR Filter factor (0 to 100)
        ''' KI_CVU_FREQUENCY Drive frequency (10 kHz to 10 MHz)
        ''' KI_CVU_MEASURE_MODEL Impedance measure model (0 through 5; see Table 15-45)
        ''' KI_CVU_MEASURE_SPEED Measurement speed (fast, normal, quiet or custom)
        ''' KI_CVU_MEASURE_STATUS Measurement status (for last reading)
        ''' KI_CVU_MEASURE_TSTAMP Measurement timestamp (for last reading)
        ''' 1. Returns range used for last measurement, even if on auto range.
        ''' 2. The measurement status codes are listed and explained in Table 15-43.
        ''' </param>
        ''' <param name="value">Returned value for the queried parameter.</param>
        <DllImport("lptlib.dll", EntryPoint:="getstatus", SetLastError:=True)>
        Private Shared Function getstatus(ByVal id As Integer, ByVal parameter As StatusParameter, ByRef value As Double) As Int32
        End Function

        ''' <summary>
        ''' Programs a sourcing instrument to generate a voltage or current at a specific level.
        ''' <para>
        ''' CVU: Sets the DC bias voltage level. 
        ''' </para>
        ''' </summary>
        ''' <param name="id">The instrument identifier</param>
        ''' <param name="level">The level of the bipolar voltage or current forced in volts or amperes.
        ''' <para>
        ''' DC bias voltage level (-30V to 30V)
        ''' </para>
        ''' </param>
        ''' <remarks>
        ''' forcev and forcei generate either a positive or negative voltage as directed by the sign of the value argument.
        ''' With both forcev and forcei:
        '''   -- Positive values generate positive voltage or current from the high terminal of the source 
        '''      relative to the low terminal.
        '''   -- Negative values generate negative voltage or current from the high terminal of the source 
        '''      relative to the low terminal.
        ''' When using limitX, rangeX, and forceX on the same source at the same time in a test sequence, 
        ''' call limitX and rangeX before forceX. See “Using source compliance limits” on page 8-56 for details.
        ''' The ranges of currents and voltages available from a voltage or current source vary with the specific
        ''' instrument type. For more detailed information, refer to the specific hardware manual for each instrument.
        ''' To force zero current with a higher voltage limit than the 20V default, include one of the following calls
        ''' ahead of the forcei call:
        '''   -- A measv call, which causes the Model 4200-SCS to autorange to a higher voltage limit.
        '''   -- A rangev call to an appropriate fixed voltage, which results in a fixed voltage limit. 
        ''' To force zero volts with a higher current limit than the 10mA default, include one of the following calls
        ''' ahead of the forcev call:
        '''   -- A measi call, which causes the Model 4200-SCS to autorange to a higher current limit.
        '''   -- A rangei call to an appropriate fixed current, which results in a fixed current limit. 
        ''' <para>
        ''' CVU:
        ''' This function is used to set a DC bias level for a single impedance measurement, 
        ''' and a frequency sweep. Use the setfreq and setlevel functions to set the AC drive frequency and AC 
        ''' voltage for the sweep. The DC source operates independently of the AC source. 
        ''' Changes to the level and state of the DC source take effect immediately, whereas the AC frequency 
        ''' and source value are only utilized during measz operations.
        ''' </para>
        ''' </remarks>
        ''' <example>
        ''' Example The reverse bias leakage of a diode is measured after applying 40.0V to the junction
        ''' double ir12
        ''' conpin(SMU1L, 2, GND, 0)
        ''' conpin(SMU1H, 1, 0)
        ''' limiti(SMU1, 2.0E-4) Limit 1 to 200μA.
        ''' forcev(SMU1, 40.0) Apply 40.0V.
        ''' measi(SMU1, ir12) Measure leakage
        ''' return results to ir12. 
        ''' </example>
        <DllImport("lptlib.dll", EntryPoint:="forcev", SetLastError:=True)>
        Private Shared Function forcev(ByVal id As Integer, ByVal level As Double) As Int32
        End Function

        ''' <summary>
        ''' Programs a sourcing instrument to generate a voltage or current at a specific level.
        ''' </summary>
        ''' <param name="id">The instrument identifier</param>
        ''' <param name="level">The level of the bipolar voltage or current forced in volts or amperes.</param>
        <DllImport("lptlib.dll", EntryPoint:="forcei", SetLastError:=True)>
        Private Shared Function forcei(ByVal id As Integer, ByVal level As Double) As Int32
        End Function

        ''' <summary>
        ''' Retrieves the frequency sourced during a single measurement.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Returned frequency</param>
        ''' <returns>
        ''' Returns the present test frequency used for a single impedance measurement. 
        ''' The measz function is used to perform a single measurement.
        ''' </returns>
        ''' <remarks>
        ''' </remarks>
        <DllImport("lptlib.dll", EntryPoint:="measf", SetLastError:=True)>
        Private Shared Function measf(ByVal id As Integer, ByRef value As Double) As Int32
        End Function

        ''' <summary>
        ''' Retrieves the DC bias voltage sourced during a single measurement.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Returned DC bias voltage</param>
        ''' <returns>
        ''' Returns the DC bias voltage presently being used for a single measurement. 
        ''' The measz function is used to perform a single measuremen
        ''' </returns>
        <DllImport("lptlib.dll", EntryPoint:="measv", SetLastError:=True)>
        Private Shared Function measv(ByVal id As Integer, ByRef value As Double) As Int32
        End Function

        ''' <summary>
        ''' Performs an impedance measurement.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="model">Measurement model (see Table 15-46).</param>
        ''' <param name="speed">Measurement speed.</param>
        ''' <param name="primaryValue">First result of the selected measure model</param>
        ''' <param name="secondaryValue">Second result of the selected measure model</param>
        <DllImport("lptlib.dll", EntryPoint:="measz", SetLastError:=True)>
        Private Shared Function measz(ByVal id As Integer,
                                      ByVal model As ImpedanceModel,
                                      ByVal speed As MeasurementSpeed,
                                      ByRef primaryValue As Double, ByRef secondaryValue As Double) As Int32
        End Function

        ''' <summary>
        ''' Selects a range and prevents the selected instrument from autoranging. 
        ''' By selecting a range, the time required for autoranging is eliminated.
        ''' <para>
        ''' For CV Measuremets: Selects an impedance measurement range. 
        ''' </para>
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        ''' <param name="value">The value of the highest measurement to be taken. 
        ''' The most appropriate range for this measurement will be selected. 
        ''' If range is set to 0, the instrument will autorange.<para>
        ''' For CV Measurements: Impedance measure range (0, 1µA, 30µA or 1mA).
        ''' </para>
        ''' </param>
        ''' <remarks>
        ''' rangeX is primarily intended to eliminate the time required by the automatic range selection performed by a measuring instrument. Because rangeX will prevent autoranging, an overrange condition can occur, for example, measuring 10.0V on a 2.0V range. The value 1.0E+22 is returned when this occurs. 
        ''' rangeX can also reference a source, because a SMU can be either of the following: 
        ''' • Simultaneously a voltage source, voltmeter, and current meter.
        ''' • Simultaneously a current source, current meter, and voltmeter.
        ''' The range of a SMU is the same for the sourcing function and the measuring function.
        ''' Compliance limits – When selecting a range below the limit value, whether it is explicitly programmed or the default value, an instrument will temporarily use the full scale value of the range as the limit. This will not change the programmed limit value and, if the instrument range is restored to a value higher than the programmed limit value, the instrument will again use the programmed limit value. See “Using source compliance limits” earlier in this section for more information.
        ''' When changing the instrument range, be careful not to overrange the instrument. For example, a test initially performed in the 10mA range with a 5mA limit is changed to test in the 1mA range with a 1mA limit. Notice that the limit is lowered from 5mA to 1mA to avoid overranging the 1mA setting.
        ''' <para>
        ''' For CVU: Sets the Model 4200-CVU to a current measure range for impedance measurements. 
        ''' Setting range to 0 selects auto range. The CVU automatically goes to the most sensitive 
        ''' (optimum) range to perform the measurement. This is the same as calling the setauto function. 
        ''' The other range parameter values select a fixed measure range. 
        ''' The CVU will remain on the fixed range until auto range is enabled or the CVU is reset (devint called). 
        ''' </para>
        ''' </remarks>
        <DllImport("lptlib.dll", EntryPoint:="rangei", SetLastError:=True)>
        Private Shared Function rangei(ByVal id As Integer, ByVal value As Double) As Int32
        End Function

        ''' <summary>
        ''' Selects a range and prevents the selected instrument from autoranging. By selecting a range, 
        ''' the time required for autoranging is eliminated.
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        ''' <param name="value">The value of the highest measurement to be taken. 
        ''' The most appropriate range for this measurement will be selected. 
        ''' If range is set to 0, the instrument will autorange.</param>
        <DllImport("lptlib.dll", EntryPoint:="rangev", SetLastError:=True)>
        Private Shared Function rangev(ByVal id As Integer, ByVal value As Double) As Int32
        End Function

        ''' <summary>
        ''' Sets the frequency for the AC drive.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Frequency of the AC drive.</param>
        ''' <remarks>
        ''' The Model 4200-CVU provides test frequencies from 10kHz to 10MHz in the following steps:
        ''' -- 10kHz to 100kHz in 10kHz steps
        ''' -- 100kHz to 1MHz in 100kHz steps
        ''' -- 1MHz to 10MHz in 1MHz steps
        ''' For Frequencies not supported the closest supported frequency is selected (e.g., 15kHz input will select 20kHz). 
        ''' The getstatus function can be used to retrieve the selected frequency value. 
        ''' AC drive (AC voltage level and frequency) does not turn on until a measurement is performed. 
        ''' The AC drive turns off after the measurement is completed. 
        ''' Note that the DC voltage source stays on for the whole test.
        ''' Programming examples 1, 2, 4 and 5 use the setfreq function to set the AC drive frequency. 
        ''' </remarks>
        ''' <example>
        ''' This example also acquires a timestamp for the measurement. 
        ''' double result1, result2, timeStamp;
        ''' int status;
        ''' status = forcev(CVU1, 1); /*  Set DC bias to 1V. */
        ''' status = setfreq(CVU1, 100e3);  /*  Set AC drive frequency to 100kHz. */
        ''' status = setlevel(CVU1, 15e-3); /*  Set AC drive voltage to 15mVRMS. */
        ''' status = rdelay(0.1); /*  Set settling time to 100ms. */
        ''' status = rangei(CVU1, 1.0e-3); /* Select 1mA measure range. */
        ''' status = measz(CVU1, KI_CVU_TYPE_CSRS, KI_CVU_SPEED_NORMAL, result1, result2); /*  Measure CsRs. */
        ''' status = meast(CVU1, timeStamp); /*  Return timestamp for measurement. */
        ''' status = devint(); /*  Reset CVU. */
        ''' </example>
        <DllImport("lptlib.dll", EntryPoint:="setfreq", SetLastError:=True)>
        Private Shared Function setfreq(ByVal id As Integer, ByVal value As Double) As Int32
        End Function

        ''' <summary>
        ''' Sets the AC drive voltage level.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Voltage level of the AC drive (10mV to 100mVRMS</param>
        <DllImport("lptlib.dll", EntryPoint:="setlevel", SetLastError:=True)>
        Private Shared Function setlevel(ByVal id As Integer, ByVal value As Double) As Int32
        End Function

        ''' <summary>
        ''' Set operating modes specific to the Model 4200-CVU.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="modifier">Specific operating characteristic to change</param>
        ''' <param name="value">Parameter value for the modifier.</param>
        ''' <remarks>
        ''' The setmode function allows control over the following Model 4200-CVU operating characteristics:<para>
        ''' --Connection compensation control (on/off) for open, short and load. 
        '''   When disabled, saved compensation constants will not be applied to the measurements. 
        '''   Whenever the connection setup has changed, connection compensation will need to be performed 
        '''   in order to acquire and save new compensation constants). Connection compensation is performed from KITE. 
        '''   For details, see "Connection compensation"). </para><para>
        ''' --Setting for cable length correction (0m, 1.5m or 3m). 
        '''   This setting is made from the window used to enable correction (see "Enabling compensation"). </para><para>
        ''' --Settings (delay factor, filter factor and aperture) for KI_CUSTOM measurement speed 
        '''   (which is set by measz, smeasz or smeaszRT).
        ''' </para>
        ''' </remarks>
        <DllImport("lptlib.dll", EntryPoint:="setmode", SetLastError:=True)>
        Private Shared Function setmode(ByVal id As Integer, ByVal modifier As StatusParameter, ByVal value As Double) As Int32
        End Function

        ''' <summary>
        ''' A user-programmable delay in seconds.
        ''' </summary>
        <DllImport("lptlib.dll", EntryPoint:="rdelay", SetLastError:=True)>
        Private Shared Function rdelay(ByVal value As Double) As Int32
        End Function

#End Region

#Region " DERIVED FUNCTIONS "

        ''' <summary>
        ''' Returns true if last operation succeeded.
        ''' This is signified by the <see cref="LastOperationStatus">last Operation status</see>
        ''' greater than or equal <see cref="OperationStatus.Success">okay</see>
        ''' </summary>
        Public Shared ReadOnly Property LastOperationSuccess() As Boolean
            Get
                Return SafeNativeMethods._lastOperationStatus >= OperationStatus.Success
            End Get
        End Property

        ''' <summary>
        ''' Returns true if last operation failed.
        ''' This is signified by the <see cref="LastOperationStatus">last Operation status</see>
        ''' smaller than <see cref="OperationStatus.Success">okay</see> (negative).
        ''' </summary>
        Public Shared ReadOnly Property LastOperationFailed() As Boolean
            Get
                Return SafeNativeMethods._lastOperationStatus < OperationStatus.Success
            End Get
        End Property

        Private Shared _alternatingVoltageLevel As Double
        ''' <summary>
        ''' Returns the AC voltage level.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function AlternatingVoltageLevelGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.ACVoltageLevel, SafeNativeMethods._alternatingVoltageLevel)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting the AC drive voltage level from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._alternatingVoltageLevel = emulatedValue
            End If
            Return CSng(SafeNativeMethods._alternatingVoltageLevel)

        End Function

        ''' <summary>
        ''' Sets the AC drive voltage level. 
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Voltage level of the AC drive (10mV to 100mVRMS</param>
        Public Shared Function AlternatingVoltageLevelSetter(ByVal id As Integer, ByVal value As Single) As Int32

            SafeNativeMethods._alternatingVoltageLevel = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setlevel(id, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _aperture As Double
        ''' <summary>
        ''' Enables or disables Aperture.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <returns>Measurement speed in power line cycles between 0.006 to 10.002</returns>
        Public Shared Function ApertureGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.Aperture, SafeNativeMethods._aperture)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting apperture from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._aperture = emulatedValue
            End If
            Return CSng(SafeNativeMethods._aperture)

        End Function

        ''' <summary>
        ''' Enables or disables Aperture.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Desired measurement speed in power line cycles between 0.006 to 10.002.</param>
        Public Shared Function ApertureSetter(ByVal id As Integer, ByVal value As Single) As Integer

            SafeNativeMethods._aperture = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.Aperture, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _cableLength As Double
        ''' <summary>
        ''' Returns the cable length corrected.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function CableLengthGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.CableCorrection, SafeNativeMethods._cableLength)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting the cable length correction from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._cableLength = emulatedValue
            End If
            Return CSng(SafeNativeMethods._cableLength)

        End Function

        ''' <summary>
        ''' Sets the cable length correction. 
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Cable length corrected (0, 1.5, or 3 m)</param>
        Public Shared Function CableLengthSetter(ByVal id As Integer, ByVal value As Single) As Integer

            SafeNativeMethods._cableLength = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.CableCorrection, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        ''' <summary>
        ''' Sets the cable length correction. 
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Cable length corrected (0, 1.5, or 3 m)</param>
        Public Shared Function CableLengthSetter(ByVal id As Integer, ByVal value As CorrectedCableLength) As Int32

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.CableCorrection, BaseInstrument.ParseCableLength(value))
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _delayFactor As Double
        ''' <summary>
        ''' Enables or disables Delay Factor.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <returns>Delay factor between 0 and 100</returns>
        Public Shared Function DelayFactorGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.DelayFactor, SafeNativeMethods._delayFactor)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting delay factor from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._delayFactor = emulatedValue
            End If
            Return CSng(SafeNativeMethods._delayFactor)

        End Function

        ''' <summary>
        ''' Enables or disables Delay Factor.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Desired delay factor between 1 and 100.</param>
        ''' <remarks>
        ''' After applying a forced voltage or current, an SMU waits for a delay time before making a measurement. 
        ''' The delay time allows for source settling. The default delay time is pre-programmed and range-dependent, 
        ''' to allow for the very long settling times needed at very low current ranges. 
        ''' The applied delay time is a multiple of the default delay time, and the value in the Delay Factor 
        ''' edit box specifies this multiple. That is: 
        ''' Applied delay time = (Default delay time) × (Delay Factor)
        ''' For example, if the default delay time is 1ms and the Delay Factor is 0.7, the applied delay time is 0.7ms (1ms ×  0.7).
        ''' If you select the Custom measurement Speed mode, you can enter a custom Delay Factor of 0 to 100. If you select the Fast, Normal, or Quiet measurement Speed mode, the SMU chooses an appropriate, fixed Delay Factor. Table 6-7 summarizes allowed Delay Factor settings for various measurement Speed modes:
        ''' fast = 0.7, normal = 1, quiet = 1.3, custom = 0 to 100.
        ''' When entering a custom Delay Factor setting, consider the following:
        ''' -- A Delay Factor of 1 allows for a normal amount of settling before the A/D converter is 
        ''' triggered to make a measurement. 
        ''' -- Each doubling of the Delay Factor doubles the time allowed for settling. 
        ''' -- A delay factor of 0 multiplies the default delay by zero, resulting in no delay.
        ''' </remarks>
        Public Shared Function DelayFactorSetter(ByVal id As Integer, ByVal value As Single) As Int32

            SafeNativeMethods._delayFactor = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.DelayFactor, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        ''' <summary>
        ''' A user-programmable delay in seconds.
        ''' </summary>
        ''' <param name="value">The desired delay duration in seconds</param>
        Public Shared Function DelayNextAction(ByVal value As Single) As Integer

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.rdelay(value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _directCurrentLevel As Single
        ''' <summary>
        ''' Sets the source current level. 
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function DirectCurrentLevelSetter(ByVal id As Integer, ByVal value As Single) As Int32

            SafeNativeMethods._directCurrentLevel = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.forcei(id, SafeNativeMethods._directCurrentLevel)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _directVoltageLevel As Double
        ''' <summary>
        ''' Retrieves the DC bias voltage sourced during a single measurement.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <returns>
        ''' Returns the DC bias voltage presently being used for a single measurement. 
        ''' The measz function is used to perform a single measurement
        ''' </returns>
        Public Shared Function DirectVoltageLevelGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.measv(id, SafeNativeMethods._directVoltageLevel)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting voltage level from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._directVoltageLevel = emulatedValue
            End If
            Return CSng(SafeNativeMethods._directVoltageLevel)

        End Function

        ''' <summary>
        ''' Sets the DC source voltage level. 
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function DirectVoltageLevelSetter(ByVal id As Integer, ByVal value As Single) As Int32

            SafeNativeMethods._directVoltageLevel = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.forcev(id, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _filterFactor As Double
        ''' <summary>
        ''' Enables or disables Filter Factor.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <returns>Filter factor value between 1 and 100</returns>
        Public Shared Function FilterFactorGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.FilterFactor, SafeNativeMethods._filterFactor)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting filter factor from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._filterFactor = emulatedValue
            End If
            Return CSng(SafeNativeMethods._filterFactor)

        End Function

        ''' <summary>
        ''' Enables or disables Filter Factor.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Desired Filter factor between 1 and 100.</param>
        ''' <remarks>
        ''' <para>
        ''' To reduce measurement noise, each Model 4200-SCS SMU applies filtering, which may include 
        ''' averaging of multiple readings to make one measurement. The SMU automatically adjusts 
        ''' the filtering to fit the selected measurement range (this system works particularly well, 
        ''' because measurements at lower current ranges require much more filtering [and much more time] 
        ''' than at higher ranges). The value entered in the Filter Factor edit box specifies a multiple 
        ''' of this preprogrammed filtering
        ''' </para>
        ''' <para>
        ''' If you select the Custom measurement Speed mode, you can enter a Filter Factor value of 0 to 100. 
        ''' If you select the Fast, Normal, or Quiet measurement Speed mode, the SMU sets an appropriate fixed 
        ''' Filter Factor. Table 6-8 summarizes allowed Filter Factor settings for various measurement Speed modes.
        ''' </para>
        ''' Summary of allowed Filter Factor settings: Fast = 0.2; Normal = 1; Quiet = 3; Custom = 0 to 100.
        ''' <para>
        ''' When entering a custom Filter Factor, consider the following: 
        ''' </para>
        ''' <para>
        ''' A Filter Factor of 1 specifies a normal level of filtering.
        ''' </para>
        ''' <para>
        ''' As a rule of thumb, doubling the Filter Factor halves the measurement noise.
        ''' </para>
        ''' <para>
        ''' A Filter Factor of 0 nullifies the SMU internal filtering.
        ''' </para>
        ''' </remarks>
        Public Shared Function FilterFactorSetter(ByVal id As Integer, ByVal value As Single) As Integer

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.FilterFactor, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _instrumentId As Integer
        ''' <summary>
        ''' Returns the instrument identified. 
        ''' </summary>
        Public Shared Function InstrumentIdGetter(ByVal instrumentName As String) As Integer?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getinstid(instrumentName, SafeNativeMethods._instrumentId)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Integer?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting instrument id for instrument name {0}. Error {1}", instrumentName, SafeNativeMethods._lastStatus)
                End If
            End If
            Return SafeNativeMethods._instrumentId

        End Function

        ''' <summary>
        ''' Check if the specified module exists. 
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        Public Shared Function InstrumentAttributeGetter(ByVal id As Integer) As Integer

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                Dim name As String = "".PadRight(42)
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getinstattr(id, "MODELNUM", name)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        ''' <summary>
        ''' Check if the specified module exists. 
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        Public Shared Function IsModuleExists(ByVal id As Integer) As Boolean

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getinstattr(id, "MODELNUM", "".PadRight(42))
            End If
            Return SafeNativeMethods._lastOperationStatus = OperationStatus.Success

        End Function

        ''' <summary>
        ''' Gets the module name. 
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        Public Shared Function ModuleNumberGetter(ByVal id As Integer, ByVal emulatedValue As String) As String

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                Dim name As String = "".PadRight(42)
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getinstattr(id, "MODELNUM", name)
                If SafeNativeMethods._lastOperationStatus = OperationStatus.Success Then
                    Return name.Trim().Trim(CChar(Nothing))
                Else
                    Return ""
                End If
            Else
                Return emulatedValue
            End If
            Return ""

        End Function

        Private Shared _lastOperationStatus As Integer
        ''' <summary>
        ''' Gets the last status. . Allows setting the operation status in case of 
        ''' exceptions or for clearing in case on non-KXCI operations.
        ''' </summary>
        Public Shared Property LastOperationStatus() As Integer
            Get
                Return SafeNativeMethods._lastOperationStatus
            End Get
            Set(ByVal value As Integer)
                SafeNativeMethods._lastOperationStatus = value
            End Set
        End Property

        Private Shared _loadCompensation As Double
        ''' <summary>
        ''' Enables or disables Load compensation.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function LoadCompensationGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Boolean?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.LoadCompensation, SafeNativeMethods._loadCompensation)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Boolean?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting load compensation parameter from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._loadCompensation = emulatedValue
            End If
            Return SafeNativeMethods._loadCompensation > 0.5

        End Function

        ''' <summary>
        ''' Enables or disables Load compensation.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="enabled">True or false.</param>
        Public Shared Function LoadCompensationSetter(ByVal id As Integer, ByVal enabled As Boolean) As Integer

            SafeNativeMethods._loadCompensation = CSng(IIf(enabled, 1, 0))
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.LoadCompensation, SafeNativeMethods._loadCompensation)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _primaryValue As Double
        Private Shared _secondayValue As Double
        ''' <summary>
        ''' Makes an impedance measurement using the preset data.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="model">Measurement model (see Table 15-46).</param>
        ''' <param name="speed">Measurement speed.</param>
        Public Shared Function MeasureImpedance(ByVal id As Integer,
                              ByVal model As ImpedanceModel,
                              ByVal speed As MeasurementSpeed) As Impedance
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.measz(id, model, speed, SafeNativeMethods._primaryValue, SafeNativeMethods._secondayValue)
                If SafeNativeMethods._lastOperationStatus < OperationStatus.Success Then
                    Return Impedance.Empty()
                    ' Throw New isr.Kte.FunctionCallException("Failed measuring impedance on instrument {0} with model {1} and speed {2}. Error {3}", id, model, speed, SafeNativeMethods._lastStatus)
                End If
            End If
            Return New Impedance(SafeNativeMethods._primaryValue, SafeNativeMethods._secondayValue)

        End Function

        Private Shared _measurementModel As Double
        ''' <summary>
        ''' Returns the measurement Model.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function MeasurementModelGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Integer?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.MeasurementModel, SafeNativeMethods._measurementModel)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Integer?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting Measurement Model from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._measurementModel = emulatedValue
            End If
            Return CInt(SafeNativeMethods._measurementModel)

        End Function

        Private Shared _measurementSpeed As Double
        ''' <summary>
        ''' Returns the measurement speed.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function MeasurementSpeedGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Integer?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.MeasurementSpeed, SafeNativeMethods._measurementSpeed)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Integer?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting measurement speed from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._measurementSpeed = emulatedValue
            End If
            Return CInt(SafeNativeMethods._measurementSpeed)

        End Function

        Private Shared _measurementStatus As Double
        ''' <summary>
        ''' Returns the measurement Status.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function MeasurementStatusGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Integer?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.MeasurementStatus, SafeNativeMethods._measurementStatus)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Integer?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting measurement Status from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._measurementStatus = emulatedValue
            End If
            Return CInt(SafeNativeMethods._measurementStatus)

        End Function

        Private Shared _measuremenTimestamp As Double
        ''' <summary>
        ''' Returns the measurement Timestamp.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function MeasurementTimestampGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.MeasurementTimestamp, SafeNativeMethods._measuremenTimestamp)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting measurement Timestamp from instrument number {0}. Error {1}", id, SafeNativeMethods._lastTimestamp)
                End If
            Else
                SafeNativeMethods._measuremenTimestamp = emulatedValue
            End If
            Return CSng(SafeNativeMethods._measuremenTimestamp)

        End Function

        Private Shared _openCompensation As Double
        ''' <summary>
        ''' Enables or disables open compensation.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function OpenCompensationGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Boolean?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.OpenCompensation, SafeNativeMethods._openCompensation)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Boolean?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting open compensation parameter from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._openCompensation = emulatedValue
            End If
            Return SafeNativeMethods._openCompensation > 0.5

        End Function

        ''' <summary>
        ''' Enables or disables open compensation.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="enabled">True or false.</param>
        Public Shared Function OpenCompensationSetter(ByVal id As Integer, ByVal enabled As Boolean) As Int32

            SafeNativeMethods._openCompensation = CSng(IIf(enabled, 1, 0))
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.OpenCompensation, SafeNativeMethods._openCompensation)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _senseCurrentRange As Double
        ''' <summary>
        ''' Returns the current sense range.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <returns>Current sense range.</returns>
        Public Shared Function SenseCurrentRangeGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.ACCurrentPresentRange, SafeNativeMethods._senseCurrentRange)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting current sense range from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._senseCurrentRange = emulatedValue
            End If
            Return CSng(SafeNativeMethods._senseCurrentRange)

        End Function

        ''' <summary>
        ''' Selects a range and prevents the selected instrument from auto ranging. 
        ''' By selecting a range, the time required for auto ranging is eliminated.
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        ''' <param name="value">The value of the highest measurement to be taken. 
        ''' The most appropriate range for this measurement will be selected. 
        ''' If range is set to 0, the instrument will auto range.</param>
        Public Shared Function SenseCurrentRangeSetter(ByVal id As Integer, ByVal value As Single) As Int32

            SafeNativeMethods._senseCurrentRange = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.rangei(id, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _shortCompensation As Double
        ''' <summary>
        ''' Enables or disables Short compensation.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        Public Shared Function ShortCompensationGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Boolean?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.getstatus(id, StatusParameter.ShortCompensation, SafeNativeMethods._shortCompensation)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Boolean?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting short compensation parameter from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._shortCompensation = emulatedValue
            End If
            Return SafeNativeMethods._shortCompensation > 0.5

        End Function

        ''' <summary>
        ''' Enables or disables Short compensation.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="enabled">True or false.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId:="short")>
        Public Shared Function ShortCompensationSetter(ByVal id As Integer, ByVal enabled As Boolean) As Int32

            SafeNativeMethods._shortCompensation = CSng(IIf(enabled, 1, 0))
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setmode(id, StatusParameter.ShortCompensation, SafeNativeMethods._shortCompensation)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _sourceFrequency As Double
        ''' <summary>
        ''' Retrieves the frequency sourced during a single measurement.
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <returns>
        ''' Returns the present test frequency used for a single impedance measurement. 
        ''' The measz function is used to perform a single measurement.
        ''' </returns>
        Public Shared Function SourceFrequencyGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.measf(id, SafeNativeMethods._sourceFrequency)
                If SafeNativeMethods._lastOperationStatus <> OperationStatus.Success Then
                    Return New Single?
                    ' Throw New isr.Kte.FunctionCallException("Failed getting drive frequency from instrument number {0}. Error {1}", id, SafeNativeMethods._lastStatus)
                End If
            Else
                SafeNativeMethods._sourceFrequency = emulatedValue
            End If
            Return CSng(SafeNativeMethods._sourceFrequency)

        End Function

        ''' <summary>
        ''' Sets the AC drive frequency. 
        ''' </summary>
        ''' <param name="id">Instrument ID of the Model 4200 module</param>
        ''' <param name="value">Frequency of the AC drive.</param>
        Public Shared Function SourceFrequencySetter(ByVal id As Integer, ByVal value As Single) As Int32

            SafeNativeMethods._sourceFrequency = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.setfreq(id, value)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        ''' <summary>
        ''' Gets or sets the condition for using devices.
        ''' </summary>
        ''' <remarks>
        ''' When true, the application connects to actual devices.  Set to false in case
        ''' testing the application in the absence of devices.
        ''' </remarks>
        Public Shared Property UsingDevices() As Boolean

        Private Shared _voltageRange As Double
        ''' <summary>
        ''' Sets the source voltage range. 
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        ''' <param name="value">The value of the highest measurement to be taken. 
        ''' The most appropriate range for this measurement will be selected. 
        ''' If range is set to 0, the instrument will autorange.</param>
        Public Shared Function VoltageRangeSetter(ByVal id As Integer, ByVal value As Single) As Integer

            SafeNativeMethods._voltageRange = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.rangev(id, SafeNativeMethods._voltageRange)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

#End Region

#Region " STATION FUNCTIONS "

        ''' <summary>
        ''' Sets all sources to a zero state. 
        ''' </summary>
        Public Shared Function ClearTestStation() As Integer

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.devclr()
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        ''' <summary>
        ''' Resets the instruments and clears the system by opening all relays and disconnecting the pathways. 
        ''' Meters and sources are reset to the default states. Refer to the specific hardware manuals for listings of available ranges together with the default conditions and ranges for the instrumentation. 
        ''' </summary>
        ''' <remarks>
        ''' <para>
        ''' This function will reset all instruments in the system to their default states. The Model 4200-CVU returns to the following states: </para><para>
        '''   30mV RMS AC signal</para><para>
        '''   0V DCV bias</para><para>
        '''   100kHz frequency</para><para>
        '''   Auto range</para><para>
        '''   Cable length correction set to 0M. </para><para>
        '''   Open/Short/Load compensation disabled. </para><para>
        ''' See devint function in Section 8 for more information.</para><para>
        ''' </para> 
        ''' </remarks>
        Public Shared Function InitializeTestStation() As Int32

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.devint()
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        ''' <summary>
        ''' Relinquishes control of the currently selected test station. 
        ''' </summary>
        Public Shared Function CloseTestStation() As Integer

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.tstsel(0)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        ''' <summary>
        ''' Enables the first test station.  
        ''' </summary>
        Public Shared Function OpenTestStation() As Integer

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._lastOperationStatus = Lpt.SafeNativeMethods.tstsel(1)
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

#End Region

#Region " DERIVED FUNCTIONS: TO BE IMPLEMENTED "

        Private Shared _channelCount As Integer
        ''' <summary>
        ''' Returns the number of CVU channels.
        ''' </summary>
        Public Shared Function ChannelCountGetter(ByVal emulatedValue As Integer) As Integer
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                SafeNativeMethods._channelCount = 1 ' SafeNativeMethods.QueryInteger(":CVU:CHANNELS?")
            Else
                SafeNativeMethods._channelCount = emulatedValue
            End If
            Return SafeNativeMethods._channelCount
        End Function

        ''' <summary>
        ''' Returns a status details
        ''' </summary>
        Public Shared Function OperationStatusDetails(ByVal status As Integer) As String
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "Last status={0}", status)
        End Function

        Private Shared _HoldTime As Single
        ''' <summary>
        ''' Gets the sample Interval.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="emulatedValue")>
        Public Shared Function HoldTimeGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single
            SafeNativeMethods._HoldTime = 0
            Return SafeNativeMethods._HoldTime
        End Function

        ''' <summary>
        ''' Sets the sample Interval.
        ''' </summary>
        ''' <remarks>
        ''' Presently, the low level instrument supports a single interval of 0.
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
        Public Shared Function HoldTimeSetter(ByVal id As Integer, ByVal value As Single) As Integer
            value = 0
            SafeNativeMethods._HoldTime = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            Return SafeNativeMethods._lastOperationStatus
        End Function

        ''' <summary>
        ''' Initiates a parallel test.
        ''' </summary>
        Public Shared Function InitiateParallelTest() As Integer
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Kxci.SafeNativeMethods.UsingDevices Then
                ' Return SafeNativeMethods.WriteLine(":CVU:PARALLEL:RUN")
            End If
            Return SafeNativeMethods._lastOperationStatus
        End Function

        Private Shared _parallelEnabled As Boolean
        ''' <summary>
        ''' Gets the status of parallel measurments.
        ''' </summary>
        Public Shared Function ParallelEnabledGetter() As Boolean
            Return SafeNativeMethods._parallelEnabled
        End Function

        ''' <summary>
        ''' Sets the status of parallel measurments.
        ''' </summary>
        Public Shared Function ParallelEnabledSetter(ByVal value As Boolean) As Boolean
            SafeNativeMethods._parallelEnabled = value
        End Function

        ''' <summary>
        ''' Resets the device to a knowb state. 
        ''' </summary>
        ''' <param name="id">The instrument identification code.</param>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
        Public Shared Function ResetKnownState(ByVal id As Integer) As Integer

            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            If Lpt.SafeNativeMethods.UsingDevices Then
                ' SafeNativeMethods._lastStatus = LowLevelMethods.devclr()
            End If
            Return SafeNativeMethods._lastOperationStatus

        End Function

        Private Shared _lastSocketStatus As Integer
        ''' <summary>
        ''' Gets the last Socket Status or Error.
        ''' </summary>
        Public Shared ReadOnly Property LastSocketStatus() As Integer
            Get
                Return SafeNativeMethods._lastSocketStatus
            End Get
        End Property

        Private Shared _statusByte As UShort
        ''' <summary>
        ''' Gets the last status byte,
        ''' </summary>
        Public Shared ReadOnly Property LastStatusByte() As Integer
            Get
                Return SafeNativeMethods._statusByte
            End Get
        End Property

        ''' <summary>
        ''' Returns overall status details
        ''' </summary>
        Public Shared Function LastStatusDetails() As String
            Dim details As New System.Text.StringBuilder
            Dim contents As String = OperationStatusDetails(_lastOperationStatus)
            If Not String.IsNullOrWhiteSpace(contents) Then
                details.Append(contents)
            End If
            Return details.ToString
        End Function


        ''' <summary>
        ''' Gets the sample bias.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
        Public Shared Function SamplingBiasGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single?
            Return SafeNativeMethods.DirectVoltageLevelGetter(id, emulatedValue)
        End Function

        ''' <summary>
        ''' Sets the sample bias.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
        Public Shared Function SamplingBiasSetter(ByVal id As Integer, ByVal value As Single) As Integer
            Return SafeNativeMethods.DirectVoltageLevelSetter(id, value)
        End Function

        Private Shared _samplingCount As Integer
        ''' <summary>
        ''' Gets the sample count.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="emulatedValue")>
        Public Shared Function SamplingCountGetter(ByVal id As Integer, ByVal emulatedValue As Integer) As Integer
            SafeNativeMethods._samplingCount = 1
            Return SafeNativeMethods._samplingCount
        End Function

        ''' <summary>
        ''' Sets the sample count.
        ''' </summary>
        ''' <remarks>
        ''' Presently, the low level instrument supports a single count.
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
        Public Shared Function SamplingCountSetter(ByVal id As Integer, ByVal value As Integer) As Integer
            value = 1
            SafeNativeMethods._samplingCount = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            Return SafeNativeMethods._lastOperationStatus
        End Function

        Private Shared _samplingInterval As Single
        ''' <summary>
        ''' Gets the sample Interval.
        ''' </summary>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="emulatedValue")>
        Public Shared Function SamplingIntervalGetter(ByVal id As Integer, ByVal emulatedValue As Single) As Single
            SafeNativeMethods._samplingInterval = 0
            Return SafeNativeMethods._samplingInterval
        End Function

        ''' <summary>
        ''' Sets the sample Interval.
        ''' </summary>
        ''' <remarks>
        ''' Presently, the low level instrument supports a single interval of 0.
        ''' </remarks>
        <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="id")>
        Public Shared Function SamplingIntervalSetter(ByVal id As Integer, ByVal value As Single) As Integer
            value = 0
            SafeNativeMethods._samplingInterval = value
            SafeNativeMethods._lastOperationStatus = OperationStatus.Success
            Return SafeNativeMethods._lastOperationStatus
        End Function

        ''' <summary>
        ''' Returns True if Parallel CVU is support..
        ''' </summary>
        Public Shared ReadOnly Property SupportsParallel() As Boolean
            Get
                Try
                    Return isr.Kte.Lpt.SafeNativeMethods.ChannelCountGetter(4) > 0
                Catch ex As FunctionCallException
                    SafeNativeMethods._lastOperationStatus = OperationStatus.Success
                    Return False
                End Try
            End Get
        End Property

#End Region

    End Class

End Namespace

